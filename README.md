# nTOF acquisition files viewer

nTOF acquisition files viewer.

This applications requires CERN CentOS7 environement to compile and run (mostly for Castor dependencies).

## Build

The following packages must be installed to compile this app:

To build the application:
```bash
# Install dependencies
yum install

rm -rf build && mkdir build
cd build && cmake ..

make -j4
```

## Testing

To run unit-tests:
```bash
TBD
```

To run linters:
```bash
# Install dependencies
yum install cppcheck clang gcc

# Run CMake again to detect linters
cd build && cmake ..

# Lint !
make lint
```


## Debugging

To compile this component for gdb/lldb:
```bash
cd build && cmake -DCMAKE_BUILD_TYPE=Debug
```
