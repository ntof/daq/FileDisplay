include(Tools)

if(NOT BOOST_ROOT AND IS_DIRECTORY "/opt/ntof/boost")
    message(STATUS "Using preferred boost version: /opt/ntof/boost")
    set(BOOST_ROOT "/opt/ntof/boost" CACHE PATH "")
endif()

find_package(Boost REQUIRED system regex filesystem thread)

find_library(PTHREAD_LIBRARIES pthread)
if(PTHREAD_LIBRARIES)
    list(APPEND Boost_LIBRARIES ${PTHREAD_LIBRARIES})
endif()
