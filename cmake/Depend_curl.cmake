
find_library(CURL_LIBRARIES curl)
find_path(CURL_INCLUDE_DIRS curl.h PATH_SUFFIXES curl)

if(NOT CURL_LIBRARIES OR NOT CURL_INCLUDE_DIRS)
    message(SEND_ERROR "Failed to find curl")
endif()
