
include(xProject)
include(Cern)

xProject_Add(NTOFLIB
    GIT_SUBMODULE
    CMAKE_ARGS -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX= -DBOOST_ROOT=${BOOST_ROOT}
    CMAKE_CACHE_ARGS "-DEXT_CACHE:path=${CMAKE_BINARY_DIR}"
    INSTALL_COMMAND $(MAKE) install "DESTDIR=${CMAKE_BINARY_DIR}/instroot"
    LIBRARIES "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_LIBDIR}/libntoflib.a"
    INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_INCLUDEDIR}/ntoflib")

# This allows to directly use ntoflib in target_link_dependecies, handling targets deps properly
add_library(ntoflib STATIC IMPORTED)
set_property(TARGET ntoflib PROPERTY IMPORTED_LOCATION "${NTOFLIB_LIBRARIES}")
add_dependencies(ntoflib NTOFLIB)
