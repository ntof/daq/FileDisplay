
include(Tools)

find_library(XRDCL_LIBRARIES XrdCl)
find_library(XRDPOSIX_LIBRARIES XrdPosix)
find_path(XROOTD_INCLUDE_DIRS XrdCl/XrdClFileSystem.hh PATH_SUFFIXES xrootd)

assert(XRDCL_LIBRARIES AND XRDPOSIX_LIBRARIES AND XROOTD_INCLUDE_DIRS 
    MESSAGE "Failed to find xrootd package")
