
string(TOLOWER "${CMAKE_PROJECT_NAME}" PACKAGE_NAME)
set(CPACK_PACKAGE_NAME "${PACKAGE_NAME}")
set(CPACK_PACKAGE_VENDOR "CERN")
set(CPACK_PACKAGE_INSTALL_REGISTRY_KEY "${CMAKE_PROJECT_NAME}")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/COPYING")
set(CPACK_PACKAGE_VERSION_MAJOR "${VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${GIT_COUNT}")
set(CPACK_PROJECT_CONFIG_FILE "${CMAKE_SOURCE_DIR}/cmake/PackConfig.cmake")

set(CPACK_PACKAGE_CONTACT "Sylvain Fargier <sylvain.fargier@cern.ch>")
set(CPACK_RPM_PACKAGE_RELEASE_DIST ON)
set(CPACK_RPM_FILE_NAME "RPM-DEFAULT")

# Generate debug package
set(CPACK_RPM_DEBUGINFO_PACKAGE ON)

add_custom_target(rpm-package COMMAND ${CMAKE_CPACK_COMMAND} -G RPM)

if(UNIX)
    install(FILES data/filedisplay.desktop DESTINATION share/applications)
    install(FILES data/filedisplay-mime.xml DESTINATION share/mime/packages)
    install(FILES data/filedisplay.svg DESTINATION share/pixmaps)
endif(UNIX)

include(CPack)
