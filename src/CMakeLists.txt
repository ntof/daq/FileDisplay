
include_directories(${CMAKE_SOURCE_DIR}/lib)
include_directories(SYSTEM
    ${QT_INCLUDE_DIRS}
    ${Boost_INCLUDE_DIRS}
    ${CURL_INCLUDE_DIRS}
    ${NTOFLIB_INCLUDE_DIRS}
    ${XROOTD_INCLUDE_DIRS}
    ${CMAKE_CURRENT_BINARY_DIR})

# Use the compile definitions defined in the Qt 5 Widgets module
add_definitions(${Qt5Widgets_DEFINITIONS})

# Add compiler flags for building executables (-fPIE)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")

configure_file("local-config.h.in" "local-config.h" @ONLY)

set(FD_SRC
    chrono.cc chrono.hh
    local-config.h
    XRootFileDialog.cpp XRootFileDialog.hpp
    XRootBrowser.cpp XRootBrowser.hpp
    Channel.cpp Channel.h
    Colorpicker.cpp Colorpicker.h
    Config.cpp Config.h
    CustomPlotZoom.cpp CustomPlotZoom.h
    FileDisplayException.cpp FileDisplayException.h
    FileReader.cpp FileReader.h
    main.cpp
    MainWindow.cpp MainWindow.h
    MenuBar.cpp MenuBar.h
    NavButton.cpp NavButton.h
    OptionPanel.cpp OptionPanel.h
    RangeSelectorPanel.cpp RangeSelectorPanel.h
    SearchFile.cpp SearchFile.h
    SummaryPanel.cpp SummaryPanel.h
    TableParams.cpp TableParams.h
    VirtualCounter.cpp VirtualCounter.h
    WriterFile.cpp WriterFile.h
    Progress.cpp Progress.h
    FormLayout.cpp FormLayout.h
    GraphDataContainer.cpp GraphDataContainer.h

    widgets/FileSelector.cpp widgets/FileSelector.h
    )

set_property(SOURCE chrono.hh PROPERTY SKIP_AUTOMOC ON)
qt5_add_resources(FD_QRC icons.qrc)

add_executable(FileDisplay ${FD_SRC} ${FD_QRC})
target_link_libraries(FileDisplay extDeps
    ${QT_LIBRARIES}
    ${Boost_LIBRARIES}
    ${CURL_LIBRARIES}
    ${XRDCL_LIBRARIES} ${XRDPOSIX_LIBRARIES}
    ntoflib)

install(TARGETS FileDisplay
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT Devel
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT Runtime
        RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT Runtime
        PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/BusListener COMPONENT Devel
        BUNDLE DESTINATION . COMPONENT Runtime)
