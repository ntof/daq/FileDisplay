#include "Channel.h"

#include <algorithm>
#include <iostream>
#include <sstream>

#include <QColor>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QMessageBox>
#include <QPushButton>

#include <NtofLib.h>
#include <math.h>
#include <stdint.h>

#include "Config.h"
#include "CustomPlotZoom.h"
#include "FileDisplayException.h"
#include "FileReader.h"
#include "MainWindow.h"
#include "TableParams.h"

namespace ntof {
namespace eventDisplay {
/*
 * \brief Constructor of the class
 */
Channel::Channel() :
    groupBox_(NULL),
    checkBox_(NULL),
    paramsButton_(NULL),
    thresholdButton_(NULL),
    adaptViewButton_(NULL),
    dataAreaButton_(NULL),
    colorButton_(NULL),
    graph_(NULL),
    graphThresholdMv_(NULL),
    graphThresholdADC_(NULL),
    dataArea_(new QCPItemRect(MainWindow::getInstance()->getCustomPlot())),
    data_(DataContainer::create()),
    dataThresholdMv_(DataContainer::create()),
    dataThresholdADC_(DataContainer::create()),
    color_(rand() % 245 + 10, rand() % 245 + 10, rand() % 245 + 10),
    active_(false),
    showParams_(false),
    showThreshold_(false),
    id_(0),
    adcRange_(0),
    detectorType_(""),
    detectorId_(0),
    moduleType_(""),
    channel_(0),
    module_(0),
    chassis_(0),
    stream_(0),
    sampleRate_(0),
    sampleSize_(0),
    fullScaleInADC_(0),
    fullScaleInMilliVolts_(0),
    delayTime_(0),
    thresholdADC_(0),
    thresholdMV_(0),
    thresholdSign_(0),
    offset_(0),
    preSample_(0),
    postSample_(0),
    clockState_(""),
    zeroSuppressionStart_(0),
    zeroSuppression_("undefined"),
    masterDetectorType_("undefined"),
    masterDetectorId_(0),
    timeWindow_(0),
    lowerLimit_(0),
    eventNumber_(-1)
{}

/*
 * \brief Destructor of the class
 */
Channel::~Channel()
{
    MainWindow::getInstance()->removeDetectorElement(groupBox_);
    // Hide the graph
    if (graph_->visible())
    {
        removeGraph();
        MainWindow::getInstance()->getCustomPlot()->removeGraph(graph_);
    }

    // Hide the threshold
    if (graphThresholdMv_->visible())
    {
        graphThresholdMv_->setVisible(false);
    }
    else if (graphThresholdADC_->visible())
    {
        graphThresholdADC_->setVisible(false);
    }

    // Hide the data area
    if (dataArea_ != NULL)
    {
        dataArea_->setVisible(false);
    }

    hideParams();

    if (paramsButton_ != NULL)
    {
        delete paramsButton_;
    }
    if (thresholdButton_ != NULL)
    {
        delete thresholdButton_;
    }
    if (adaptViewButton_ != NULL)
    {
        delete adaptViewButton_;
    }
    if (dataAreaButton_ != NULL)
    {
        delete dataAreaButton_;
    }
    if (checkBox_ != NULL)
    {
        delete checkBox_;
    }
    if (groupBox_ != NULL)
    {
        delete groupBox_;
    }
}

/*
 * \brief Get the ID of the channel
 * \return Return the ID of the channel
 */
int32_t Channel::getId()
{
    return id_;
}

/*
 * \brief Get the ADC range
 * \return Return the ADC range
 */
int32_t Channel::getADCRange()
{
    return adcRange_;
}

/*
 * \brief Get the even number
 * \return Return the event number
 */
int32_t Channel::getEventNumber()
{
    return eventNumber_;
}
/*
 * \brief Get the detector type
 * \return the detector type
 */
std::string &Channel::getDetectorType()
{
    return detectorType_;
}

/*
 * \brief Get the detector id
 * \return the detector id
 */
int32_t Channel::getDetectorId()
{
    return detectorId_;
}

/*
 * \brief Get the sample rate
 * \return the sample rate
 */
float Channel::getSampleRate()
{
    return sampleRate_;
}

/*
 * \brief Get the sample size
 * \return the sample size
 */
float Channel::getSampleSize()
{
    return sampleSize_;
}

/*
 * \brief Get the full scale in millivolts
 * \return the full scale in millivolts
 */
float Channel::getFullScaleInMilliVolts()
{
    return fullScaleInMilliVolts_;
}

/*
 * \brief Get the full scale in volts
 * \return the full scale in volts
 */
float Channel::getFullScaleInVolts()
{
    return round(fullScaleInMilliVolts_ / 10) / 100;
}

/*
 * \brief Get the delai time
 * \return the delai time
 */
int32_t Channel::getDelayTime()
{
    return delayTime_;
}

/*
 * \brief Get the threshold
 * \return the threshold
 */
float Channel::getThreshold()
{
    return thresholdMV_;
}

/*
 * \brief Get the offset
 * \return the offset
 */
float Channel::getOffset()
{
    return offset_;
}

/*
 * \brief Get the pre samples
 * \return the pre samples
 */
int32_t Channel::getPreSamples()
{
    return preSample_;
}

/*
 * \brief Get the post samples
 * \return the post samples
 */
int32_t Channel::getPostSamples()
{
    return postSample_;
}

/*
 * \brief Get the clock state
 * \return the clock state
 */
std::string &Channel::getClockState()
{
    return clockState_;
}

/*
 * \brief Get the zero suppression start
 * \return the zero suppression start
 */
int32_t Channel::getZeroSuppressionStart()
{
    return zeroSuppressionStart_;
}

/*
 * \brief Set the event number
 * \param eventNumber: The event number to set
 */
void Channel::setEventNumber(int32_t eventNumber)
{
    eventNumber_ = eventNumber;
}

/*
 * \brief Reserve a part memory
 * \param size: Size to reserve
 */
bool Channel::reserve(int64_t size)
{
    bool res = false;
    try
    {
        MainWindow::getInstance()->addActionOnGoing();
        MainWindow::getInstance()->setDebugMessage("Allocate memory");
        data_->reserve(size);
        res = true;
    }
    catch (std::bad_alloc &ba)
    {
        std::ostringstream oss;
        oss << "Not enough memory to read the data of the detector "
            << detectorType_ << "_" << detectorId_ << " [" << size << "]";
        QMessageBox::critical(this, QObject::tr("Error"),
                              QObject::tr(oss.str().c_str()), QMessageBox::Ok);
    }
    MainWindow::getInstance()->removeActionOnGoing();
    return res;
}

/*
 * \brief Add a point to the list of points
 * \param x: position in the X axis
 * \param y: position in the Y axis
 */
void Channel::addPoint(double x, double y)
{
    data_->add(QCPGraphData(x, y));
}

/*
 * \brief Add the graph to the plot and to the legend
 */
void Channel::setupGraph()
{
    graph_->addToLegend();
    graph_->setVisible(true);
    graph_->setData(data_);
}

/*
 * \brief Remove the graph from the plot area
 */
void Channel::removeGraph()
{
    graph_->removeFromLegend();
    graph_->setVisible(false);
}

/*
 * \brief Allow to activate the channel (will appear on the plot area)
 */
void Channel::setActive()
{
    checkBox_->setChecked(true);
    active_ = true;
}

/*
 * \brief Allow to deactivate the channel (wont appear on the plot area)
 */
void Channel::setInactive()
{
    checkBox_->setChecked(false);
    active_ = false;
    // TODO
    activeThreshold(false);
    dataArea();
}

/*
 * \brief Allow to know if the channel is active (can appear on the plot area)
 * \return Return true if the channel is active
 */
bool Channel::isActive()
{
    return active_;
}

/*
 * \brief Show the parameters of this channel
 */
void Channel::showParams()
{
    paramsButton_->setChecked(true);
    showParams_ = true;
}

/*
 * \brief Hide the parameters of this channel
 */
void Channel::hideParams()
{
    paramsButton_->setChecked(false);
    showParams_ = false;
}

/*
 * \brief Allow to know if the parameters are displayed or not
 * \return Return true if the parameters are displayed
 */
bool Channel::isParamsShown()
{
    return showParams_;
}

/*
 * \brief Get the channel ID
 * \return the channel ID
 */
uint32_t Channel::getChannel() const
{
    return channel_;
}

/*
 * \brief Get the chassis ID
 * \return the chassis ID
 */
uint32_t Channel::getChassis() const
{
    return chassis_;
}

/*
 * \brief Get the module ID
 * \return the module ID
 */
uint32_t Channel::getModule() const
{
    return module_;
}

/*
 * \brief Get the stream ID
 * \return the stream ID
 */
uint32_t Channel::getStream() const
{
    return stream_;
}

/*
 * \brief Get the type of the module
 * \return the type of the module
 */
const std::string &Channel::getModuleType() const
{
    return moduleType_;
}

/*
 * \brief Get the falling or rising edge
 * \return the falling or rising edge
 */
int32_t Channel::getThresholdSign() const
{
    return thresholdSign_;
}

/*
 * \brief Get the data container
 * \return the data container
 */
const Channel::DataContainer &Channel::getData() const
{
    return data_;
}

/*
 * \brief Get the button used to apply a custom zoom (using the configuration)
 * \return the button used to apply a custom zoom (using the configuration)
 */
QPushButton *&Channel::getAdaptViewButton()
{
    return adaptViewButton_;
}

/*
 * \brief Get the button used to display the rectangle where the data can be
 * defined \return a pointer on the button
 */
QPushButton *&Channel::getDataAreaButton()
{
    return dataAreaButton_;
}

/*
 * \brief Allow to know if the threshold is displayed
 * \return Return true if the threshold appears on the plot area
 */
bool Channel::isShowThreshold() const
{
    return showThreshold_;
}

/*
 * \brief Set the threshold as should be display
 * \param showThreshold: True if the threshold must be displayed or false else
 */
void Channel::setShowThreshold(bool showThreshold)
{
    showThreshold_ = showThreshold;
}

/*
 * \brief Get the name of the detector
 * \Return Return the detector type concatenate with the detector ID
 */
QString Channel::getDetectorName()
{
    std::ostringstream ossName;
    ossName << detectorType_ << " " << detectorId_;
    return tr(ossName.str().c_str());
    ;
}

/*
 * \brief Initialize the graph
 */
void Channel::initGraph()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Initialize a new graph");
    CustomPlotZoom *customPlot = (MainWindow::getInstance()->getCustomPlot());
    graph_ = customPlot->addGraph();
    graph_->setAdaptiveSampling(true);
    graph_->setSelectable(QCP::stWhole);
    // Check the color attribut
    updateColor();
    graph_->setName(getDetectorName());
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Initialize the channel
 */
void Channel::initChannel()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Initialize a new channel");

    MainWindow::getInstance()->setDebugMessage("Get parameters");
    //            ntof::lib::NtofLib ntofLib =
    //            MainWindow::getInstance()->getFileReader()->getNtofLib();
    id_ = FileReader::getInstance()->getNtofLib().getMODH()->getIndex();
    adcRange_ = FileReader::getInstance()->getNtofLib().getMODH()->getADCRange();
    detectorType_ =
        FileReader::getInstance()->getNtofLib().getMODH()->getDetectorType();
    detectorId_ =
        FileReader::getInstance()->getNtofLib().getMODH()->getDetectorId();
    moduleType_ =
        FileReader::getInstance()->getNtofLib().getMODH()->getModuleType();
    channel_ = FileReader::getInstance()->getNtofLib().getMODH()->getChannel();
    module_ = FileReader::getInstance()->getNtofLib().getMODH()->getModule();
    chassis_ = FileReader::getInstance()->getNtofLib().getMODH()->getChassis();
    stream_ = FileReader::getInstance()->getNtofLib().getMODH()->getStream();
    sampleRate_ =
        FileReader::getInstance()->getNtofLib().getMODH()->getSampleRate();
    sampleSize_ =
        FileReader::getInstance()->getNtofLib().getMODH()->getSampleSize();
    fullScaleInADC_ =
        FileReader::getInstance()->getNtofLib().getMODH()->getFullScale();
    fullScaleInMilliVolts_ = FileReader::getInstance()
                                 ->getNtofLib()
                                 .getMODH()
                                 ->getFullScaleInMilliVolts();
    delayTime_ =
        FileReader::getInstance()->getNtofLib().getMODH()->getDelayTime();
    thresholdADC_ =
        FileReader::getInstance()->getNtofLib().getMODH()->getThresholdADC();
    thresholdMV_ = FileReader::getInstance()
                       ->getNtofLib()
                       .getMODH()
                       ->getThresholdMilliVolts();
    thresholdSign_ =
        FileReader::getInstance()->getNtofLib().getMODH()->getThresholdSign();
    offset_ = FileReader::getInstance()->getNtofLib().getMODH()->getOffset();
    preSample_ =
        FileReader::getInstance()->getNtofLib().getMODH()->getPreSample();
    postSample_ =
        FileReader::getInstance()->getNtofLib().getMODH()->getPostSample();
    clockState_ =
        FileReader::getInstance()->getNtofLib().getMODH()->getClockState();

    timeWindow_ = sampleSize_ / sampleRate_;
    lowerLimit_ = -offset_ - (fullScaleInMilliVolts_ / 2);

    try
    {
        zeroSuppressionStart_ = FileReader::getInstance()
                                    ->getNtofLib()
                                    .getMODH()
                                    ->getZeroSuppressionStart();
    }
    catch (ntof::lib::NtofLibException &ex)
    {
        zeroSuppressionStart_ = -1;
    }

    try
    {
        masterDetectorType_ = FileReader::getInstance()
                                  ->getNtofLib()
                                  .getMODH()
                                  ->getMasterDetectorType();
        masterDetectorId_ = FileReader::getInstance()
                                ->getNtofLib()
                                .getMODH()
                                ->getMasterDetectorId();
    }
    catch (ntof::lib::NtofLibException &ex)
    {
        masterDetectorType_ = "unknown";
        masterDetectorId_ = -1;
    }

    if (zeroSuppressionStart_ == -1)
    {
        zeroSuppression_ = "undefined";
    }
    else if (zeroSuppressionStart_ < (timeWindow_ * 1E6))
    {
        if (masterDetectorType_ == "unknown")
        {
            zeroSuppression_ = "on";
        }
        else if (masterDetectorType_ == "")
        {
            zeroSuppression_ = "master";
        }
        else
        {
            zeroSuppression_ = "slave";
        }
    }
    else
    {
        zeroSuppression_ = "off";
    }

    MainWindow::getInstance()->setDebugMessage("Add buttons");
    groupBox_ = new QWidget;
    groupBox_->setLayout(new QHBoxLayout);
    groupBox_->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);

    std::ostringstream oss;
    oss << FileReader::getInstance()->getNtofLib().getMODH()->getDetectorType()
        << "_"
        << FileReader::getInstance()->getNtofLib().getMODH()->getDetectorId();
    checkBox_ = new QCheckBox(QString::fromStdString(oss.str()));
    checkBox_->setChecked(false);
    QObject::connect(checkBox_, SIGNAL(clicked(bool)), this, SLOT(active(bool)));

    paramsButton_ = new QPushButton(this);
    paramsButton_->setIcon(QIcon(
        QPixmap(":/icons/oxygen/48x48/categories/preferences-system.png")));
    paramsButton_->setIconSize(QSize(20, 20));
    paramsButton_->setCheckable(true);
    paramsButton_->setToolTip(tr("Display the parameters"));
    paramsButton_->setFixedSize(
        QSize(paramsButton_->height(), paramsButton_->height()));
    QObject::connect(paramsButton_, SIGNAL(clicked(bool)), this,
                     SLOT(showParams(bool)));

    thresholdButton_ = new QPushButton(this);
    thresholdButton_->setIcon(QIcon(QPixmap(
        ":/icons/oxygen/48x48/actions/office-chart-line-percentage.png")));
    thresholdButton_->setIconSize(QSize(20, 20));
    thresholdButton_->setCheckable(true);
    thresholdButton_->setToolTip(tr("Display the threshold"));
    thresholdButton_->setFixedSize(
        QSize(paramsButton_->height(), paramsButton_->height()));
    QObject::connect(thresholdButton_, SIGNAL(clicked(bool)), this,
                     SLOT(showThreshold(bool)));

    adaptViewButton_ = new QPushButton(this);
    adaptViewButton_->setIcon(
        QIcon(QPixmap(":/icons/oxygen/48x48/actions/page-zoom.png")));
    adaptViewButton_->setIconSize(QSize(20, 20));
    adaptViewButton_->setCheckable(true);
    adaptViewButton_->setToolTip(
        tr("Adapt the amplitude to the digitizer configuration"));
    adaptViewButton_->setFixedSize(
        QSize(paramsButton_->height(), paramsButton_->height()));
    QObject::connect(adaptViewButton_, SIGNAL(clicked()), this,
                     SLOT(adaptTimeWindow()));

    dataAreaButton_ = new QPushButton(this);
    dataAreaButton_->setIcon(
        QIcon(QPixmap(":/icons/oxygen/48x48/actions/select-rectangular.png")));
    dataAreaButton_->setIconSize(QSize(20, 20));
    dataAreaButton_->setCheckable(true);
    dataAreaButton_->setToolTip(
        tr("Display the area where the data can be defined"));
    dataAreaButton_->setFixedSize(
        QSize(paramsButton_->height(), paramsButton_->height()));
    QObject::connect(dataAreaButton_, SIGNAL(clicked()), this, SLOT(dataArea()));

    colorButton_ = new QPushButton(this);
    colorButton_->setStyleSheet(QString("background-color: rgb(%1,%2,%3)")
                                    .arg(color_.red())
                                    .arg(color_.green())
                                    .arg(color_.blue()));
    colorButton_->setToolTip(tr("Select plot color"));
    colorButton_->setFixedSize(
        QSize(paramsButton_->height(), paramsButton_->height()));
    QObject::connect(colorButton_, SIGNAL(clicked()), this,
                     SLOT(colorChooser()));

    groupBox_->layout()->addWidget(checkBox_);
    groupBox_->layout()->addWidget(paramsButton_);
    groupBox_->layout()->addWidget(thresholdButton_);
    groupBox_->layout()->addWidget(adaptViewButton_);
    groupBox_->layout()->addWidget(dataAreaButton_);
    groupBox_->layout()->addWidget(colorButton_);
    MainWindow::getInstance()->addDetectorElement(groupBox_);

    initGraph();
    graph_->removeFromLegend();

    initDataArea();
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 *\brief Initialized the rectangle dimensions
 */
void Channel::initDataArea()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Initialize data area");
    double x1 = 0;
    double y1 = lowerLimit_;
    double x2 = timeWindow_ * 1E6;
    double y2 = lowerLimit_ + fullScaleInMilliVolts_;

    if (Config::getInstance()->getDataUnit() == "adc")
    {
        y1 = mvToADC(y1);
        y2 = mvToADC(y2);
    }

    QPen pen;
    pen.setStyle(Qt::DashDotLine);
    pen.setWidth(1);
    pen.setColor(color_);

    dataArea_->setPen(pen);
    dataArea_->topLeft->setCoords(x1, y2);
    dataArea_->bottomRight->setCoords(x2, y1);
    dataArea_->setVisible(false);
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Clear the data
 */
void Channel::clear()
{
    data_->clear();
}

/*
 * \brief Update the color of the graph
 * \return Return true if the color has been modified
 */
bool Channel::updateColor()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Update color");
    bool res = false;
    Detector detector = Config::getInstance()->getDetectorConf(detectorType_,
                                                               detectorId_);
    if (detector.name == detectorType_)
    {
        if (color_.red() != detector.red || color_.green() != detector.green ||
            color_.blue() != detector.blue)
        {
            res = true;
            color_ = QColor(detector.red, detector.green, detector.blue);
            colorButton_->setStyleSheet(
                QString("background-color: rgb(%1,%2,%3)")
                    .arg(detector.red)
                    .arg(detector.green)
                    .arg(detector.blue));
            initDataArea();
        }
    }
    else
    {
        Config::getInstance()->setDetectorConf(detectorType_, detectorId_,
                                               color_.red(), color_.green(),
                                               color_.blue());
    }
    if (graph_ != NULL)
    {
        graph_->setPen(QPen(color_, 1));
        graph_->selectionDecorator()->setPen(QPen(color_, 3));
    }
    if (graphThresholdMv_ != NULL)
    {
        graphThresholdMv_->setPen(QPen(color_, 1));
    }
    if (graphThresholdADC_ != NULL)
    {
        graphThresholdADC_->setPen(QPen(color_, 1));
    }
    MainWindow::getInstance()->removeActionOnGoing();
    return res;
}

/*
 * \brief Convert data from mV to ADC according the parameters
 * \param data: Data to convert
 * \return Return the data converted
 */
int32_t Channel::mvToADC(double data)
{
    if (getADCRange() != 0 && getFullScaleInMilliVolts() != 0)
    {
        return ((data + getOffset()) * (double) getADCRange()) /
            getFullScaleInMilliVolts();
    }

    throw FileDisplayException("The data can't be converted", __FILE__,
                               __LINE__);
}

/*
 * \brief Convert data from ADC to mV according the parameters
 * \param data: Data to convert
 * \return Return the data converted
 */
double Channel::ADCToMv(double data)
{
    if (getADCRange() != 0 && getFullScaleInMilliVolts() != 0)
    {
        return (data * getFullScaleInMilliVolts() / (double) getADCRange()) -
            getOffset();
    }

    throw FileDisplayException("The data can't be converted", __FILE__,
                               __LINE__);
}

/* ************************************************************* */
/* SLOTS                                                         */
/* ************************************************************* */
/*
 * \brief Activate or deactivate the channel
 * \param state: true to activate or false to deactivate
 */
void Channel::active(bool state)
{
    if (state == Qt::Unchecked)
    {
        setInactive();
    }
    else
    {
        setActive();
    }
    MainWindow::getInstance()->enablePlot();
}

/*
 * \brief Show or hide the parameters
 * \param state: true to show or false to hide
 */
void Channel::showParams(bool state)
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Displaying parameters");
    if (state == Qt::Unchecked)
    {
        hideParams();
        MainWindow::getInstance()->getTableParams()->clear();
        FileReader::getInstance()->updateElement();
        MainWindow::getInstance()->getTableParams()->updateElement();
    }
    else
    {
        showParams();
        MainWindow::getInstance()->getTableParams()->addRow(this);
        MainWindow::getInstance()->getTableParams()->updateElement();
    }
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Initialize the threshold by adding a dedicated graph (which doesn't
 * appear in the legend)
 */
void Channel::initThreshold()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Initialize the threshold");
    CustomPlotZoom *customPlot = (MainWindow::getInstance()->getCustomPlot());
    int32_t extraRange = (timeWindow_ * 1E6) / 10;
    // Threshold in mV
    graphThresholdMv_ = customPlot->addGraph();
    graphThresholdMv_->setAdaptiveSampling(true);

    // Set the color attribut
    graphThresholdMv_->setPen(QPen(color_, 1));

    graphThresholdMv_->setName(getDetectorName());
    graphThresholdMv_->removeFromLegend();

    dataThresholdMv_->add(QCPGraphData(0 - extraRange, thresholdMV_));
    dataThresholdMv_->add(
        QCPGraphData((timeWindow_ * 1E6) + extraRange, thresholdMV_));
    graphThresholdMv_->setData(dataThresholdMv_);
    graphThresholdMv_->setVisible(false);

    // Threshold in ADC
    graphThresholdADC_ = customPlot->addGraph();
    graphThresholdADC_->setAdaptiveSampling(true);

    // Set the color attribut
    graphThresholdADC_->setPen(QPen(color_, 1));

    graphThresholdADC_->setName(getDetectorName());
    graphThresholdADC_->removeFromLegend();

    dataThresholdADC_->add(QCPGraphData(0 - extraRange, thresholdADC_));
    dataThresholdADC_->add(
        QCPGraphData((timeWindow_ * 1E6) + extraRange, thresholdADC_));
    graphThresholdADC_->setData(dataThresholdADC_);
    graphThresholdADC_->setVisible(false);
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Show or hide the threshold
 * \param state: true to show or false to hide
 */
void Channel::activeThreshold(bool state)
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Displaying threshold");
    if (active_)
    {
        // Hide or show the threshold (depends on the data unit) and hide the
        // other
        showThreshold_ = state;
        if (Config::getInstance()->getDataUnit() == "adc")
        {
            graphThresholdADC_->setVisible(state);
            graphThresholdMv_->setVisible(false);
        }
        else if (Config::getInstance()->getDataUnit() == "mv")
        {
            graphThresholdMv_->setVisible(state);
            graphThresholdADC_->setVisible(false);
        }
        else
        {
            std::cerr << "data unit unknown" << std::endl;
        }
    }
    else
    {
        graphThresholdMv_->setVisible(false);
        graphThresholdADC_->setVisible(false);
        thresholdButton_->setChecked(false);
    }
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Action to perform when the threshold button is used
 * \param state: true to show or false to hide
 */
void Channel::showThreshold(bool state)
{
    activeThreshold(state);
    if (active_)
    {
        MainWindow::getInstance()->getCustomPlot()->replot();
        MainWindow::getInstance()->updateElement();
    }
}

/*
 * \brief Show the threshold and check the check box
 */
void Channel::showThreshold()
{
    thresholdButton_->setChecked(true);
    showThreshold(true);
}

/*
 * \brief Get the lower limit
 * \return the lower limit
 */
double Channel::getLowerLimit() const
{
    return lowerLimit_;
}

/*
 * \brief Get the time window
 * \return the time window
 */
double Channel::getTimeWindow() const
{
    return timeWindow_;
}

/*
 * \brief Get the zero suppression status
 * \return the zero suppression status
 */
const std::string &Channel::getZeroSuppression() const
{
    return zeroSuppression_;
}

/*
 * \brief Get the master group
 * \return the master group
 */
std::string Channel::getMasterGroup()
{
    std::ostringstream masterGroup("");
    if (zeroSuppression_ == "slave")
    {
        masterGroup << masterDetectorType_ << " " << masterDetectorId_;
    }
    return masterGroup.str();
}

/*
 * \brief Get the master detector type
 * \return the master detector type
 */
const std::string &Channel::getMasterDetectorType() const
{
    return masterDetectorType_;
}

/*
 * \brief Get the master detector id
 * \return the master detector id
 */
int32_t Channel::getMasterDetectorId() const
{
    return masterDetectorId_;
}

/*
 * \brief Adapt the zoom according the configuration of the channel
 */
void Channel::adaptTimeWindow()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Adapt the range");
    FileReader::getInstance()->disabledButtonsAdaptedView();
    adaptViewButton_->setChecked(true);

    Zoom range = MainWindow::getInstance()->getCustomPlot()->getZoom();
    range.yAxis.lower = -(lowerLimit_ + fullScaleInMilliVolts_);
    range.yAxis.upper = -lowerLimit_;

    // Adapt the view to ADC
    if (Config::getInstance()->getDataUnit() == "adc")
    {
        range.yAxis.lower = (range.yAxis.lower + offset_) * adcRange_ /
            fullScaleInMilliVolts_;
        range.yAxis.upper = (range.yAxis.upper + offset_) * adcRange_ /
            fullScaleInMilliVolts_;
    }

    MainWindow::getInstance()->getCustomPlot()->setZoom(range);
    MainWindow::getInstance()->getCustomPlot()->replot();
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Display a rectangle to symbolize the area in which the data can be
 * defined \return Return true if the rectangle is displayed
 */
bool Channel::isDisplayDataArea()
{
    bool res = false;
    if (dataAreaButton_ && dataAreaButton_->isChecked())
    {
        res = true;
    }
    return res;
}

/*
 * \brief Display a rectangle to symbolize the area in which the data can be
 * defined \param replot: The plot area is not redraw if this parameter is equal
 * to false
 */
void Channel::displayDataArea(bool replot)
{
    dataAreaButton_->setChecked(true);
    dataArea_->setVisible(true);

    if (replot)
    {
        MainWindow::getInstance()->getCustomPlot()->replot();
    }
}

/*
 * \brief Hide the rectangle used to symbolize the area in which the data can be
 * defined \param replot: The plot area is not redraw if this parameter is equal
 * to false
 */
void Channel::hideDataArea(bool replot)
{
    dataAreaButton_->setChecked(false);
    dataArea_->setVisible(false);

    if (replot)
    {
        MainWindow::getInstance()->getCustomPlot()->replot();
    }
}

/*
 * \brief Activate or deactivate the data area
 */
void Channel::dataArea()
{
    if (dataAreaButton_)
    {
        // If the channel is not displayed the rectangle will not appear
        if (active_)
        {
            if (dataAreaButton_->isChecked())
            {
                displayDataArea();
            }
            else
            {
                hideDataArea();
            }
        }
        else
        {
            hideDataArea(false);
            dataAreaButton_->setChecked(false);
        }
    }
}

void Channel::colorChooser()
{
    QColor newColor = QColorDialog::getColor(color_);
    if (newColor.isValid() && (newColor != color_))
    {
        Config::getInstance()->setDetectorConf(detectorType_, detectorId_,
                                               newColor.red(), newColor.green(),
                                               newColor.blue());
        updateColor();
        MainWindow::getInstance()->getCustomPlot()->replot();
    }
}

/*
 * \brief Update the element and its children
 */
void Channel::updateElement()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Update channel");
    bool plotLine = FileReader::getInstance()->isPlotLine();
    bool plotPoints = FileReader::getInstance()->isPlotPoints();

    // If the graph has been removed and the checkBox is checked, the graph has
    // to be added to the plot area
    if (checkBox_->isChecked())
    {
        graph_->setVisible(true);
    }

    if (plotLine && plotPoints)
    {
        // Line & dots
        graph_->setLineStyle(QCPGraph::lsLine);
        graph_->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 5));
    }
    else if (plotLine)
    {
        // Line only
        graph_->setLineStyle(QCPGraph::lsLine);
        graph_->setScatterStyle(QCPScatterStyle::ssNone);
    }
    else if (plotPoints)
    {
        // Dots only
        graph_->setLineStyle(QCPGraph::lsNone);
        graph_->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 5));
    }
    else
    {
        graph_->setVisible(false);
    }
    MainWindow::getInstance()->removeActionOnGoing();
}
} // namespace eventDisplay
} // namespace ntof
