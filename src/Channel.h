#ifndef CHANNEL_H
#define CHANNEL_H

#include <QObject>
#include <QSharedPointer>

#include <stdint.h>

#include "GraphDataContainer.h"
#include "qcustomplot.h"

class QCheckBox;
class QCPGraph;
class QGroupBox;
class QPushButton;

namespace ntof {
namespace lib {
class NtofLib;
}
} // namespace ntof

namespace ntof {
namespace eventDisplay {
class MainWindow;

class Channel : public QWidget
{
    Q_OBJECT
public:
    typedef QSharedPointer<GraphDataContainer> DataContainer;
    /*
     * \brief Constructor of the class
     */
    Channel();

    /*
     * \brief Destructor of the class
     */
    ~Channel();

    /* ************************************************* */
    /* GETTERS                                           */
    /* ************************************************* */
    /*
     * \brief Get the ID of the channel
     * \return Return the ID of the channel
     */
    int32_t getId();

    /*
     * \brief Get the ADC range
     * \return Return the ADC range
     */
    int32_t getADCRange();

    /*
     * \brief Get the even number
     * \return Return the event number
     */
    int32_t getEventNumber();

    /*
     * \brief Get the detector type
     * \return the detector type
     */
    std::string &getDetectorType();

    /*
     * \brief Get the detector id
     * \return the detector id
     */
    int32_t getDetectorId();

    /*
     * \brief Get the sample rate
     * \return the sample rate
     */
    float getSampleRate();

    /*
     * \brief Get the sample size
     * \return the sample size
     */
    float getSampleSize();

    /*
     * \brief Get the full scale in millivolts
     * \return the full scale in millivolts
     */
    float getFullScaleInMilliVolts();

    /*
     * \brief Get the full scale in volts
     * \return the full scale in volts
     */
    float getFullScaleInVolts();

    /*
     * \brief Get the delai time
     * \return the delai time
     */
    int32_t getDelayTime();

    /*
     * \brief Get the threshold
     * \return the threshold
     */
    float getThreshold();

    /*
     * \brief Get the offset
     * \return the offset
     */
    float getOffset();

    /*
     * \brief Get the pre samples
     * \return the pre samples
     */
    int32_t getPreSamples();

    /*
     * \brief Get the post samples
     * \return the post samples
     */
    int32_t getPostSamples();

    /*
     * \brief Get the clock state
     * \return the clock state
     */
    std::string &getClockState();

    /*
     * \brief Get the zero suppression start
     * \return the zero suppression start
     */
    int32_t getZeroSuppressionStart();

    /*
     * \brief Get the channel ID
     * \return the channel ID
     */
    uint32_t getChannel() const;

    /*
     * \brief Get the chassis ID
     * \return the chassis ID
     */
    uint32_t getChassis() const;

    /*
     * \brief Get the module ID
     * \return the module ID
     */
    uint32_t getModule() const;

    /*
     * \brief Get the stream ID
     * \return the stream ID
     */
    uint32_t getStream() const;

    /*
     * \brief Get the lower limit
     * \return the lower limit
     */
    double getLowerLimit() const;

    /*
     * \brief Get the time window
     * \return the time window
     */
    double getTimeWindow() const;

    /*
     * \brief Get the type of the module
     * \return the type of the module
     */
    const std::string &getModuleType() const;

    /*
     * \brief Get the falling or rising edge
     * \return the falling or rising edge
     */
    int32_t getThresholdSign() const;

    /*
     * \brief Get the zero suppression status
     * \return the zero suppression status
     */
    const std::string &getZeroSuppression() const;

    /*
     * \brief Get the master group
     * \return the master group
     */
    std::string getMasterGroup();

    /*
     * \brief Get the master detector type
     * \return the master detector type
     */
    const std::string &getMasterDetectorType() const;

    /*
     * \brief Get the master detector id
     * \return the master detector id
     */
    int32_t getMasterDetectorId() const;

    /*
     * \brief Get the data container
     * \return the data container
     */
    const DataContainer &getData() const;

    /*
     * \brief Get the button used to apply a custom zoom (using the
     * configuration) \return the button used to apply a custom zoom (using the
     * configuration)
     */
    QPushButton *&getAdaptViewButton();

    /*
     * \brief Get the button used to display the rectangle where the data can be
     * defined \return a pointer on the button
     */
    QPushButton *&getDataAreaButton();

    /*
     * \brief Set the event number
     * \param eventNumber: The event number to set
     */
    void setEventNumber(int32_t eventNumber);

    /*
     * \brief Update the color of the graph
     * \return Return true if the color has been modified
     */
    bool updateColor();

    /*
     * \brief Reserve a part memory
     * \param size: Size to reserve
     */
    bool reserve(int64_t size); // TODO check if the size is in bytes or in
                                // number of samples

    /*
     * \brief Add a point to the list of points
     * \param x: position in the X axis
     * \param y: position in the Y axis
     */
    void addPoint(double x, double y);

    /*
     * \brief Add the graph to the plot and to the legend
     */
    void setupGraph();

    /*
     * \brief Remove the graph from the plot area
     */
    void removeGraph();

    /*
     * \brief Allow to activate the channel (will appear on the plot area)
     */
    void setActive();

    /*
     * \brief Allow to deactivate the channel (wont appear on the plot area)
     */
    void setInactive();

    /*
     * \brief Allow to know if the channel is active (can appear on the plot
     * area) \return Return true if the channel is active
     */
    bool isActive();

    /*
     * \brief Show the parameters of this channel
     */
    void showParams();

    /*
     * \brief Hide the parameters of this channel
     */
    void hideParams();

    /*
     * \brief Allow to know if the parameters are displayed or not
     * \return Return true if the parameters are displayed
     */
    bool isParamsShown();

    /*
     * \brief Allow to know if the threshold is displayed
     * \return Return true if the threshold appears on the plot area
     */
    bool isShowThreshold() const;

    /*
     * \brief Set the threshold as should be display
     * \param showThreshold: True if the threshold must be displayed or false
     * else
     */
    void setShowThreshold(bool showThreshold);

    /*
     * \brief Show the threshold and check the check box
     */
    void showThreshold();

    /*
     * \brief Clear the data
     */
    void clear();

    /*
     * \brief Initialize the channel
     */
    void initChannel();

    /*
     * \brief Initialize the threshold by adding a dedicated graph (which
     * doesn't appear in the legend)
     */
    void initThreshold();

    /*
     * \brief Update the element and its children
     */
    void updateElement();

    /*
     * \brief Initialize the graph
     */
    void initGraph();

    /*
     * \brief Show or hide the threshold
     * \param state: true to show or false to hide
     */
    void activeThreshold(bool state);

    /*
     * \brief Convert data from mV to ADC according the parameters
     * \param data: Data to convert
     * \return Return the data converted
     */
    int32_t mvToADC(double data);

    /*
     * \brief Convert data from ADC to mV according the parameters
     * \param data: Data to convert
     * \return Return the data converted
     */
    double ADCToMv(double data);

    /*
     * \brief Display a rectangle to symbolize the area in which the data can be
     * defined \return Return true if the rectangle is displayed
     */
    bool isDisplayDataArea();

    /*
     * \brief Display a rectangle to symbolize the area in which the data can be
     * defined \param replot: The plot area is not redraw if this parameter is
     * equal to false
     */
    void displayDataArea(bool replot = true);

    /*
     * \brief Hide the rectangle used to symbolize the area in which the data
     * can be defined \param replot: The plot area is not redraw if this
     * parameter is equal to false
     */
    void hideDataArea(bool replot = true);

    /*
     *\brief Initialized the rectangle dimensions
     */
    void initDataArea();

    /*
     * \brief Get the name of the detector
     * \Return Return the detector type concatenate with the detector ID
     */
    QString getDetectorName();

public slots:
    /*
     * \brief Action to perform when the threshold button is used
     * \param state: true to show or false to hide
     */
    void showThreshold(bool state);

    /*
     * \brief Show or hide the parameters
     * \param state: true to show or false to hide
     */
    void showParams(bool state);

private slots:
    /*
     * \brief Activate or deactivate the channel
     * \param state: true to activate or false to deactivate
     */
    void active(bool state);

    /*
     * \brief Adapt the zoom according the configuration of the channel
     */
    void adaptTimeWindow(); // TODO To be renamed

    /*
     * \brief Activate or deactivate the data area
     */
    void dataArea();

    /**
     * \brief change channel color
     */
    void colorChooser();

private:
    QWidget *groupBox_;   //!< Group box containing the elements related to the
                          //!< channel
    QCheckBox *checkBox_; //!< Check box used to activate or deactivate the
                          //!< channel
    QPushButton *paramsButton_;      //!< Button to show or hide the parameters
    QPushButton *thresholdButton_;   //!< Button to show or hide the threshold
    QPushButton *adaptViewButton_;   //!< Button to adapt the zoom
    QPushButton *dataAreaButton_;    //!< Button to display the data area
    QPushButton *colorButton_;       //!< Button to change color
    QCPGraph *graph_;                //!< Graph of the data
    QCPGraph *graphThresholdMv_;     //!< Graph for the threshold in millivolts
    QCPGraph *graphThresholdADC_;    //!< Graph for the threshold in ADC
    QCPItemRect *dataArea_;          //!< Rectangle used to define the data area
    DataContainer data_;             //!< Data container
    DataContainer dataThresholdMv_;  //!< Data for the threshold in millivolts
    DataContainer dataThresholdADC_; //!< Data for the threshold in ADC
    QColor color_;                   //!< Color of the graph
    bool active_;     //!< Allow to know if the channel is active or not
    bool showParams_; //!< Allow to know if the parameters are displayed or not
    bool showThreshold_; //!< Allow to know if the threshold is displayed or not
    int32_t id_;         //!< ID of the channel
    int32_t adcRange_;   //!< ADC range
    std::string detectorType_;       //!< Detector type
    uint32_t detectorId_;            //!< Detector ID
    std::string moduleType_;         //!< Module type
    uint32_t channel_;               //!< Channel ID
    uint32_t module_;                //!< Module ID
    uint32_t chassis_;               //!< Chassis ID
    uint32_t stream_;                //!< Stream ID
    float sampleRate_;               //!< Sample rate
    float sampleSize_;               //!< Sample size
    uint32_t fullScaleInADC_;        //!< Full scale in ADC
    float fullScaleInMilliVolts_;    //!< Full scale in millivolts
    int32_t delayTime_;              //!< Delay time
    int32_t thresholdADC_;           //!< Threshold in ADC
    float thresholdMV_;              //!< Threshold in MV
    int32_t thresholdSign_;          //!< Threshold sign
    float offset_;                   //!< Offset
    uint32_t preSample_;             //!< Pre samples
    uint32_t postSample_;            //!< Post samples
    std::string clockState_;         //!< Clock state
    int32_t zeroSuppressionStart_;   //!< Zero suppression start
    std::string zeroSuppression_;    //!< Zero suppression ("on" or "off")
    std::string masterDetectorType_; //!< Master detector type
    uint32_t masterDetectorId_;      //!< Master detector id
    double timeWindow_;              //!< Time window
    double lowerLimit_;              //!< Lower limit
    int32_t eventNumber_;            //!< Event number
};
} // namespace eventDisplay
} // namespace ntof
#endif // CHANNEL_H
