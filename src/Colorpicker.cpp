#include "Colorpicker.h"

#include <iostream>

namespace ntof {
namespace eventDisplay {
ColorPicker *ColorPicker::instance_ = NULL;

/*!
 *  \brief Get the instance of the ColorPicker object and initialize the
 * instance if the pointer is null \return A pointer on the ColorPicker object
 */
ColorPicker *ColorPicker::getInstance()
{
    if (instance_ == NULL)
    {
        instance_ = new ColorPicker();
    }
    return instance_;
}

/*
 * \brief Constructor of the class
 */
ColorPicker::ColorPicker() : parent_(NULL), name_(""), id_(0) {}

/*
 * \brief Set the parent element of the color picker
 * \param parent: Parent element in the tree
 */
void ColorPicker::setParent(OptionPanel *parent)
{
    parent_ = parent;
}

/*
 * \brief Set the detector information
 * \param name: Name of the detector
 * \param id: ID of the detector
 */
void ColorPicker::setDetector(const std::string &name, uint32_t id)
{
    name_ = name;
    id_ = id;
}

/*
 * \brief Apply the result for the defined detector
 * \param result: color to apply
 */
void ColorPicker::done(int32_t result)
{
    if (result == QColorDialog::Accepted)
    {
        QColor color = QColorDialog::currentColor();
        // std::cout << "Color selected " << color.red() << " " << color.green()
        // << " " << color.blue() << std::endl;
        parent_->setColor(name_, id_, color.red(), color.green(), color.blue());
    }
    QColorDialog::done(result);
}
} // namespace eventDisplay
} // namespace ntof
