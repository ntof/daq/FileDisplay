#ifndef COLORPICKER_H
#define COLORPICKER_H

#include <QColorDialog>
#include <QObject>

#include <stdint.h>

#include "OptionPanel.h"

namespace ntof {
namespace eventDisplay {
class ColorPicker : public QColorDialog
{
    Q_OBJECT
public:
    /*!
     *  \brief Get the instance of the ColorPicker object and initialize the
     * instance if the pointer is null \return A pointer on the ColorPicker
     * object
     */
    static ColorPicker *getInstance();

    /*
     * \brief Set the detector information
     * \param name: Name of the detector
     * \param id: ID of the detector
     */
    void setDetector(const std::string &name, uint32_t id);

    /*
     * \brief Set the parent element of the color picker
     * \param parent: Parent element in the tree
     */
    void setParent(OptionPanel *parent);

    /*
     * \brief Apply the result for the defined detector
     * \param result: color to apply
     */
    void done(int32_t result);

private:
    /*
     * \brief Constructor of the class
     */
    ColorPicker();

    static ColorPicker *instance_; //!< ColorPicker instance
    OptionPanel *parent_;          //!< Parent element
    std::string name_;             //!< Detector name
    uint32_t id_;                  //!< ID of the detector
};
} // namespace eventDisplay
} // namespace ntof
#endif // COLORPICKER_H
