#include "Config.h"

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <fstream> // std::ofstream
#include <iostream>
#include <string>
#include <utility>
#include <vector>

#include <QDebug>
#include <QDir>
#include <QHostInfo>
#include <QStandardPaths>

#include "MainWindow.h"
#include "pugixml.hpp"

namespace ntof {
namespace eventDisplay {
Config *Config::instance_ = NULL;
std::string Config::configFile_;

/*!
 *  \brief Get the instance of the Config object and initialize the instance if
 * the pointer is null \return A pointer on the Config object
 */
Config *Config::getInstance()
{
    if (instance_ == NULL)
    {
        instance_ = new Config();
    }
    return instance_;
}

/* ************************************************************************** */
/* CONSTRUCTOR AND DESTRUCTOR                                                 */
/* ************************************************************************** */
/*
 * \brief Constructor of the class
 */
Config::Config()
{
    if (Config::configFile_.empty())
    {
        QDir confPath(QStandardPaths::writableLocation(
            QStandardPaths::AppConfigLocation));
        if (!confPath.exists())
            confPath.mkpath(".");
        Config::configFile_ = (confPath.absolutePath() + QDir::separator() +
                               "config-" + QHostInfo::localHostName() + ".xml")
                                  .toStdString();
        qDebug() << qPrintable("Using config file:")
                 << qPrintable(Config::configFile_.c_str());
    }
    if (Config::configFile_.find_first_of('/') != 0)
    {
        Config::configFile_ = QDir().absolutePath().toStdString() + "/" +
            Config::configFile_;
    }
    readFile();
}

void Config::init()
{
    modeDebug_ = "off";
    openGLRendering_ = 1;
    measurementHints_ = 0;
    aggregateDaqFiles_ = true;
    xrootPath_ = "xroot://eosctapublicdisk.cern.ch/eos/ctapublicdisk"
                 "/archive/ntof?svcClass=ntof";
    dataUnit_ = "mv";
    workspace_ = "/";
    userDoc_ = "/";
    distanceEAR1_ = 185.0;
    distanceEAR2_ = 19.5;
    tGamma_ = 10000;
    height_ = 600;
    width_ = 800;
    cacheDir_ = (QStandardPaths::writableLocation(QStandardPaths::TempLocation) +
                 QDir::separator() + "FileDisplay-" + QHostInfo::localHostName())
                    .toStdString();
    cacheSize_ = 10;
}

/*
 * \brief Destructor of the class
 */
Config::~Config()
{
    if (instance_)
        instance_ = 0;
}

/* ************************************************************************** */
/* GETTERS                                                                    */
/* ************************************************************************** */
/*
 * \brief Get the debug mode status
 * \return Return the debug mode status
 */
std::string &Config::getModeDebug()
{
    return modeDebug_;
}

/*
 * \brief Get the data unit to apply
 * \return Return the data unit to apply
 */
std::string &Config::getDataUnit()
{
    return dataUnit_;
}

/*
 * \brief Get the path of the workspace
 * \return Return the path of the workspace
 */
std::string &Config::getWorkspace()
{
    return workspace_;
}

/*
 * \brief Get the list of the detectors found
 * \return Return the list of the detectors found
 */
const std::vector<Detector> &Config::getDetectors()
{
    std::sort(detectors_.begin(), detectors_.end());
    return detectors_;
}

/*
 * \brief Get the full path of the user documentation
 * \return Return the full path of the user documentation
 */
std::string &Config::getUserDoc()
{
    return userDoc_;
}

/*
 * \brief Get the configuration of a particular detector
 * \param detectorName: Name of the detector
 * \param detectorId: ID of the detector
 * \return Return the configuration of a particular detector
 */
Detector Config::getDetectorConf(const std::string &detectorName,
                                 uint32_t detectorId)
{
    for (std::vector<Detector>::iterator detector = detectors_.begin();
         detector != detectors_.end(); ++detector)
    {
        if ((*detector).name == detectorName && (*detector).id == detectorId)
        {
            return (*detector);
        }
    }
    return Detector();
}

/*
 * \brief Get the distance between the target and the detectors of EAR1
 * \return Return the distance between the target and the detectors of EAR1
 */
double Config::getDistanceEAR1()
{
    return distanceEAR1_;
}

/*
 * \brief Get the distance between the target and the detectors of EAR2
 * \return Return the distance between the target and the detectors of EAR2
 */
double Config::getDistanceEAR2()
{
    return distanceEAR2_;
}

/*
 * \brief Get the value of T gamma
 * \return Return the value of T gamma
 */
int32_t Config::getTGamma()
{
    return tGamma_;
}

/*
 * \brief Get the value of height
 * \return Return the value of height
 */
int32_t Config::getHeight()
{
    return height_;
}

/*
 * \brief Get the value of width
 * \return Return the value of width
 */
int32_t Config::getWidth()
{
    return width_;
}

/* ************************************************************************** */
/* SETTERS                                                                    */
/* ************************************************************************** */
/*
 * \brief Set the mode debug status
 * \param modeDebug: status to set
 */
void Config::setModeDebug(const std::string &modeDebug)
{
    modeDebug_ = modeDebug;
}

/*
 * \brief Set the data unit
 * \param dataUnit: the data unit to set
 */
void Config::setDataUnit(const std::string &dataUnit)
{
    dataUnit_ = dataUnit;
}

/*
 * \brief Set the full path of the workspace
 * \param workspace: full path of the workspace to set
 */
void Config::setWorkspace(const std::string &workspace)
{
    workspace_ = workspace;
}

/*
 * \brief Set the list of detectors
 * \param detectors: list of detectors to set
 */
void Config::setDetectors(const std::vector<Detector> &detectors)
{
    detectors_ = detectors;
}

/*
 * \brief Set the full path of the user documentation
 * \param userDoc: full path of the user documentation to set
 */
void Config::setUserDoc(const std::string &userDoc)
{
    userDoc_ = userDoc;
}

/*
 * \brief Set the configuration of a detector
 * \param detectorName: name of the detector to set
 * \param detectorId: id of the detector to set
 * \param red: red value to set
 * \param green: green value to set
 * \param blue: blue value to set
 */
void Config::setDetectorConf(const std::string &detectorName,
                             uint32_t detectorId,
                             int32_t red,
                             int32_t green,
                             int32_t blue)
{
    for (std::vector<Detector>::iterator detector = detectors_.begin();
         detector != detectors_.end(); ++detector)
    {
        if ((*detector).name == detectorName && (*detector).id == detectorId)
        {
            (*detector).red = red;
            (*detector).green = green;
            (*detector).blue = blue;
            return;
        }
    }

    Detector detector;
    detector.name = detectorName;
    detector.id = detectorId;
    detector.red = red;
    detector.green = green;
    detector.blue = blue;
    detectors_.push_back(detector);
    writeFile();
}

/*
 * \brief Set the distance between the target and the detectors of EAR1
 * \param distanceEAR1: distance to set
 */
void Config::setDistanceEAR1(double distanceEAR1)
{
    distanceEAR1_ = distanceEAR1;
}

/*
 * \brief Set the distance between the target and the detectors of EAR2
 * \param distanceEAR2: distance to set
 */
void Config::setDistanceEAR2(double distanceEAR2)
{
    distanceEAR2_ = distanceEAR2;
}

/*
 * \brief Set the tGamma value
 * \param tGamma: value to set
 */
void Config::setTGamma(int32_t tGamma)
{
    tGamma_ = tGamma;
}

/*
 * \brief Set the height value
 * \param height: value to set
 */
void Config::setHeight(int32_t height)
{
    height_ = height;
}

/*
 * \brief Set the width value
 * \param width: value to set
 */
void Config::setWidth(int32_t width)
{
    width_ = width;
}

void Config::setCacheDir(const std::string &cacheDir)
{
    cacheDir_ = cacheDir;
}

void Config::setCacheSize(size_t size)
{
    cacheSize_ = size;
}

void Config::setOpenGLRendering(bool state)
{
    openGLRendering_ = state;
}

void Config::setMeasurementHints(bool state)
{
    measurementHints_ = state;
}

void Config::setAggregateDaqFiles(bool value)
{
    aggregateDaqFiles_ = value;
}

/* ************************************************************************** */
/* WRITER                                                                     */
/* ************************************************************************** */
/*
 * \brief Update the configuration file
 * \return Return true if everything is OK
 */
bool Config::writeFile()
{
    MainWindow::getInstance()->setDebugMessage("Update the configuration file");
    std::ofstream file;
    file.open(Config::configFile_.c_str());
    if (file)
    {
        file << "<?xml version=\"1.0\"?>" << std::endl;
        file << "<fileDisplay>" << std::endl;
        file << "    <modeDebug value=\"" << modeDebug_ << "\" />" << std::endl;
        file << "    <dataUnit value=\"" << dataUnit_ << "\" />" << std::endl;
        file << "    <workspace value=\"" << workspace_ << "\" />" << std::endl;
        file << "    <userDoc value=\"" << userDoc_ << "\" />" << std::endl;
        file << "    <distanceEAR1 value=\"" << distanceEAR1_ << "\" />"
             << std::endl;
        file << "    <distanceEAR2 value=\"" << distanceEAR2_ << "\" />"
             << std::endl;
        file << "    <tGamma value=\"" << tGamma_ << "\" />" << std::endl;
        file << "    <height value=\"" << height_ << "\" />" << std::endl;
        file << "    <width value=\"" << width_ << "\" />" << std::endl;
        file << "    <cacheDir value=\"" << cacheDir_ << "\" />" << std::endl;
        file << "    <cacheSize value=\"" << cacheSize_ << "\" />" << std::endl;
        file << "    <openGLRendering value=\"" << (openGLRendering_ ? 1 : 0)
             << "\" />" << std::endl;
        file << "    <measurementHints value=\"" << (measurementHints_ ? 1 : 0)
             << "\" />" << std::endl;
        file << "    <aggregateDaqFiles value=\""
             << (aggregateDaqFiles_ ? 1 : 0) << "\" />" << std::endl;
        file << "    <xrootPath value=\"" << xrootPath_ << "\" />" << std::endl;

        file << "    <detectors>" << std::endl;
        for (std::vector<Detector>::iterator detector = detectors_.begin();
             detector != detectors_.end(); ++detector)
        {
            file << "        <detector ";
            file << "name=\"" << (*detector).name << "\" ";
            file << "id=\"" << (*detector).id << "\" ";
            file << "red=\"" << (*detector).red << "\" ";
            file << "green=\"" << (*detector).green << "\" ";
            file << "blue=\"" << (*detector).blue << "\" ";
            file << "/>" << std::endl;
        }
        file << "    </detectors>" << std::endl;
        file << "</fileDisplay>" << std::endl;

        file.close();
    }
    else
    {
        return false;
    }
    emit updated();
    return true;
}

/* ************************************************************************** */
/* READER                                                                     */
/* ************************************************************************** */
/*
 * \brief Read the configuration file
 * \return Return true if everything is OK
 */
bool Config::readFile()
{
    init();
    pugi::xml_document doc;

    pugi::xml_parse_result result = doc.load_file(Config::configFile_.c_str());
    if (result != 0)
    {
        modeDebug_ = doc.child("fileDisplay")
                         .child("modeDebug")
                         .attribute("value")
                         .as_string(modeDebug_.c_str());
        dataUnit_ = doc.child("fileDisplay")
                        .child("dataUnit")
                        .attribute("value")
                        .as_string(dataUnit_.c_str());
        workspace_ = doc.child("fileDisplay")
                         .child("workspace")
                         .attribute("value")
                         .as_string(workspace_.c_str());
        userDoc_ = doc.child("fileDisplay")
                       .child("userDoc")
                       .attribute("value")
                       .as_string(userDoc_.c_str());
        distanceEAR1_ = doc.child("fileDisplay")
                            .child("distanceEAR1")
                            .attribute("value")
                            .as_double(distanceEAR1_);
        distanceEAR2_ = doc.child("fileDisplay")
                            .child("distanceEAR2")
                            .attribute("value")
                            .as_double(distanceEAR2_);
        tGamma_ = doc.child("fileDisplay")
                      .child("tGamma")
                      .attribute("value")
                      .as_int(tGamma_);
        height_ = doc.child("fileDisplay")
                      .child("height")
                      .attribute("value")
                      .as_int(height_);
        width_ = doc.child("fileDisplay")
                     .child("width")
                     .attribute("value")
                     .as_int(width_);
        cacheDir_ = doc.child("fileDisplay")
                        .child("cacheDir")
                        .attribute("value")
                        .as_string(cacheDir_.c_str());
        cacheSize_ = doc.child("fileDisplay")
                         .child("cacheSize")
                         .attribute("value")
                         .as_ullong(cacheSize_); /* 10GB */
        openGLRendering_ = doc.child("fileDisplay")
                               .child("openGLRendering")
                               .attribute("value")
                               .as_int(1);
        measurementHints_ = doc.child("fileDisplay")
                                .child("measurementHints")
                                .attribute("value")
                                .as_int(0);
        aggregateDaqFiles_ = doc.child("fileDisplay")
                                 .child("aggregateDaqFiles")
                                 .attribute("value")
                                 .as_int(1);
        xrootPath_ = doc.child("fileDisplay")
                         .child("xrootPath")
                         .attribute("value")
                         .as_string(xrootPath_.c_str());

        pugi::xml_node detectors = doc.child("fileDisplay").child("detectors");
        for (pugi::xml_node nodes = detectors.first_child(); nodes;
             nodes = nodes.next_sibling())
        {
            if (strcmp(nodes.name(), "detector") == 0)
            {
                Detector detector;
                detector.name = nodes.attribute("name").as_string("");
                detector.id = nodes.attribute("id").as_uint(0);
                detector.red = nodes.attribute("red").as_int(0);
                detector.green = nodes.attribute("green").as_int(0);
                detector.blue = nodes.attribute("blue").as_int(0);
                detectors_.push_back(detector);
            }
        }
    }
    else
        return false;

    return true;
}
} // namespace eventDisplay
} // namespace ntof
