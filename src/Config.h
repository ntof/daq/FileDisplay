#ifndef CONFIG_H_
#define CONFIG_H_

#include <map>
#include <sstream>
#include <string>
#include <vector>

#include <QObject>

#include <stdint.h>
#include <sys/types.h>

namespace ntof {
namespace eventDisplay {
struct Detector
{
    std::string name;
    uint32_t id;
    int32_t red;
    int32_t green;
    int32_t blue;

    Detector() : name(""), id(0), red(0), green(0), blue(0) {}

    bool operator<(const Detector &a) const
    {
        if (name != a.name)
            return name < a.name;
        else if (id != a.id)
            return id < a.id;
        else if (red != a.red)
            return red < a.red;
        else if (green != a.green)
            return green < a.green;
        else if (blue != a.blue)
            return blue < a.blue;
        else
            return false; /* == */
    }

    bool operator==(const Detector &a) const
    {
        return (name == a.name) && (id == a.id) && (red == a.red) &&
            (green == a.green) && (blue == a.blue);
    }
};

class Config : public QObject
{
    Q_OBJECT
public:
    static std::string configFile_; //!< Full path of the configuration file

    /*!
     *  \brief Get the instance of the Config object and initialize the instance
     * if the pointer is null \return A pointer on the Config object
     */
    static Config *getInstance();

    /*
     * \brief Destructor of the class
     */
    virtual ~Config();

    /* *********************************** */
    /* GETTERS                             */
    /* *********************************** */
    /*
     * \brief Get the debug mode status
     * \return Return the debug mode status
     */
    std::string &getModeDebug();

    /*
     * \brief Get the data unit to apply
     * \return Return the data unit to apply
     */
    std::string &getDataUnit();

    /*
     * \brief Get the path of the workspace
     * \return Return the path of the workspace
     */
    std::string &getWorkspace();

    /*
     * \brief Get the list of the detectors found
     * \return Return the list of the detectors found
     */
    const std::vector<Detector> &getDetectors();

    /*
     * \brief Get the full path of the user documentation
     * \return Return the full path of the user documentation
     */
    std::string &getUserDoc();

    /*
     * \brief Get the configuration of a particular detector
     * \param detectorName: Name of the detector
     * \param detectorId: ID of the detector
     * \return Return the configuration of a particular detector
     */
    Detector getDetectorConf(const std::string &detectorName,
                             uint32_t detectorId);

    /*
     * \brief Get the distance between the target and the detectors of EAR1
     * \return Return the distance between the target and the detectors of EAR1
     */
    double getDistanceEAR1();

    /*
     * \brief Get the distance between the target and the detectors of EAR2
     * \return Return the distance between the target and the detectors of EAR2
     */
    double getDistanceEAR2();

    /*
     * \brief Get the value of T gamma
     * \return Return the value of T gamma
     */
    int32_t getTGamma();

    /*
     * \brief Get the value of height
     * \return Return the value of height
     */
    int32_t getHeight();

    /*
     * \brief Get the value of width
     * \return Return the value of width
     */
    int32_t getWidth();

    /* *********************************** */
    /* SETTERS                             */
    /* *********************************** */
    /*
     * \brief Set the mode debug status
     * \param modeDebug: status to set
     */
    void setModeDebug(const std::string &modeDebug);

    /*
     * \brief Set the data unit
     * \param dataUnit: the data unit to set
     */
    void setDataUnit(const std::string &dataUnit);

    /*
     * \brief Set the full path of the workspace
     * \param workspace: full path of the workspace to set
     */
    void setWorkspace(const std::string &workspace);

    /*
     * \brief Set the list of detectors
     * \param detectors: list of detectors to set
     */
    void setDetectors(const std::vector<Detector> &detectors);

    /*
     * \brief Set the full path of the user documentation
     * \param userDoc: full path of the user documentation to set
     */
    void setUserDoc(const std::string &userDoc);

    /*
     * \brief Set the configuration of a detector
     * \param detectorName: name of the detector to set
     * \param detectorId: id of the detector to set
     * \param red: red value to set
     * \param green: green value to set
     * \param blue: blue value to set
     */
    void setDetectorConf(const std::string &detectorName,
                         uint32_t detectorId,
                         int32_t red,
                         int32_t green,
                         int32_t blue);

    /*
     * \brief Set the distance between the target and the detectors of EAR1
     * \param distanceEAR1: distance to set
     */
    void setDistanceEAR1(double distanceEAR1);

    /*
     * \brief Set the distance between the target and the detectors of EAR2
     * \param distanceEAR2: distance to set
     */
    void setDistanceEAR2(double distanceEAR2);

    /*
     * \brief Set the T gamma value
     * \param tGamma: value to set
     */
    void setTGamma(int32_t tGamma);

    /*
     * \brief Set the height value
     * \param height: value to set
     */
    void setHeight(int32_t height);

    /*
     * \brief Set the width value
     * \param width: value to set
     */
    void setWidth(int32_t width);

    /*
     * \brief Update the configuration file
     * \return Return true if everything is OK
     */
    bool writeFile();

    /*
     * \brief Read the configuration file
     * \return Return true if everything is OK
     */
    bool readFile();

    /**
     * @brief get cache directory
     * @details cache directory is used by ntofutil's FileCache to store
     * temporary files.
     */
    inline const std::string &getCacheDir() const { return cacheDir_; }
    void setCacheDir(const std::string &cacheDir);

    /**
     * @brief get cache size
     * @return maximum cache size in bytes
     */
    inline size_t getCacheSize() const { return cacheSize_; }
    void setCacheSize(size_t size);

    /**
     * @brief get openGL rendering option
     * @details it does not mean that openGL is working, just that
     * the QCustomPlot widget will try to create a GL context.
     */
    inline bool getOpenGLRendering() const { return openGLRendering_; }
    void setOpenGLRendering(bool state);

    /**
     * @brief get and set Zoom ToolTips Mode
     */
    inline bool getMeasurementHints() const { return measurementHints_; }
    void setMeasurementHints(bool state);

    /**
     * @brief whereas we should look for files on local daqs
     * @details this is effective only when the current file is opened from
     * a daq.
     */
    inline bool isAggregateDaqFiles() const { return aggregateDaqFiles_; }
    void setAggregateDaqFiles(bool value);

    const std::string &getXRootPath() const { return xrootPath_; }
    void setXRootPath(const std::string &path) { xrootPath_ = path; };

signals:
    void updated();

private:
    /*
     * \brief Constructor of the class
     */
    Config();

    void init();

    static Config *instance_;         //!< Config instance
    std::string modeDebug_;           //!< Debug mode status
    std::string dataUnit_;            //!< Data unit
    std::string workspace_;           //!< Full path of the workspace
    std::vector<Detector> detectors_; //!< List of the detectors colors
                                      //!< configurations
    std::string userDoc_;             //!< Full path to the user documentation
    double distanceEAR1_; //!< Distance between the target and the detectors of
                          //!< EAR1
    double distanceEAR2_; //!< Distance between the target and the detectors of
                          //!< EAR2
    int32_t tGamma_; //!< Value of the time reference of the energy calculations
    int32_t height_; //!< Height of the main window
    int32_t width_;  //!< Width of the main window
    std::string cacheDir_;   //!< Cache directory for castor
    size_t cacheSize_;       //!< Cache directory size in bytes
    bool openGLRendering_;   //!< Is openGL rendering enabled
    bool measurementHints_;  //!< Are tooltips on the zoom lasso enabled
    bool aggregateDaqFiles_; //!< Look for files on other daqs
    std::string xrootPath_;  //!< default path for XRootFileDialog
};

} // namespace eventDisplay
} // namespace ntof

#endif /* CONFIG_H_ */
