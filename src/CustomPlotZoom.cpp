
#include "CustomPlotZoom.h"

#include <iostream>

#include <QKeyEvent>

#include "FileReader.h"
#include "MainWindow.h"
#include "MenuBar.h"

namespace ntof {
namespace eventDisplay {
/* **************************************************** */
/*                  ZOOM STRUCTURE                      */
/* **************************************************** */
const double Zoom::SimilarThreshold = .1;
const int CustomPlotZoom::ZoomWatchDelay = 1500;
const double CustomPlotZoom::KeyboardMoveFactor = .1;

Zoom::Zoom() {}

Zoom::Zoom(double x1, double x2, double y1, double y2) :
    xAxis(x1, x2), yAxis(y1, y2)
{}

Zoom::Zoom(const QCPRange &xAxis, const QCPRange &yAxis) :
    xAxis(xAxis), yAxis(yAxis)
{}

bool Zoom::similarTo(const Zoom &other, double threshold) const
{
    if ((qAbs(xAxis.lower - other.xAxis.lower) > qAbs(xAxis.lower * threshold)) ||
        (qAbs(xAxis.upper - other.xAxis.upper) > qAbs(xAxis.upper * threshold)) ||
        (qAbs(yAxis.lower - other.yAxis.lower) > qAbs(yAxis.lower * threshold)) ||
        (qAbs(yAxis.upper - other.yAxis.upper) > qAbs(yAxis.upper * threshold)))
        return false;
    else
        return true;
}

bool Zoom::operator==(const Zoom &other) const
{
    return (xAxis == other.xAxis) && (yAxis == other.yAxis);
}

/* **************************************************** */
/*                  POINT INFO CLASS                    */
/* **************************************************** */
const int PointInfo::s_textBoxMargin = 5;

PointInfo::PointInfo(QCustomPlot *cp,
                     double x,
                     double y,
                     const std::string &xPrefix,
                     const std::string &yPrefix,
                     double xData,
                     double yData,
                     const std::string &xSuffix,
                     const std::string &ySuffix) :
    QCPItemText(cp),
    m_cp(cp),
    m_X(x),
    m_Y(y),
    m_xPrefix(xPrefix),
    m_yPrefix(yPrefix),
    m_Xdata(xData),
    m_Ydata(yData),
    m_xSuffix(xSuffix),
    m_ySuffix(ySuffix)
{
    if (m_cp)
    {
        setLayer("overlay");
        setPositionAlignment(Qt::AlignTop | Qt::AlignCenter);
        setTextAlignment(Qt::AlignLeft);
        position->setType(QCPItemPosition::ptPlotCoords);
        setBrush(
            QBrush(QColor(255, 255, 255, 204))); // white with 80% trasparency
        setPen(QPen(QBrush(QColor(245, 245, 245, 204)), 5, Qt::SolidLine,
                    Qt::RoundCap, Qt::RoundJoin));
        setPadding(QMargins(s_textBoxMargin, s_textBoxMargin, s_textBoxMargin,
                            s_textBoxMargin));
        setClipToAxisRect(false);
        position->setAxes(m_cp->xAxis, m_cp->yAxis);
        setFont(m_cp->font());
        setVisible(false);
        setText("");
    }
    else
        std::cerr << "PoinInfo Error: Passed a NULL QCustomPlot object to "
                     "QCPItemText constructor"
                  << std::endl;
}

double PointInfo::getXcoord() const
{
    return m_cp->xAxis->pixelToCoord(m_X);
}

double PointInfo::getYcoord() const
{
    return m_cp->yAxis->pixelToCoord(m_Y);
}

void PointInfo::updateInfo()
{
    setText(
        QString::fromStdString(m_xPrefix) + QString::number(m_Xdata, 'e', 6) +
        QString::fromStdString(m_xSuffix) + "\n" +
        QString::fromStdString(m_yPrefix) + QString::number(m_Ydata, 'e', 6) +
        QString::fromStdString(m_ySuffix));
}

void PointInfo::setInfo(double x, double y)
{
    m_Xdata = x;
    m_Ydata = y;
    updateInfo();
}

void PointInfo::setPosition(double x, double y)
{
    m_X = x;
    m_Y = y;
    position->setCoords(m_cp->xAxis->pixelToCoord(m_X),
                        m_cp->yAxis->pixelToCoord(m_Y));
    updateInfo();
}

void PointInfo::setPrefixes(const std::string &xPrf, const std::string &yPrf)
{
    m_xPrefix = xPrf;
    m_yPrefix = yPrf;
    updateInfo();
}

void PointInfo::setSuffixes(const std::string &xSff, const std::string &ySff)
{
    m_xSuffix = xSff;
    m_ySuffix = ySff;
    updateInfo();
}

bool PointInfo::isFarFrom(const PointInfo &other) const
{
    QSize BoundingRect1 = m_cp->fontMetrics().size(Qt::TextExpandTabs, text());
    QSize BoundingRect2 = m_cp->fontMetrics().size(Qt::TextExpandTabs,
                                                   other.text());

    if (qAbs(m_X - other.m_X) >
            ((BoundingRect1.width() + BoundingRect2.width()) / 2) +
                s_textBoxMargin ||
        qAbs(m_Y - other.m_Y) >
            ((BoundingRect2.height() + BoundingRect2.height()) / 2) +
                s_textBoxMargin)
        return true;
    else
        return false;
}

/* **************************************************** */
/*              CUSTOM PLOT ZOOM CLASS                  */
/* **************************************************** */
/*
 * \brief Constructor of the class
 */
CustomPlotZoom::CustomPlotZoom() :
    QCustomPlot(MainWindow::getInstance()),
    m_isDragging(false),
    mZoomMode(false), /* will be modified in constructor */
    m_savedMode(true),
    m_measurementHints(false),
    historyIdx_(0),
    title_(new QCPTextElement(this, "")),
    startZoomPoint_(new PointInfo(this, 0, 0, "", "")),
    endZoomPoint_(new PointInfo(this, 0, 0, "", "")),
    // This is delta unicode character
    midZoomPoint_(new PointInfo(this, 0, 0, "\u0394t  ", "\u0394V "))
{
    connect(Config::getInstance(), SIGNAL(updated()), this,
            SLOT(onConfigUpdate()));
    onConfigUpdate();

    connect(this, SIGNAL(openglDetect()), this,
            SLOT(openglRenderingDetection()));

    setMultiSelectModifier(Qt::NoModifier);
    setInteractions(QCP::iRangeZoom | QCP::iSelectPlottables |
                    QCP::iSelectItems | QCP::iSelectAxes | QCP::iSelectLegend);
    xAxis->setNumberPrecision(10);
    // xAxis->setNumberFormat("g");
    plotLayout()->insertRow(0);
    plotLayout()->addElement(0, 0, title_);
    setInitialZoom(Zoom(xAxis->range(), yAxis->range()));
    setZoomMode(true);
    setMeasurementHints(Config::getInstance()->getMeasurementHints());
}

/*
 * \brief Destructor of the class
 */
CustomPlotZoom::~CustomPlotZoom()
{
    delete title_;
    delete startZoomPoint_;
    delete endZoomPoint_;
    delete midZoomPoint_;
}

/*
 * \brief According the mode enabled, the cursor appearance change
 */
void CustomPlotZoom::changeCursor()
{
    if (mZoomMode)
        setCursor(Qt::CrossCursor);
    else
        setCursor(Qt::SizeAllCursor);
}

/*
 * \brief Enable or disable the ToolTips option for the lasso zoomimg
 * tool \param mode: true to enable the option or false to disable it
 */
void CustomPlotZoom::setMeasurementHints(bool mode)
{
    m_measurementHints = mode;
}

/*
 * \brief Enable or disable the zoom mode
 * \param mode: true to enable the zoom mode or false to disable it
 */
void CustomPlotZoom::setZoomMode(bool mode)
{
    if (mode == mZoomMode)
        return;

    mZoomMode = mode;

    if (mode)
    {
        setSelectionRectMode(QCP::srmZoom);
        setInteractions(interactions() & ~QCP::iRangeDrag);
    }
    else
    {
        setSelectionRectMode(QCP::srmNone);
        setInteractions(interactions() | QCP::iRangeDrag);
    }
    changeCursor();
    emit zoomModeChanged(mZoomMode);
}

void CustomPlotZoom::mousePressEvent(QMouseEvent *event)
{
    m_savedMode = mZoomMode;
    if (event->modifiers() & Qt::ControlModifier)
    {
        setZoomMode(!mZoomMode);
    }
    if (event->button() == Qt::RightButton)
    {
        resetZoom(false);
    }

    if (m_measurementHints && mZoomMode)
    {
        startZoomPoint_->setPosition(event->pos().x(), event->pos().y());
        startZoomPoint_->setInfo(startZoomPoint_->getXcoord(),
                                 startZoomPoint_->getYcoord());
    }
    QCustomPlot::mousePressEvent(event);
}

void CustomPlotZoom::keyPressEvent(QKeyEvent *event)
{
    if (!event)
        return;

    int key = event->key();
    switch (key)
    {
    case Qt::Key_Escape: {
        if (mZoomMode)
        {
            setSelectionRectMode(QCP::srmNone);
            startZoomPoint_->setVisible(false);
            endZoomPoint_->setVisible(false);
            midZoomPoint_->setVisible(false);
            replot(QCustomPlot::rpQueuedReplot);
            setSelectionRectMode(QCP::srmZoom);
        }
        break;
    }
    case Qt::Key_Plus:
    case Qt::Key_Minus: {
        QCPAxisRect *rect = axisRect();
        if (rect->rangeZoom() & Qt::Horizontal)
        {
            QCPRange range = xAxis->range();
            double size = range.size() * KeyboardMoveFactor;
            if (key == Qt::Key_Plus)
                size = -size;
            xAxis->setRange(range.lower - size, range.upper + size);
        }

        if (rect->rangeZoom() & Qt::Vertical)
        {
            QCPRange range = yAxis->range();
            double size = range.size() * KeyboardMoveFactor;
            if (key == Qt::Key_Plus)
                size = -size;
            yAxis->setRange(range.lower - size, range.upper + size);
        }

        replot(QCustomPlot::rpQueuedReplot);
        m_zoomWatch.start(ZoomWatchDelay, this);
    }
    break;

    case Qt::Key_Up:
    case Qt::Key_Down: {
        const QCPRange range = yAxis->range();
        double size = range.size() * KeyboardMoveFactor;
        if (key == Qt::Key_Down)
            size = -size;

        yAxis->setRange(range.lower + size, range.upper + size);
        replot(QCustomPlot::rpQueuedReplot);
        m_zoomWatch.start(ZoomWatchDelay, this);
    }
    break;

    case Qt::Key_Left:
    case Qt::Key_Right: {
        const QCPRange range = xAxis->range();
        double size = range.size() * KeyboardMoveFactor;
        if (key == Qt::Key_Left)
            size = -size;

        xAxis->setRange(range.lower + size, range.upper + size);
        replot(QCustomPlot::rpQueuedReplot);
        m_zoomWatch.start(ZoomWatchDelay, this);
    }
    break;

    default: break;
    }
    QCustomPlot::keyPressEvent(event);
}

void CustomPlotZoom::mouseMoveEvent(QMouseEvent *event)
{
    if (m_measurementHints &&
        selectionRect()->isActive()) // if a selection rect interaction was
                                     // initiated
    {
        startZoomPoint_->setVisible(true);

        endZoomPoint_->setPosition(event->pos().x(), event->pos().y());
        endZoomPoint_->setInfo(endZoomPoint_->getXcoord(),
                               endZoomPoint_->getYcoord());

        if (startZoomPoint_->isFarFrom((*endZoomPoint_)))
        {
            endZoomPoint_->setVisible(true);

            midZoomPoint_->setInfo(
                qAbs(startZoomPoint_->getXcoord() - endZoomPoint_->getXcoord()),
                qAbs(startZoomPoint_->getYcoord() - endZoomPoint_->getYcoord()));
            midZoomPoint_->setPosition(
                (startZoomPoint_->getXpixel() + endZoomPoint_->getXpixel()) / 2,
                (startZoomPoint_->getYpixel() + endZoomPoint_->getYpixel()) / 2);

            if (midZoomPoint_->isFarFrom((*startZoomPoint_)) &&
                midZoomPoint_->isFarFrom((*endZoomPoint_)))
                midZoomPoint_->setVisible(true);
            else
                midZoomPoint_->setVisible(false);
        }
        else
        {
            endZoomPoint_->setVisible(false);
            midZoomPoint_->setVisible(false);
        }
        replot(QCustomPlot::rpQueuedReplot);
    }

    if (!isZoomMode() && (event->buttons() & Qt::LeftButton))
    {
        m_isDragging = true;
        MainWindow::getInstance()->setDebugMessage("Drag'n'drop");
        setCursor(Qt::WaitCursor);
    }
    else
    {
        changeCursor();
    }

    QCustomPlot::mouseMoveEvent(event);
    // Energy calculations
    double x = xAxis->pixelToCoord(event->pos().x());
    double y = yAxis->pixelToCoord(event->pos().y());
    MainWindow::getInstance()->setCoords(x, y);
    MainWindow::getInstance()->energyCalculation(
        xAxis->pixelToCoord(event->pos().x()));
}

void CustomPlotZoom::mouseReleaseEvent(QMouseEvent *event)
{
    if (m_measurementHints)
    {
        startZoomPoint_->setVisible(false);
        endZoomPoint_->setVisible(false);
        midZoomPoint_->setVisible(false);
    }

    if (m_isDragging)
    {
        m_zoomWatch.start(ZoomWatchDelay, this);
        m_isDragging = false;
    }
    QCustomPlot::mouseReleaseEvent(event);
    changeCursor();
    setZoomMode(m_savedMode);
}

/*
 * \brief Listener of the mouse wheel event
 * \param event: mouse event
 */
void CustomPlotZoom::wheelEvent(QWheelEvent *event)
{
    emit mouseWheel(event);

    MainWindow::getInstance()->setCursor(Qt::WaitCursor);
    MainWindow::getInstance()->setDebugMessage("Zoom");

    /* calling QCustomPlot::wheelEvent is very slow since it calls selectTest on
     * QCPGraph which does a lot of maths */
    /* from QCPAxisRect::wheelEvent (which is protected) */
    QCPAxisRect *rect = axisRect();
    double wheelSteps = event->delta() /
        120.0; // a single step delta is +/-120 usually
    double factor;
    QList<QCPAxis *> axes;
    if (rect->rangeZoom() & Qt::Horizontal)
    { /* horizontal zoom */
        factor = qPow(rect->rangeZoomFactor(Qt::Horizontal), wheelSteps);
        axes = rect->rangeZoomAxes(Qt::Horizontal);
        for (QCPAxis *ax : axes)
        {
            if (ax)
                ax->scaleRange(factor, ax->pixelToCoord(event->pos().x()));
        }
    }

    if (rect->rangeZoom() & Qt::Vertical)
    { /* horizontal zoom */
        factor = qPow(rect->rangeZoomFactor(Qt::Vertical), wheelSteps);
        axes = rect->rangeZoomAxes(Qt::Vertical);
        for (QCPAxis *ax : axes)
        {
            if (ax)
                ax->scaleRange(factor, ax->pixelToCoord(event->pos().y()));
        }
    }
    replot(QCustomPlot::rpQueuedReplot);
    m_zoomWatch.start(ZoomWatchDelay, this);

    FileReader::getInstance()->disabledButtonsAdaptedView();
    MainWindow::getInstance()->setCursor(Qt::ArrowCursor);
}

void CustomPlotZoom::processRectZoom(QRect rect, QMouseEvent *event)
{
    Q_UNUSED(event);
    QCustomPlot::processRectZoom(rect, event);
    addToHistory(Zoom(xAxis->range(), yAxis->range()));
}

void CustomPlotZoom::onConfigUpdate()
{
    Config *cfg = Config::getInstance();
    setMeasurementHints(cfg->getMeasurementHints());
    bool openGlopt = cfg->getOpenGLRendering();
    std::string tmp_str = ((openGlopt) ? "on" : "off");
    std::string ySuff = (cfg->getDataUnit() == "mv") ? " mV" : " (ADC)";
    startZoomPoint_->setSuffixes(" ns", ySuff);
    endZoomPoint_->setSuffixes(" ns", ySuff);
    midZoomPoint_->setSuffixes(" ns", ySuff);

    if (openGlopt != openGl()) // QCustomPlot::openGl()
    {
        qDebug() << qPrintable("Turning openGL rendering: ") << tmp_str.c_str();
        MainWindow::getInstance()->setDebugMessage(
            "Turning openGL rendering: " + tmp_str);
        setOpenGl(openGlopt, 16);
        emit openglDetect();
    }
    else
        replot(QCustomPlot::rpQueuedReplot);
}

/*
 * \brief Allow detecting if OpenGL Rendering is available or not
 */
void CustomPlotZoom::openglRenderingDetection()
{
    Config *cfg = Config::getInstance();
    MainWindow *MnW = MainWindow::getInstance();
    bool openGlopt = cfg->getOpenGLRendering();

    /*  Using QCustomPlot::openGl(), you can test whether OpenGL rendering was
     *  successful or not.
     */
    if (openGlopt)
    {
        if (openGl())
            MnW->setWindowTitle(tr("File display [GL]"));
        else
        {
            setOpenGl(false);
            MnW->setDebugMessage(
                "Error in OpenGL initialization, OpenGL disabled");
            std::cerr << "Error in OpenGL initialization, OpenGL disabled"
                      << std::endl;
        }
    }
    else
    {
        if (!openGl())
            MnW->setWindowTitle(tr("File display"));
        else
        {
            QMessageBox::critical(
                this, QObject::tr("OpenGL"),
                QObject::tr("Error in OpenGL disabling, OpenGL enabled"),
                QMessageBox::Ok);
            std::cerr << "Error in OpenGL disabling, OpenGL enabled"
                      << std::endl;
        }
    }
    replot(QCustomPlot::rpQueuedReplot);
}

/*
 * \brief Reset the zoom mode
 */
void CustomPlotZoom::resetZoom(bool clearHistory)
{
    m_zoomWatch.stop();
    historyIdx_ = 0;
    if (clearHistory)
    {
        history_.clear();
        history_.push_back(origins_);
    }
    xAxis->setRange(origins_.xAxis);
    yAxis->setRange(origins_.yAxis);
    replot(QCustomPlot::rpQueuedReplot);
    emit zoomHistoryChanged();
}

/*
 * \brief Add a zoom to the history list
 * \param zoom: zoom rectangle
 */
void CustomPlotZoom::addToHistory(const Zoom &zoom)
{
    qDebug() << qPrintable("recording zoom:") << "x:" << zoom.xAxis
             << "y:" << zoom.yAxis;
    m_zoomWatch.stop();
    history_.resize(++historyIdx_);
    history_.push_back(zoom);
    emit zoomHistoryChanged();
}

/*
 * \brief Set the previous zoom from the history
 */
void CustomPlotZoom::previousZoom()
{
    MainWindow::getInstance()->setDebugMessage("Loading the previous zoom");
    if (historyIdx_ > 0)
    {
        m_zoomWatch.stop();
        xAxis->setRange(history_[--historyIdx_].xAxis);
        yAxis->setRange(history_[historyIdx_].yAxis);
        replot(QCustomPlot::rpQueuedReplot);
        emit zoomHistoryChanged();
    }
}

/*
 * \brief Set the next room in the history list
 */
void CustomPlotZoom::nextZoom()
{
    MainWindow::getInstance()->setDebugMessage("Loading the next zoom");
    if (static_cast<size_t>(historyIdx_ + 1) < history_.size())
    {
        m_zoomWatch.stop();
        xAxis->setRange(history_[++historyIdx_].xAxis);
        yAxis->setRange(history_[historyIdx_].yAxis);
        replot(QCustomPlot::rpQueuedReplot);
        emit zoomHistoryChanged();
    }
}

/*
 * \brief Get the actual zoom range
 * \return Return the actual zoom range
 */
Zoom CustomPlotZoom::getZoom() const
{
    return Zoom(xAxis->range(), yAxis->range());
}

/*
 * \brief Get the optimal zoom range
 * \return Return the zoom range by default
 */
const Zoom &CustomPlotZoom::getInitialZoom() const
{
    return origins_;
}

void CustomPlotZoom::setInitialZoom(const Zoom &zoom)
{
    origins_ = zoom;
    if (!hasPreviousZoom())
        resetZoom();
    else
        history_[0] = origins_;
}

/*
 * \brief Change the value of the label of the x axis
 * \param title: New title to set
 */
void CustomPlotZoom::setLabelAxisX(std::string title)
{
    xAxis->setLabel(tr(title.c_str()));
}

/*
 * \brief Change the value of the label of the y axis
 * \param title: New title to set
 */
void CustomPlotZoom::setLabelAxisY(std::string title)
{
    yAxis->setLabel(tr(title.c_str()));
}

/*
 * \brief Set manually the zoom
 * \param xp1: top left corner x position
 * \param yp1: top left corner y position
 * \param xp2: bottom right corner x position
 * \param yp2: bottom right corner y position
 */
void CustomPlotZoom::setZoom(double x1, double y1, double x2, double y2)
{
    setZoom(Zoom(x1, x2, y1, y2));
}

/*
 * \brief Set manually the zoom
 * \param range: Range to set
 */
void CustomPlotZoom::setZoom(const Zoom &range)
{
    xAxis->setRange(range.xAxis);
    yAxis->setRange(range.yAxis);
    addToHistory(range);
}

bool CustomPlotZoom::hasNextZoom() const
{
    return !history_.empty() &&
        (static_cast<size_t>(historyIdx_ + 1) < history_.size());
}

bool CustomPlotZoom::hasPreviousZoom() const
{
    return !history_.empty() && (historyIdx_ > 0);
}

/*
 * \brief Give a title to the graph
 * \param title: The title
 */
void CustomPlotZoom::setTitle(std::string title)
{
    title_->setText(tr(title.c_str()));
}

/*
 * \brief Show the title of the graph
 */
void CustomPlotZoom::showTitle()
{
    title_->setVisible(true);
}

/*
 * \brief Hide the title of the graph
 */
void CustomPlotZoom::hideTitle()
{
    title_->setVisible(false);
}

/*
 * \brief Give the value on the time range corresponding to the pixel given in
 * parameter \param pixel: pixel position \return Return the time in nano
 * seconds
 */
double CustomPlotZoom::pixelToNanosec(int32_t pixel)
{
    return xAxis->pixelToCoord(pixel);
}

void CustomPlotZoom::timerEvent(QTimerEvent *event)
{
    if (event && event->timerId() == m_zoomWatch.timerId())
    {
        m_zoomWatch.stop();
        Zoom current = getZoom();
        if (history_.empty() || !history_.back().similarTo(current))
            addToHistory(current);
    }
    else
        QCustomPlot::timerEvent(event);
}
} // namespace eventDisplay
} // namespace ntof
