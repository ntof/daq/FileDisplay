#ifndef CUSTOMPLOTZOOM_H
#define CUSTOMPLOTZOOM_H

#include <QBasicTimer>
#include <QObject>
#include <QPoint>
#include <QRect>

#include "qcustomplot.h"

class QRubberBand;
class QMouseEvent;
class QCPTextElement;

namespace ntof {
namespace eventDisplay {

struct Zoom
{
    Zoom();
    Zoom(double x1, double x2, double y1, double y2);
    Zoom(const QCPRange &xAxis, const QCPRange &yAxis);

    QCPRange xAxis;
    QCPRange yAxis;

    bool similarTo(const Zoom &other,
                   double threshold = SimilarThreshold) const; /* approximation
                                                                  operator */
    bool operator==(const Zoom &other) const;
    inline bool operator!=(const Zoom &other) const
    {
        return !this->operator==(other);
    };

protected:
    static const double SimilarThreshold; /*<! percent change to consider two
                                             zooms similar */
};

class PointInfo : public QCPItemText
{
public:
    /*
     * \brief Constructor of the class
     */
    explicit PointInfo(QCustomPlot *cp,
                       double x = 0,
                       double y = 0,
                       const std::string &xPrefix = "",
                       const std::string &yPrefix = "",
                       double xData = 0,
                       double yData = 0,
                       const std::string &xSuffix = "",
                       const std::string &ySuffix = "");

    /*
     * \brief Set the position of the PointInfo in the graph using
     * pixel coordinates
     */
    void setPosition(double x, double y);

    /*
     * \brief Get the X pixel coordinate of the PointInfo in the graph
     * \return Return X pixel coordinate of the PointInfo in the graph
     */
    inline double getXpixel() const { return m_X; }

    /*
     * \brief Get the Y pixel coordinate of the PointInfo in the graph
     * \return Return Y pixel coordinate of the PointInfo in the graph
     */
    inline double getYpixel() const { return m_Y; }

    /*
     * \brief Get the X Cartesian coordinate of the PointInfo in the graph
     * \return Return the X Cartesian coordinate of the PointInfo in the
     * graph
     */
    double getXcoord() const;

    /*
     * \brief Get the Y Cartesian coordinate of the PointInfo in the graph
     * \return Return the Y Cartesian coordinate of the PointInfo in the
     * graph
     */
    double getYcoord() const;

    /*
     * \brief Set the coordinates to be shown on the point
     */
    void setInfo(double x, double y);

    /*
     * \brief Set prefixes to the shown coordinates
     */
    void setPrefixes(const std::string &x, const std::string &y);

    /*
     * \brief Set suffixes to the shown coordinates
     */
    void setSuffixes(const std::string &x, const std::string &y);

    /*
     * \brief Determine if the distance between the two PointInfos is
     * large enough to avoid overlapping \param other: the other
     * PointInfo \return Return true if the two points are distant
     * enough from each other
     */
    bool isFarFrom(const PointInfo &other) const;

private:
    /*
     * \brief Update the information to show
     */
    void updateInfo();

    QCustomPlot *m_cp;
    double m_X;
    double m_Y;
    std::string m_xPrefix;
    std::string m_yPrefix;
    double m_Xdata;
    double m_Ydata;
    std::string m_xSuffix;
    std::string m_ySuffix;

    static const int s_textBoxMargin;
};

class MainWindow;
class CustomPlotZoom : public QCustomPlot
{
    Q_OBJECT
public:
    /*
     * \brief Constructor of the class
     */
    CustomPlotZoom();

    /*
     * \brief Destructor of the class
     */
    virtual ~CustomPlotZoom();

    /*
     * \brief Enable or disable the zoom mode
     * \param mode: true to enable the zoom mode or false to disable it
     */
    void setZoomMode(bool mode);

    /*
     * \brief Get the state of the zoom mode
     * \return Return true if the zoom mode is enable or false if it is not
     */
    inline bool isZoomMode() const { return mZoomMode; }

    /*
     * \brief Enable or disable the ToolTips option for the lasso zoomimg
     * tool \param mode: true to enable the option or false to disable it
     */
    void setMeasurementHints(bool mode);

    /*
     * \brief Get the state of the ToolTips option for the lasso zoomimg
     * tool \return Return true if the option is enable or false if it is not
     */
    inline bool isMeasurementHints() const { return m_measurementHints; }

    /*
     * \brief Listener of the key press event
     * \param event: the key event
     */
    void keyPressEvent(QKeyEvent *event);

    /*
     * \brief Listener of the mouse press event
     * \param event: mouse event
     */
    void mousePressEvent(QMouseEvent *event);

    /*
     * \brief Listener of the mouse move event
     * \param event: mouse event
     */
    void mouseMoveEvent(QMouseEvent *event);

    /*
     * \brief Listener of the mouse release event
     * \param event: mouse event
     */
    void mouseReleaseEvent(QMouseEvent *event);

    /*
     * \brief Listener of the mouse wheel event
     * \param event: mouse event
     */
    void wheelEvent(QWheelEvent *event);

    /*
     * \brief Reset the zoom mode
     */
    void resetZoom(bool clearHistory = true);

    /*
     * \brief Add a zoom to the history list
     * \param zoom: zoom to add to history
     */
    void addToHistory(const Zoom &zoom);

    /*
     * \brief Set the previous zoom from the history
     */
    void previousZoom();

    /*
     * \brief Set the next room in the history list
     */
    void nextZoom();

    /*
     * \brief Allow to know if there is a zoom history
     * \return Return true if a undo can be applied or false else
     */
    bool hasPreviousZoom() const;

    /*
     * \brief Allow to know if there is a zoom history
     * \return Return true if a redo can be applied or false else
     */
    bool hasNextZoom() const;

    /*
     * \brief Give the value on the time range corresponding to the pixel given
     * in parameter \param pixel: pixel position \return Return the time in nano
     * seconds
     */
    double pixelToNanosec(int32_t pixel);

    /*
     * \brief Give a title to the graph
     * \param title: The title
     */
    void setTitle(std::string title);

    /*
     * \brief Show the title of the graph
     */
    void showTitle();

    /*
     * \brief Hide the title of the graph
     */
    void hideTitle();

    /*
     * \brief Set manually the zoom
     * \param xp1: top left corner x position
     * \param yp1: top left corner y position
     * \param xp2: bottom right corner x position
     * \param yp2: bottom right corner y position
     */
    void setZoom(double xp1, double yp1, double xp2, double yp2);

    /*
     * \brief Set manually the zoom
     * \param range: Range to set
     */
    void setZoom(const Zoom &range);

    /**
     * @brief set the initial zoom
     *
     * @details reset zoom and clear history if we were on the initial zoom.
     */
    void setInitialZoom(const Zoom &zoom);

    /*
     * \brief Get the actual zoom range
     * \return Return the actual zoom range
     */
    Zoom getZoom() const;

    /*
     * \brief Get the optimal zoom range
     * \return Return the zoom range by default
     */
    const Zoom &getInitialZoom() const;

    /*
     * \brief Change the value of the label of the x axis
     * \param title: New title to set
     */
    void setLabelAxisX(std::string title);

    /*
     * \brief Change the value of the label of the y axis
     * \param title: New title to set
     */
    void setLabelAxisY(std::string title);

signals:
    void zoomHistoryChanged();
    void zoomModeChanged(bool state);
    void openglDetect();

public slots:
    void openglRenderingDetection();

protected slots:
    virtual void processRectZoom(QRect rect, QMouseEvent *event);

    void onConfigUpdate();

protected:
    /*
     * \brief According the mode enabled, the cursor appearance change
     */
    void changeCursor();

    virtual void timerEvent(QTimerEvent *event);

    static const int ZoomWatchDelay; //!< number of miliseconds to wait on
                                     //!< mousewheel
    static const double KeyboardMoveFactor;

    QBasicTimer m_zoomWatch; //!< Zoom watch timer
    bool m_isDragging;       //!< Define if a dragging is on going
    bool mZoomMode;          //!< State of the zoom mode
    bool m_savedMode;        //!< Saved mode for keyboard modifiers
    bool m_measurementHints; //!< On/Off of tooltips on the zoom lasso
    QPoint mOrigin; //!< Origin of the Rectangle used to define the zoom area
    Zoom origins_;  //!< Default range settings
    std::vector<Zoom> history_; //!< History of the zoom applied
    ssize_t historyIdx_;        //!< Iterator in the zoom history
    QCPTextElement *title_;     //!< Title of the graph
    PointInfo *startZoomPoint_; //!< Info on the initial point of zoom selection
                                //!< area
    PointInfo *endZoomPoint_;   //!< Info on the final point of zoom selection
                                //!< area
    PointInfo *midZoomPoint_;   //!< Info on the midpoint of zoom selection area
};
} // namespace eventDisplay
} // namespace ntof
#endif // CUSTOMPLOTZOOM_H
