/*
 * FileDisplayException.cpp
 *
 *  Created on: Oct 13, 2014
 *      Author: mdonze
 */

#include "FileDisplayException.h"

#include <sstream>

namespace ntof {
namespace eventDisplay {

/**
 * \brief Constructs this
 * \param Msg Error message
 * \param file Name of the file where the exception is thrown (__FILE__)
 * \param Line Line of code doing the error (__LINE__)
 */
FileDisplayException::FileDisplayException(const std::string &Msg,
                                           const std::string &file,
                                           int Line) :
    what_(""), msg(Msg), file_(file), line(Line), errorCode(0)
{}

/**
 * \brief Construct this
 * \param Msg Error message
 * \param file Name of the file where the exception is thrown (__FILE__)
 * \param Line Line of code doing the error (__LINE__)
 */
FileDisplayException::FileDisplayException(const char *Msg,
                                           const std::string &file,
                                           int Line) :
    what_(""), msg(Msg), file_(file), line(Line), errorCode(0)
{}

/**
 * \brief Construct this
 * \param Msg Error message
 * \param file Name of the file where the exception is thrown (__FILE__)
 * \param Line Line of code doing the error (__LINE__)
 * \param error Error code
 */
FileDisplayException::FileDisplayException(const char *Msg,
                                           const std::string &file,
                                           int Line,
                                           int error) :
    what_(""), msg(Msg), file_(file), line(Line), errorCode(error)
{}

/**
 * \brief Empty constructor of the class
 */
FileDisplayException::FileDisplayException() :
    what_(""), msg(""), file_(""), line(0), errorCode(0)
{}

/**
 * \brief Destructor of the class
 */
FileDisplayException::~FileDisplayException() throw() {}

/**
 * \brief Overload of std::exception
 * \return Return the error message
 */
const char *FileDisplayException::what() const throw()
{
    if (what_.empty())
    {
        std::ostringstream oss;
        oss << "Ntof Lib exception in source file " << file_ << " at line "
            << line << " : " << msg;
        what_ = oss.str();
    }
    return what_.c_str();
}

/**
 * \brief Gets the error message
 * \return Return the error message
 */
const char *FileDisplayException::getMessage() const throw()
{
    return this->msg.c_str();
}

/**
 * \brief Gets the error code
 * \return
 */
int FileDisplayException::getErrorCode() const throw()
{
    return errorCode;
}

/**
 * \brief Gets the source file
 * \return Source file name
 */
const std::string FileDisplayException::getFile() const throw()
{
    return file_;
}

/**
 * \brief Gets the line in source file
 * \return Return the line in source file
 */
int FileDisplayException::getLine() const throw()
{
    return line;
}
} // namespace eventDisplay
} // namespace ntof
