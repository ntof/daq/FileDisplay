
#include "FileReader.h"

#include <iostream>
#include <set>

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>

#include <QFileInfo>
#include <QMessageBox>
#include <QtConcurrent>

#include <FileCache.h>
#include <NtofLib.h>
#include <sys/time.h>

#include "Channel.h"
#include "Config.h"
#include "CustomPlotZoom.h"
#include "MainWindow.h"
#include "Progress.h"
#include "TableParams.h"

namespace ntof {

namespace eventDisplay {
FileReader *FileReader::instance_ = NULL;

static const boost::regex s_daqname_re = boost::regex(
    "(ntofdaq-m[0-9]*)",
    boost::regex_constants::icase);

/*!
 *  \brief Get the instance of the FileReader object and initialize the instance
 * if the pointer is null \return A pointer on the FileReader object
 */
FileReader *FileReader::getInstance()
{
    if (instance_ == NULL)
    {
        instance_ = new FileReader();
    }
    return instance_;
}

/*
 * \brief Constructor of the class
 */
FileReader::FileReader() : plotLine_(true), plotPoints_(false)
{
    ntofLib_.setServiceClass("default");
    onConfigUpdate();
    connect(Config::getInstance(), SIGNAL(updated()), this,
            SLOT(onConfigUpdate()));
}

/*
 * \brief Destructor of the class
 */
FileReader::~FileReader()
{
    // The life time of this object if the lifetime of the application but ...
    // Remove all the channel from the graphic and delete them
    clearFileReader();
    delete instance_;
}

void FileReader::onConfigUpdate()
{
    ntofLib_.getFileCache()->setBaseDir(Config::getInstance()->getCacheDir());
    ntofLib_.getFileCache()->setMaxSize(Config::getInstance()->getCacheSize() *
                                        1024 * 1024 * 1024);
}

/* ***************************************************** */
/* OBJECTS INTERACTIONS                                  */
/* ***************************************************** */
/*
 * \brief Read all the ACQC relative to this event and initialize the channels
 * configurations
 */
void FileReader::readData()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Read the raw file(s)");

    // TODO to be removed
    struct timeval start;
    gettimeofday(&start, NULL);           // left hand struct is usually enough
    double secondsStart = start.tv_sec;   // time in seconds
    double usecondsStart = start.tv_usec; // time in microseconds

    if (ntofLib_.isDefinedEVEH())
    {
        while (ntofLib_.getNextACQC())
        {
            if (!ntofLib_.getACQC()->setChannelConfiguration())
            {
                std::cerr << "Error to set the channel configuration."
                          << std::endl;
            }
            else
            {
                Channel *channel = getChannel(ntofLib_.getMODH()->getIndex());
                if (channel && channel->isActive())
                {
                    setChannel(channel);
                }
            }
        }
    }

    gettimeofday(&start, NULL);          // left hand struct is usually enough
    double secondsStop = start.tv_sec;   // time in seconds
    double usecondsStop = start.tv_usec; // time in microseconds

    double diff = ((secondsStop - secondsStart) * 1E6) +
        (usecondsStop - usecondsStart);
    QLatin1Char mNumberFormatChar('g');
    QString value = QString("%L1").arg(diff, 0, mNumberFormatChar.toLatin1(),
                                       10);
    std::cerr
        << "[DEBUG] Read data from the file(s) in: " << value.toStdString()
        << " us" << std::endl;

    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Create the channels objects
 */
void FileReader::initReader()
{
    // Update the list of channels
    initChannelList();

    MainWindow::getInstance()->enableButton();
    if (Config::getInstance()->getModeDebug() == "on" &&
        ntofLib_.getFilenames().size() != 0)
    {
        MainWindow::getInstance()->getMenuBar()->getDebugFileAction()->setEnabled(
            true);
    }
}

/*
 * \brief Update the graphical elements related to the FileReader
 */
void FileReader::updateElement()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage(
        "Update the graphical elements related to the FileReader");
    for (std::list<Channel *>::iterator it = channels_.begin();
         it != channels_.end(); ++it)
    {
        if ((*it)->isParamsShown())
        {
            MainWindow::getInstance()->getTableParams()->addRow((*it));
        }
        if ((*it)->updateColor())
        {
            MainWindow::getInstance()->setToReplot(true);
        }
    }
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Update the appearance of the channels
 */
void FileReader::updateChannelsAppearance()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage(
        "Update the appearance of the channels");
    for (std::list<Channel *>::iterator it = channels_.begin();
         it != channels_.end(); ++it)
    {
        (*it)->updateElement();
    }
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Reset the zoom to the initial one
 */
void FileReader::resetZoom()
{
    MainWindow::getInstance()->getCustomPlot()->resetZoom();
}

/*
 * \brief Destroy all the channels objects
 */
void FileReader::clearChannels()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage(
        "Destroy all the channels objects");
    for (std::list<Channel *>::iterator it = channels_.begin();
         it != channels_.end(); ++it)
    {
        delete (*it);
    }
    channels_.clear();
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Destroy all the attribute of the FileReader and reset it
 */
void FileReader::clearFileReader()
{
    CustomPlotZoom *customPlot = MainWindow::getInstance()->getCustomPlot();
    clearChannels();
    customPlot->clearGraphs();
    customPlot->legend->clear();
    customPlot->resetZoom();
    customPlot->setZoom(0, 0, 5, 5);
}

/* **************************************************** */
/* EVENTS INTERACTIONS                                  */
/* **************************************************** */
/*
 * \brief Load the previous event
 * \return Return if a previous event has been found
 */
bool FileReader::previousEvent()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Load the previous event");
    bool res = ntofLib_.getPreviousEvent();
    if (res)
    {
        initChannelList();
        plotData();
    }
    MainWindow::getInstance()->removeActionOnGoing();
    return res;
}

/*
 * \brief Load the next event
 * \return Return if a next event has been found
 */
bool FileReader::nextEvent(bool replot)
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Load the next event");
    bool res = ntofLib_.getNextEvent();
    if (res)
    {
        initChannelList();
        plotData(replot);
    }
    MainWindow::getInstance()->removeActionOnGoing();
    return res;
}

/*
 * \brief Load the last event
 */
void FileReader::lastEvent()
{
    if (ntofLib_.getEvent(ntofLib_.getNumberOfEvents() - 1))
    {
        initChannelList();
        plotData();
    }
}

/* **************************************************** */
/* CUSTOM PLOT INTERACTIONS                             */
/* **************************************************** */
/*
 * \brief Get the plot line state
 * \return Return the plot line state
 */
bool FileReader::isPlotLine()
{
    return plotLine_;
}

/*
 * \brief Set the plot line state
 * \param plotLine: new state of the plot line
 */
void FileReader::setPlotLine(bool plotLine)
{
    plotLine_ = plotLine;
}

/*
 * \brief Get the plot points state
 * \return Return the plot points state
 */
bool FileReader::isPlotPoints()
{
    return plotPoints_;
}

/*
 * \brief Set the plot points state
 * \param plotPoints: new state of the plot points
 */
void FileReader::setPlotPoints(bool plotPoints)
{
    plotPoints_ = plotPoints;
}

/*
 * \brief Reset the plot display
 */
void FileReader::plotData(bool replot)
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Plot the data");

    double maxFullScale = 0;
    double minLowerLimit = 0;
    double minHeight = 0;
    double maxHeight = 0;
    // cppcheck-suppress variableScope ; [minTimeWindow]
    double minTimeWindow = 0;
    double maxTimeWindow = 0;
    bool channelFound = false;

    // Update the content of the main info group box
    MainWindow::getInstance()->setTriggerDisplay();
    MainWindow::getInstance()->setBCT();

    // Update the value of the segment number loaded
    MainWindow::getInstance()->setSegmentNumber();

    CustomPlotZoom *customPlot = MainWindow::getInstance()->getCustomPlot();
    readData();
    for (std::list<Channel *>::iterator it = channels_.begin();
         it != channels_.end(); ++it)
    {
        if ((*it)->isActive())
        {
            (*it)->setupGraph();

            if (!channelFound)
            {
                channelFound = true;
                maxFullScale = (*it)->getFullScaleInMilliVolts();
                minLowerLimit = (*it)->getLowerLimit();
                maxTimeWindow = (*it)->getTimeWindow() * 1E6;

                maxHeight = minLowerLimit + maxFullScale;
                minHeight = minLowerLimit;
                if (Config::getInstance()->getDataUnit() == "adc")
                {
                    maxHeight = (*it)->mvToADC(maxHeight);
                    minHeight = (*it)->mvToADC(minHeight);
                }
            }
            else
            {
                double fullScale = (*it)->getFullScaleInMilliVolts();
                double lowerLimit = (*it)->getLowerLimit();
                double timeWindow = (*it)->getTimeWindow() * 1E6;

                if (maxFullScale < fullScale)
                {
                    maxFullScale = fullScale;
                }
                if (minLowerLimit > lowerLimit)
                {
                    minLowerLimit = lowerLimit;
                }
                if (maxTimeWindow < timeWindow)
                {
                    maxTimeWindow = timeWindow;
                }

                maxHeight = minLowerLimit + maxFullScale;
                minHeight = minLowerLimit;
                if (Config::getInstance()->getDataUnit() == "adc")
                {
                    maxHeight = (*it)->mvToADC(maxHeight);
                    minHeight = (*it)->mvToADC(minHeight);
                }
            }
        }
        else
        {
            (*it)->removeGraph();
        }
    }

    // +10% on the axis
    if (channelFound)
    {
        Zoom range(minTimeWindow, maxTimeWindow, minHeight, maxHeight);

        /* enlarge 10% */
        double size = range.xAxis.size() / 10;
        range.xAxis.lower -= size;
        range.xAxis.upper += size;
        size = range.yAxis.size() / 10;
        range.yAxis.lower -= size;
        range.yAxis.upper += size;
        customPlot->setInitialZoom(range);
    }

    if (replot)
    {
        customPlot->replot();
    }

    MainWindow::getInstance()->removeActionOnGoing();
}

/* **************************************************** */
/* CHANNEL INTERACTIONS                                 */
/* **************************************************** */
/*
 * \brief Get the list of the channels
 * \return Return the list of the channels
 */

const std::list<Channel *> &FileReader::getChannels() const
{
    return channels_;
}

/*
 * \brief Get the channel related to the id given in parameter
 * \param id: The id of the channel to return
 * \return Return the channel if it has been found or NULL in the other case
 */
Channel *FileReader::getChannel(int32_t id)
{
    for (std::list<Channel *>::iterator it = channels_.begin();
         it != channels_.end(); ++it)
    {
        if ((*it)->getId() == id)
        {
            return (*it);
        }
    }
    return NULL;
}

/*
 * \brief Disabled all the buttons allowing to adapt the view
 */
void FileReader::disabledButtonsAdaptedView()
{
    for (std::list<Channel *>::iterator it = channels_.begin();
         it != channels_.end(); ++it)
    {
        (*it)->getAdaptViewButton()->setChecked(false);
    }
}

/*
 * \brief Allow to know if at least one channel is enabled
 * \return Return true if there is at least one channel enabled
 */
bool FileReader::hasChannelEnabled()
{
    for (std::list<Channel *>::iterator it = channels_.begin();
         it != channels_.end(); ++it)
    {
        if ((*it)->isActive())
        {
            return true;
        }
    }
    return false;
}

/*
 * \brief Activate all the channels
 */
void FileReader::setActive()
{
    for (std::list<Channel *>::iterator it = channels_.begin();
         it != channels_.end(); ++it)
    {
        (*it)->setActive();
    }
    MainWindow::getInstance()->enablePlot();
}

/*
 * \brief Deactivate all the channels
 */
void FileReader::setInactive()
{
    for (std::list<Channel *>::iterator it = channels_.begin();
         it != channels_.end(); ++it)
    {
        (*it)->setInactive();
    }
    MainWindow::getInstance()->enablePlot();
}

/*
 * \brief Activate only one channel
 * \param id: ID of the channel to activate
 */
void FileReader::setActive(int32_t id)
{
    for (std::list<Channel *>::iterator it = channels_.begin();
         it != channels_.end(); ++it)
    {
        if ((*it)->getId() == id)
        {
            (*it)->setActive();
            break;
        }
    }
}

/*
 * \brief Deactivate only one channel
 * \param id: ID of the channel to deactivate
 */
void FileReader::setInactive(int32_t id)
{
    for (std::list<Channel *>::iterator it = channels_.begin();
         it != channels_.end(); ++it)
    {
        if ((*it)->getId() == id)
        {
            (*it)->setInactive();
            break;
        }
    }
}

/* **************************************************** */
/* NTOFLIB INTERACTIONS                                 */
/* **************************************************** */
/*
 * \brief Read the file and set the good event after having closed the files
 * already opened \param filename: Full path of the file \return Return true
 * if everything goes right
 */
bool FileReader::openFile(const std::string &filename)
{
    std::vector<std::string> filenames;
    filenames.push_back(filename);
    return openFile(filenames);
}

#define MAX_DAQS 99

/*
 * \brief For each raw file, try to open the related .run and the related .event
 * \param filenames: List of all the full path to be opened
 */
// TODO Use a deque instead of the vector to be able to push_front
void FileReader::addMissingFiles(std::vector<std::string> &filenames)
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Search the missing files");
    std::vector<std::string> missingFiles;

    // Generate the list of the files found
    for (std::vector<std::string>::iterator fileIt = filenames.begin();
         fileIt != filenames.end(); ++fileIt)
    {
        ntof::lib::FILE_INFO fileInfo = ntofLib_.getFileInfo(*fileIt);
        // Looking for a raw local file
        if (fileInfo.extension == "raw")
        {
            boost::filesystem::path fullpath(fileInfo.fullpath);
            const boost::filesystem::path baseDir(fullpath.parent_path());

            const boost::filesystem::path baseEacsDir(
                boost::regex_replace(baseDir.string(), s_daqname_re,
                                     "ntofapp-" +
                                         boost::lexical_cast<std::string>(
                                             fileInfo.runNumber / 100000)));

            boost::filesystem::path runFile(
                "run" + boost::lexical_cast<std::string>(fileInfo.runNumber) +
                ".run");
            boost::filesystem::path eventFile(
                "run" + boost::lexical_cast<std::string>(fileInfo.runNumber) +
                "_" + boost::lexical_cast<std::string>(fileInfo.segmentNumber) +
                ".event");

            if (boost::filesystem::exists(baseEacsDir / runFile))
                runFile = baseEacsDir / runFile;
            else if (boost::filesystem::exists(baseDir / runFile))
                runFile = baseDir / runFile;
            else
                runFile.clear();

            if (boost::filesystem::exists(baseEacsDir / eventFile))
                eventFile = baseEacsDir / eventFile;
            else if (boost::filesystem::exists(baseDir / eventFile))
                eventFile = baseDir / eventFile;
            else
                eventFile.clear();

            std::cout << "Run file:   " << runFile.string() << std::endl;
            if (!runFile.empty())
            {
                missingFiles.push_back(runFile.string());

                if (Config::getInstance()->isAggregateDaqFiles())
                {
                    addDAQFiles(fullpath.string(), runFile.string(),
                                missingFiles);
                }
            }

            std::cout << "Event file: " << eventFile.string() << std::endl;
            if (!eventFile.empty())
                missingFiles.push_back(eventFile.string());
        }
    }

    for (const std::string &missingFile : missingFiles)
    {
        bool found = false;
        for (const std::string &filename : filenames)
        {
            if (missingFile == filename)
            {
                found = true;
                break;
            }
        }

        if (!found)
        {
            filenames.insert(filenames.begin(), missingFile);
        }
    }
    MainWindow::getInstance()->removeActionOnGoing();
}

void FileReader::addDAQFiles(const std::string &fullpath,
                             const std::string &runFile,
                             std::vector<std::string> &missingFiles)
{
    std::set<uint32_t> chassisList;
    try
    {
        ntof::lib::NtofLib lib(runFile);
        if (lib.isDefinedMODH())
        {
            for (const ntof::lib::CHAN_CONFIG &cfg :
                 lib.getMODH()->getChannelsConfig())
                chassisList.insert(cfg.chassis);
        }
    }
    catch (ntof::lib::NtofLibException &ex)
    {
        std::cout << "Failed to load MODH" << std::endl;
    }
    for (uint32_t chassis : chassisList)
    {
        const boost::filesystem::path daqFile(boost::regex_replace(
            fullpath, s_daqname_re, "ntofdaq-m" + std::to_string(chassis)));
        if ((daqFile != fullpath) && boost::filesystem::exists(daqFile))
        {
            std::cout << "Adding RAW File: " << daqFile.string() << std::endl;
            missingFiles.push_back(daqFile.string());
        }
    }
}

bool FileReader::readFiles(const std::vector<std::string> &filenames,
                           bool closeFiles)
{
    Progress notif(ntofLib_, QObject::tr("Loading files"),
                   QObject::tr("Preparing ..."));

    QFuture<bool> ret = QtConcurrent::run([&]() -> bool {
        try
        {
            ntofLib_.readFiles(filenames, closeFiles);
            return true;
        }
        catch (ntof::lib::NtofLibException &ex)
        {
            if (std::string(ex.getMessage()).empty())
                return false;
            else
                throw ntof::eventDisplay::Progress::Exception(ex);
        }
    });

    try
    {
        notif.exec(ret);
        return !notif.isCanceled();
    }
    catch (Progress::Exception &ex)
    {
        QMessageBox::critical(
            eventDisplay::MainWindow::getInstance(), QObject::tr("Error"),
            QObject::tr("Error during the reading of the file:\n") +
                ex.message(),
            QMessageBox::Ok);
        return false;
    }
    return ret.result();
}

/*
 * \brief Read the file and set the good event after having closed the files
 * already opened \param filenames: Full paths of the files \param forced:
 * Open the new files without asking to close the old ones \param quiet:
 * Avoid to display an error message if it equals false \param castor:
 * Check if files are on Castor when it equals true \return Return true if
 * everything goes right
 */
bool FileReader::openFile(const std::vector<std::string> &arg,
                          bool forced,
                          bool quiet,
                          bool castor)
{
    std::vector<std::string> filenames(arg);

    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Open file(s)");
    bool res = true;

    addMissingFiles(filenames);

    // TODO to be removed
    struct timeval start;
    gettimeofday(&start, NULL);           // left hand struct is usually enough
    double secondsStart = start.tv_sec;   // time in seconds
    double usecondsStart = start.tv_usec; // time in microseconds

    // Open the files
    if (!filenames.empty())
    {
        if (castor)
        {
            for (std::vector<std::string>::iterator fileIt = filenames.begin();
                 fileIt != filenames.end(); ++fileIt)
            {
                if (ntofLib_.isOnCastor((*fileIt)) &&
                    XRootBrowser::getInfo(*fileIt)->isOffline())
                {
                    res = false;
                    break;
                }
            }
        }

        // If all the files are staged they can be opened
        if (castor && !res)
        {
            if (!quiet)
            {
                QString title("File not staged");
                QString message("At least one of the files you are trying to "
                                "open isn't STAGED.\n You can't open it.");
                QMessageBox::critical(MainWindow::getInstance(), title, message,
                                      QMessageBox::Ok);
            }
        }
        else
        {
            bool resetRange = false;
            // First file opened by the ntofLib
            if (ntofLib_.getFilenames().size() == 0)
            {
                res = readFiles(filenames, true);
                resetRange = true;

                if (res && ntofLib_.isDefinedRCTR() && ntofLib_.isDefinedMODH())
                {
                    // Update the content of the main panel
                    MainWindow::getInstance()->initMainInfo();
                    //                            MainWindow::getInstance()->setTriggerLoaded();
                }
            }
            else
            {
                // Check if the file can be added to the files already opened
                ntof::lib::CHECK_LIST check = ntofLib_.checkFiles(filenames);
                if (check.isValid)
                {
                    res = readFiles(filenames, false);
                }
                else
                {
                    int32_t result;
                    if (!forced)
                    {
                        // Ask the user what to do
                        QMessageBox msgBox;
                        msgBox.setText(QObject::tr(check.message.c_str()));
                        msgBox.setInformativeText(
                            "Do you want to close the other(s) files to open "
                            "this one ?");
                        msgBox.setStandardButtons(QMessageBox::Yes |
                                                  QMessageBox::No);
                        msgBox.setDefaultButton(QMessageBox::Yes);
                        result = msgBox.exec();
                    }
                    else
                    {
                        result = QMessageBox::Yes;
                    }

                    if (result == QMessageBox::Yes)
                    {
                        clearFileReader();
                        MainWindow::getInstance()->getTableParams()->clear();
                        MainWindow::getInstance()->getCustomPlot()->replot();

                        res = readFiles(filenames, true);

                        resetRange = true;
                        if (res && ntofLib_.isDefinedRCTR() &&
                            ntofLib_.isDefinedMODH())
                        {
                            // Update the content of the main panel
                            MainWindow::getInstance()->initMainInfo();
                        }
                    }
                    else
                    {
                        // No changes applied
                        MainWindow::getInstance()->removeActionOnGoing();
                        return res;
                    }
                }
            }

            if (ntofLib_.isDefinedRCTR() && ntofLib_.isDefinedMODH())
            {
                // Update the list of detectors, the content of the main group
                // box
                initReader();
                if (resetRange)
                {
                    MainWindow::getInstance()->rangeSavedReset();
                }

                // save the actual configuration
                MainWindow::getInstance()->saveConfiguration();

                // Jump to the first event
                int32_t firstEvent = ntofLib_.getFirstEventOf(filenames[0]);
                // ntofLib_.getValidatedEvent(firstEvent);
                MainWindow::getInstance()->loadEvent(firstEvent);

                // apply the old configuration
                MainWindow::getInstance()->applyConfiguration();
            }
        }
    }
    else
    {
        res = false;
    }

    gettimeofday(&start, NULL);          // left hand struct is usually enough
    double secondsStop = start.tv_sec;   // time in seconds
    double usecondsStop = start.tv_usec; // time in microseconds

    double diff = ((secondsStop - secondsStart) * 1E6) +
        (usecondsStop - usecondsStart);
    QLatin1Char mNumberFormatChar('g');
    QString value = QString("%L1").arg(diff, 0, mNumberFormatChar.toLatin1(),
                                       10);
    std::cerr << "[DEBUG] Open file(s) in: " << value.toStdString() << " us"
              << std::endl;

    MainWindow::getInstance()->removeActionOnGoing();
    return res;
}

/*
 * \brief Get the NtofLib object
 * \return Return a reference of the ntofLib object
 */
ntof::lib::NtofLib &FileReader::getNtofLib()
{
    return ntofLib_;
}

/*
 * \brief Create all the channels and initialize them
 */
void FileReader::initChannelList()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage(
        "Create all the channels and initialize them");

    // Save configuration
    MainWindow::getInstance()->saveConfiguration();

    acqcList_.clear();
    clearChannels();
    // Only the channels containing data are displayed at the beginning
    ntofLib_.getMODH()->setIndex(0);
    do
    {
        if ((ntofLib_.getMODH()->getStream() ==
             ntofLib_.getRCTR()->getStreamNumber()))
        {
            uint32_t id = ntofLib_.getMODH()->getIndex();

            bool acqcFound = checkACQC(ntofLib_.getMODH()->getDetectorType(),
                                       ntofLib_.getMODH()->getDetectorId());
            if (!channelExists(id) && acqcFound)
            {
                Channel *channel = new Channel();
                channel->initChannel();
                channel->initThreshold();
                channels_.push_back(channel);
            }
        }
    } while (ntofLib_.getMODH()->getNextChannel());

    // Apply old configuration
    MainWindow::getInstance()->applyConfiguration();

    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Check if the ACQC can be found for the actual event
 * \param detectorType: Type of the detector
 * \param detectorId: ID of the detector
 */
bool FileReader::checkACQC(const std::string &detectorType, uint32_t detectorId)
{
    bool res = false;
    // If no event defined, load the first one
    if (!ntofLib_.isDefinedEVEH())
    {
        if (!ntofLib_.getNextEvent())
        {
            return false;
        }
    }

    // Get the list of acqc related to this event
    if (acqcList_.empty())
    {
        int32_t eventNumber = ntofLib_.getEVEH()->getEventNumber();
        acqcList_ = ntofLib_.getACQCList(eventNumber);
    }

    // Check if the ACQC can be found
    for (std::vector<ntof::lib::ACQC_INFO>::iterator acqcIt = acqcList_.begin();
         acqcIt != acqcList_.end(); ++acqcIt)
    {
        if ((*acqcIt).detectorType == detectorType &&
            (*acqcIt).detectorId == detectorId)
        {
            res = true;
            break;
        }
    }

    return res;
}

/*
 * \brief Load the channels data into the memory
 * \param id: ID of the channel
 * \return Return true if the channel exists
 */
bool FileReader::channelExists(int32_t id)
{
    for (std::list<Channel *>::iterator it = channels_.begin();
         it != channels_.end(); ++it)
    {
        if ((*it)->getId() == id)
        {
            return true;
        }
    }
    return false;
}

/*
 * \brief Load the channels data into the memory
 * \param channel: where the data will be stored
 */
void FileReader::setChannel(Channel *channel)
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Load the data into the memory");

    int32_t eventNumber = ntofLib_.getEVEH()->getEventNumber();
    if (channel->getEventNumber() != eventNumber)
    {
        channel->clear();
        if (channel->reserve(ntofLib_.getACQC()->getPulseSize()))
        {
            double baseTimestamp = 1.0 / (channel->getSampleRate() * 1000000.0);
            int16_t dataADC;
            double dataMv;
            double relativeTimestamp;

            while (ntofLib_.getACQC()->getNextPulse())
            {
                int64_t timestamp = ntofLib_.getACQC()->getPulseTimestamp();
                int64_t length = ntofLib_.getACQC()->getPulseLength();

                int64_t start = 0;
                if (timestamp > channel->getPreSamples())
                {
                    start = timestamp - channel->getPreSamples();
                }

                for (int64_t i = 0; i < length; ++i)
                {
                    dataADC = (*ntofLib_.getACQC())[i];
                    relativeTimestamp = ((double) (i + start) * baseTimestamp) *
                        1000.0 * 1000.0 * 1000.0; // In ns

                    if (Config::getInstance()->getDataUnit() == "adc")
                    {
                        channel->addPoint(relativeTimestamp, dataADC);
                    }
                    else
                    {
                        dataMv = ((double) dataADC *
                                  channel->getFullScaleInMilliVolts() /
                                  (double) channel->getADCRange()) -
                            channel->getOffset();
                        channel->addPoint(relativeTimestamp, dataMv);
                    }
                }
            }
        }
        channel->setEventNumber(eventNumber);
    }
    MainWindow::getInstance()->removeActionOnGoing();
}
} // namespace eventDisplay
} // namespace ntof
