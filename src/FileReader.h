#ifndef FILEREADER_H
#define FILEREADER_H

#include <list>
#include <map>
#include <string>
#include <vector>

#include <QObject>

#include <HeaderManager.h>
#include <NtofLib.h>

class QCustomPlot;

namespace ntof {
namespace lib {
class NtofLib;
}

namespace eventDisplay {
class Channel;
class MainWindow;

typedef struct
{
    int32_t runNumber;
    std::vector<int32_t> eventsNumber;
    std::vector<int32_t> rawNumber;
} NOT_FINISHED_ELEMENTS;

typedef struct
{
    bool eventsNumber;
    bool rawNumber;
} EVENT_FILES_INFO;

typedef struct
{
    bool runFile;
    std::map<int32_t, EVENT_FILES_INFO> events;
} LOCAL_FILES_INFO;

class FileReader : public QObject
{
    Q_OBJECT
public:
    /*!
     *  \brief Get the instance of the FileReader object and initialize the
     * instance if the pointer is null \return A pointer on the FileReader
     * object
     */
    static FileReader *getInstance();

    /*
     * \brief Destructor of the class
     */
    ~FileReader();

    /* **************************************************** */
    /* OBJECTS INTEACTIONS                                  */
    /* **************************************************** */
    /*
     * \brief Read all the ACQC relative to this event and initialize the
     * channels configurations
     */
    void readData();

    /*
     * \brief Create the channels objects
     */
    void initReader();

    /*
     * \brief Update the graphical elements related to the FileReader
     */
    void updateElement();

    /*
     * \brief Update the appearance of the channels
     */
    void updateChannelsAppearance();

    /*
     * \brief Reset the zoom to the initial one
     */
    void resetZoom();

    /*
     * \brief Destroy all the attribute of the FileReader and reset it
     */
    void clearFileReader();

    /* **************************************************** */
    /* EVENTS INTERACTIONS                                  */
    /* **************************************************** */
    /*
     * \brief Get the id of the actual event
     * \return Return the id of the actual event (default -1)
     */
    //                int32_t getEventId();

    /*
     * \brief Get the number of events loaded in the ntofLib
     * \return Return the number of events loaded in the ntofLib
     */
    //                int32_t getNumberOfEvents();

    /*
     * \brief Load the previous event
     * \return Return if a previous event has been found
     */
    bool previousEvent();

    /*
     * \brief Load the next event
     * \return Return if a next event has been found
     */
    bool nextEvent(bool replot = true);

    /*
     * \brief Load the last event
     */
    void lastEvent();

    /* **************************************************** */
    /* CUSTOM PLOT INTERACTIONS                             */
    /* **************************************************** */
    /*
     * \brief Get the plot line state
     * \return Return the plot line state
     */
    bool isPlotLine();

    /*
     * \brief Set the plot line state
     * \param plotLine: new state of the plot line
     */
    void setPlotLine(bool plotLine);

    /*
     * \brief Get the plot points state
     * \return Return the plot points state
     */
    bool isPlotPoints();

    /*
     * \brief Set the plot points state
     * \param plotPoints: new state of the plot points
     */
    void setPlotPoints(bool plotPoints);

    /*
     * \brief Reset the plot display
     */
    void plotData(bool replot = true);

    /* **************************************************** */
    /* CHANNEL INTERACTIONS                                 */
    /* **************************************************** */
    /*
     * \brief Get the list of the channels
     * \return Return the list of the channels
     */
    const std::list<Channel *> &getChannels() const;

    /*
     * \brief Get the channel related to the id given in parameter
     * \param id: The id of the channel to return
     * \return Return the channel if it has been found or NULL in the other case
     */
    Channel *getChannel(int32_t id);

    /*
     * \brief Disabled all the buttons allowing to adapt the view
     */
    void disabledButtonsAdaptedView();

    /*
     * \brief Allow to know if at least one channel is enabled
     * \return Return true if there is at least one channel enabled
     */
    bool hasChannelEnabled();

    /*
     * \brief Activate all the channels
     */
    void setActive();

    /*
     * \brief Deactivate all the channels
     */
    void setInactive();

    /*
     * \brief Activate only one channel
     * \param id: ID of the channel to activate
     */
    void setActive(int32_t id);

    /*
     * \brief Deactivate only one channel
     * \param id: ID of the channel to deactivate
     */
    void setInactive(int32_t id);

    /* **************************************************** */
    /* NTOFLIB INTERACTIONS                                 */
    /* **************************************************** */
    /*
     * \brief Read the file and set the good event after having closed the files
     * already opened \param filename: Full path of the file
     */
    bool openFile(const std::string &filename);

    /*
     * \brief For each raw file, try to open the related .run and the related
     * .event \param filesname: List of all the full path to be opened
     */
    void addMissingFiles(std::vector<std::string> &filenames);

    /*
     * \brief Read the file and set the good event after having closed the files
     * already opened \param filenames: Full paths of the files \param forced:
     * Open the new files without asking to close the old ones \param quiet:
     * Avoid to display an error message if it equals false \param castor:
     * Check if files are on Castor when it equals true \return Return true if
     * everything goes right
     */
    bool openFile(const std::vector<std::string> &filenames,
                  bool forced = false,
                  bool quiet = false,
                  bool castor = true);

    /*
     * \brief Get the NtofLib object
     * \return Return a reference of the ntofLib object
     */
    ntof::lib::NtofLib &getNtofLib();

    /*
     * \brief Destroy all the channels objects
     */
    void clearChannels();

    /*
     * \brief Create all the channels and initialize them
     */
    void initChannelList();

protected slots:
    void onConfigUpdate();

protected:
    /*
     * \brief Constructor of the class
     */
    FileReader();

    /*
     * \brief Load the channels data into the memory
     * \param channel: where the data will be stored
     */
    void setChannel(Channel *channel);

    /*
     * \brief Check if the ACQC can be found for the actual event
     * \param detectorType: Type of the detector
     * \param detectorId: ID of the detector
     */
    bool checkACQC(const std::string &detectorType, uint32_t detectorId);

    bool readFiles(const std::vector<std::string> &files, bool closeFiles);

    void addDAQFiles(const std::string &fullpath,
                     const std::string &runFile,
                     std::vector<std::string> &missingFiles);

    /*
     * \brief Load the channels data into the memory
     * \param id: ID of the channel
     * \return Return true if the channel exists
     */
    bool channelExists(int32_t id);

    static FileReader *instance_;   //!< FileReader instance
    std::list<Channel *> channels_; //!< List of all the channels stored
    bool plotLine_;   //!< State of the plotLine      //TODO move to Custom plot
    bool plotPoints_; //!< State of the plotPoints    //TODO move to Custom plot
    ntof::lib::NtofLib ntofLib_; //!< Library used to read the ntof files
    // int32_t eventNumber_;                         //!< Avoid to read it from
    // the lib each time
    std::vector<ntof::lib::ACQC_INFO> acqcList_; //!< Avoid to read it from the
                                                 //!< lib each time
};
} // namespace eventDisplay
} // namespace ntof
#endif // FILEREADER_H
