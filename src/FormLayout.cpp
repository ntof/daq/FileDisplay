/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-22T11:30:23+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "FormLayout.h"

#include <QDebug>
#include <QLabel>
#include <QSpacerItem>

FormLayout::FormLayout(QWidget *parent) : QGridLayout(parent)
{
    addItem(
        new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding),
        0, 1);
    addItem(
        new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding),
        1, 1);
    setRowStretch(0, 1);
    setRowStretch(1, 1);
}

FormLayout::FormLayout() : QGridLayout()
{
    addItem(
        new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding),
        0, 1);
    addItem(
        new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding),
        1, 1);
    setRowStretch(0, 1);
    setRowStretch(1, 1);
}

void FormLayout::addRow(const QString &label, QWidget *widget)
{
    int row = rowCount();
    QLayoutItem *spacer = takeAt(count() - 1); /* take spacer */
    addWidget(new QLabel(label), --row, 0, Qt::AlignVCenter);

    Qt::Alignment align = Qt::AlignVCenter;
    if (!(widget->sizePolicy().expandingDirections() & Qt::Horizontal))
        align |= Qt::AlignRight; /* setting this on expanding widgets will
                                    prevent full expansion on QGridLayout */

    addWidget(widget, row, 1, align);
    setRowStretch(row, 1);
    addItem(spacer, row + 1, 1);
    setRowStretch(row + 1, 1);
}
