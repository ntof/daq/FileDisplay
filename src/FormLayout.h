/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-22T11:27:25+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef FORMLAYOUT_H_
#define FORMLAYOUT_H_

#include <QGridLayout>
#include <QString>
#include <QWidget>

/**
 * @brief Simple FormLayout.
 * @details QFormLayout doesn't expand its content, let's make a simple wrapper
 * on top of QGridLayout.
 */
class FormLayout : public QGridLayout
{
public:
    explicit FormLayout(QWidget *parent);
    explicit FormLayout();

    void addRow(const QString &label, QWidget *widget);
};

#endif
