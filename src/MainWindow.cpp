#include "MainWindow.h"

#include <cmath>
#include <iostream>

#include <QApplication>
#include <QCheckBox>
#include <QFileDialog>
#include <QFormLayout>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QMenuBar>
#include <QMessageBox>
#include <QPushButton>
#include <QScrollArea>
#include <QScrollBar>
#include <QString>
#include <QToolBar>

#include <NtofLibException.h>
#include <unistd.h>

#include "Channel.h"
#include "Config.h"
#include "FileReader.h"
#include "MenuBar.h"
#include "OptionPanel.h"
#include "SearchFile.h"
#include "TableParams.h"
#include "flowlayout.h"

namespace ntof {
namespace eventDisplay {
MainWindow *MainWindow::instance_ = NULL;

/*!
 *  \brief Get the instance of the MainWindow object and initialize the instance
 * if the pointer is null \return A pointer on the MainWindow object
 */
MainWindow *MainWindow::getInstance()
{
    if (instance_ == NULL)
    {
        instance_ = new MainWindow();
    }
    return instance_;
}

/*
 * \brief Create the content of the main window
 */
void MainWindow::create()
{
    summaryPanel_ = new SummaryPanel();

    setWindowTitle(tr("File display"));
    createMenu();
    createWidget();
    setTGamma();
    setDistance();
    updateRangeTitle();

    connect(customPlot_, SIGNAL(zoomModeChanged(bool)), getMenuBar(),
            SLOT(zoomAction(bool))); /* reflect zoom mode */
    resize(Config::getInstance()->getWidth(),
           Config::getInstance()->getHeight());
    setDebugMessage("Ready");
}

/*
 * \brief update the range titles of the custom plot
 */
void MainWindow::updateRangeTitle()
{
    if (customPlot_ != NULL)
    {
        customPlot_->setLabelAxisX("t [ns]");
        if (Config::getInstance()->getDataUnit() == "mv")
        {
            customPlot_->setLabelAxisY("Amplitude [mV]");
        }
        else
        {
            customPlot_->setLabelAxisY("Amplitude [ADC]");
        }
    }
}

/*
 * \brief Constructor of the class
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    menu_(NULL),
    searchFile_(new SearchFile()),
    runNumberLineEdits_(NULL),
    experimentLineEdits_(NULL),
    segmentNumberLineEdits_(NULL),
    eventsInRunLineEdits_(NULL),
    eventNumberLineEdits_(NULL),
    BCTLineEdits_(NULL),
    tGammaLineEdits_(NULL),
    distanceLineEdits_(NULL),
    coordXLineEdits_(NULL),
    coordYLineEdits_(NULL),
    energyLineEdits_(NULL),
    tofLineEdits_(NULL),
    detectorsList_(NULL),
    plotButton_(NULL),
    selectAll_(NULL),
    unselectAll_(NULL),
    scrollArea_(NULL),
    previousEventButton_(NULL),
    nextEventButton_(NULL),
    paramsGroupBox_(NULL),
    paramsTable_(NULL),
    debugToolbarGroupBox_(NULL),
    debugToolbarLabel_(NULL),
    customPlot_(NULL),
    optionsPanel_(new OptionPanel()),
    rangeSelectorPanel_(new RangeSelectorPanel()),
    summaryPanel_(NULL),
    toReplot_(false),
    actionsOnGoing_(0)
{
    // Reset the range
    rangeSavedReset();
}

/*
 * \brief Destructor of the class
 */
MainWindow::~MainWindow()
{
    // The life time of this object is the lifetime of the application but ...
    delete menu_;
    delete searchFile_;
    delete runNumberLineEdits_;
    delete experimentLineEdits_;
    delete segmentNumberLineEdits_;
    delete eventsInRunLineEdits_;
    delete eventNumberLineEdits_;
    delete BCTLineEdits_;
    delete tGammaLineEdits_;
    delete coordXLineEdits_;
    delete coordYLineEdits_;
    delete energyLineEdits_;
    delete tofLineEdits_;
    delete detectorsList_;
    delete plotButton_;
    delete selectAll_;
    delete unselectAll_;
    delete scrollArea_;
    delete previousEventButton_;
    delete nextEventButton_;
    delete paramsGroupBox_;
    delete paramsTable_;
    delete customPlot_;
    delete optionsPanel_;
    delete rangeSelectorPanel_;
    delete instance_;
}

bool MainWindow::isToReplot() const
{
    return toReplot_;
}

void MainWindow::setToReplot(bool toReplot)
{
    toReplot_ = toReplot;
}

/*
 * \brief Create the menu of the windows
 */
void MainWindow::createMenu()
{
    menu_ = new MenuBar();
    QMenuBar *menuBar = menu_->getMenuBar();
    if (menuBar != NULL)
    {
        setMenuBar(menuBar);
    }

    QToolBar *toolBar = menu_->getToolBar();
    if (toolBar != NULL)
    {
        addToolBar(toolBar);
    }
}

/* ************************************************************* */
/* GETTERS                                                       */
/* ************************************************************* */
/*
 * \brief Get the menu bar object
 * \return Return a pointer on the object
 */
MenuBar *MainWindow::getMenuBar()
{
    return menu_;
}

/*
 * \brief Get the search file object
 * \return Return a pointer on the object
 */
SearchFile *MainWindow::getSearchFile()
{
    return searchFile_;
}

/*
 * \brief Update the element and its children
 */
void MainWindow::updateElement()
{
    menu_->updateElement();
    paramsTable_->clear();

    FileReader::getInstance()->updateElement();
    customPlot_->legend->setVisible(
        FileReader::getInstance()->hasChannelEnabled());
    debugToolbarGroupBox_->setVisible(Config::getInstance()->getModeDebug() ==
                                      "on");

    paramsTable_->updateElement();
    detectorsList_->update();

    if (toReplot_)
    {
        customPlot_->replot();
        toReplot_ = false;
    }
}

/* ************************************************************* */
/* ACTIONS                                                       */
/* ************************************************************* */
/*
 * \brief Create the main widget
 */
void MainWindow::createWidget()
{
    std::string style("");
    QWidget *mainWidget = new QWidget();
    QGroupBox *mainInfoGroupBox = createMainInfoBox();
    QGroupBox *positionGroupBox = createPositionBox();
    QGroupBox *detectorsGroupBox = createDetectorBox();
    QGroupBox *chartGroupBox = createChartBox();
    paramsGroupBox_ = createParamsBox();
    debugToolbarGroupBox_ = createDebugToolBarBox();

    QGroupBox *rightMenuGroupBox = new QGroupBox();
    rightMenuGroupBox->setObjectName(tr("rightMenu"));
    style = "#rightMenu{padding:-1px -6px -1px -1px;}";
    rightMenuGroupBox->setStyleSheet(tr(style.c_str()));
    rightMenuGroupBox->setSizePolicy(
        QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding));

    QVBoxLayout *rightLayout = new QVBoxLayout;
    rightLayout->addWidget(mainInfoGroupBox);
    rightLayout->addWidget(positionGroupBox);
    rightLayout->addWidget(detectorsGroupBox);
    rightLayout->setStretch(2, 1);
    rightMenuGroupBox->setLayout(rightLayout);

    QGroupBox *leftMenuGroupBox = new QGroupBox();
    leftMenuGroupBox->setObjectName(tr("leftMenu"));
    style = "#leftMenu{margin:0 0 0 0;}";
    leftMenuGroupBox->setStyleSheet(tr(style.c_str()));

    QVBoxLayout *leftLayout = new QVBoxLayout;
    leftLayout->addWidget(chartGroupBox);
    leftLayout->addWidget(paramsGroupBox_);
    leftLayout->addWidget(debugToolbarGroupBox_);
    leftMenuGroupBox->setLayout(leftLayout);

    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(rightMenuGroupBox);
    mainLayout->addWidget(leftMenuGroupBox);
    mainWidget->setLayout(mainLayout);
    setCentralWidget(mainWidget);
}

/* ************************************************************* */
/* CREATE BOXES                                                  */
/* ************************************************************* */
/*
 * \brief Create the main info group box
 * \return Return the object initialized
 */
QGroupBox *MainWindow::createMainInfoBox()
{
    QGroupBox *mainInfoGroupBox;
    initGroupBox(mainInfoGroupBox, "Main info", "mainInfo");
    QFormLayout *layout = new QFormLayout;
    layout->setHorizontalSpacing(10);

    // Run number
    initLabel(experimentLineEdits_, "Experiment", layout);
    initLabel(runNumberLineEdits_, "Run number", layout);
    initLabel(eventNumberLineEdits_, "Trigger number", layout);
    initLabel(segmentNumberLineEdits_, "Segment number", layout);
    initLabel(eventsInRunLineEdits_, "Triggers in run", layout);
    initLabel(BCTLineEdits_, "BCT", layout);
    initLabel(tGammaLineEdits_, "T gammma", layout);
    initLabel(distanceLineEdits_, "L", layout);

    mainInfoGroupBox->setLayout(layout);
    return mainInfoGroupBox;
}

/*
 * \brief Create the position group box
 * \return Return the object initialized
 */
QGroupBox *MainWindow::createPositionBox()
{
    QGroupBox *positionGroupBox;
    initGroupBox(positionGroupBox, "Positions", "positions");
    QFormLayout *layout = new QFormLayout;
    layout->setHorizontalSpacing(10);

    initLabel(coordXLineEdits_, "Position X", layout);
    initLabel(coordYLineEdits_, "Position Y", layout);
    initLabel(energyLineEdits_, "E(eV)", layout);
    initLabel(tofLineEdits_, "TOF", layout);

    positionGroupBox->setLayout(layout);
    return positionGroupBox;
}

/*
 * \brief Create the detector group box
 * \return Return the object initialized
 */
QGroupBox *MainWindow::createDetectorBox()
{
    QGroupBox *detectorsGroupBox;
    initGroupBox(detectorsGroupBox, "Detectors list", "detectors");
    detectorsGroupBox->setLayout(new QVBoxLayout);

    detectorsList_ = new QWidget();
    detectorsList_->setLayout(new QVBoxLayout);
    detectorsList_->layout()->setSpacing(0);
    detectorsList_->setSizePolicy(
        QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding));

    scrollArea_ = new QScrollArea(this);
    scrollArea_->setWidgetResizable(true);
    scrollArea_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scrollArea_->setSizePolicy(
        QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding));
    scrollArea_->setWidget(detectorsList_);
    detectorsGroupBox->layout()->addWidget(scrollArea_);

    plotButton_ = new QPushButton(tr("Apply selection"));
    plotButton_->setEnabled(false);
    QObject::connect(plotButton_, SIGNAL(clicked()), this, SLOT(plot()));

    QGroupBox *selectionButtons = new QGroupBox();
    selectionButtons->setObjectName(tr("selectionButtons"));
    selectionButtons->setStyleSheet(
        "#selectionButtons{padding:-1px -6px -1px -1px;}");

    selectAll_ = new QPushButton(tr("Select all"));
    unselectAll_ = new QPushButton(tr("Clear selection"));
    QObject::connect(selectAll_, SIGNAL(clicked()), this, SLOT(selectAll()));
    QObject::connect(unselectAll_, SIGNAL(clicked()), this, SLOT(unselectAll()));

    QGridLayout *selectionLayout = new QGridLayout();
    selectionLayout->addWidget(selectAll_, 0, 0);
    selectionLayout->addWidget(unselectAll_, 0, 1);
    selectionLayout->addWidget(plotButton_, 1, 0, 1, 2);
    selectionButtons->setLayout(selectionLayout);
    detectorsGroupBox->layout()->addWidget(selectionButtons);

    return detectorsGroupBox;
}

/*
 * \brief Create the chart group box
 * \return Return the object initialized
 */
QGroupBox *MainWindow::createChartBox()
{
    QGroupBox *chartGroupBox;
    initGroupBox(chartGroupBox, "Signal viewer", "chartGroupBox");
    QGridLayout *layout = new QGridLayout;

    customPlot_ = new CustomPlotZoom();
    customPlot_->setNoAntialiasingOnDrag(true);
    customPlot_->setNotAntialiasedElements(QCP::aeAll);

    customPlot_->addLayer("back");
    customPlot_->addLayer("front");
    customPlot_->setCurrentLayer("front");

    customPlot_->moveLayer(customPlot_->layer("legend"),
                           customPlot_->layer("front"), QCustomPlot::limAbove);
    customPlot_->layer("legend")->setMode(QCPLayer::lmBuffered);

    customPlot_->moveLayer(customPlot_->layer("overlay"),
                           customPlot_->layer("legend"), QCustomPlot::limAbove);

    QFont legendFont = font();  // start out with MainWindow's font..
    legendFont.setPointSize(9); // and make a bit smaller for legend
    customPlot_->legend->setFont(legendFont);
    customPlot_->legend->setBrush(QBrush(Qt::white));
    customPlot_->legend->setLayer("legend");

    connect(customPlot_, SIGNAL(selectionChangedByUser()), this,
            SLOT(selectionChanged()));
    connect(customPlot_, SIGNAL(zoomHistoryChanged()), this,
            SLOT(onZoomChanged()));

    previousEventButton_ = new QPushButton(tr("Previous trigger"));
    QObject::connect(previousEventButton_, SIGNAL(clicked()), this,
                     SLOT(previousEvent()));
    previousEventButton_->setEnabled(false);
    nextEventButton_ = new QPushButton(tr("Next trigger"));
    QObject::connect(nextEventButton_, SIGNAL(clicked()), this,
                     SLOT(nextEvent()));
    nextEventButton_->setEnabled(false);

    layout->addWidget(customPlot_, 0, 0, 10, 5);
    layout->addWidget(previousEventButton_, 10, 0);
    layout->addWidget(nextEventButton_, 10, 4);

    chartGroupBox->setLayout(layout);

    QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    chartGroupBox->setSizePolicy(sizePolicy);

    return chartGroupBox;
}

/*
 * \brief Create the parameters group box
 * \return Return the object initialized
 */
QGroupBox *MainWindow::createParamsBox()
{
    QGroupBox *paramsGroupBox;
    initGroupBox(paramsGroupBox, "Parameters", "paramsGroupBox");
    QVBoxLayout *layout = new QVBoxLayout;

    paramsTable_ = new TableParams();
    std::string style = "#tableParam{padding:0px 0px 0px 0px;}";
    paramsTable_->setStyleSheet(tr(style.c_str()));
    layout->addWidget(paramsTable_);
    paramsGroupBox->setLayout(layout);
    paramsGroupBox->setMaximumHeight(200);

    return paramsGroupBox;
}

/*
 * \brief Create the debug toolbar group box
 * \return Return the object initialized
 */
QGroupBox *MainWindow::createDebugToolBarBox()
{
    QGroupBox *debugToolBarGroupBox;
    initGroupBox(debugToolBarGroupBox, "Debug", "debugToolBarGroupBox");
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setAlignment(Qt::AlignRight);

    debugToolbarLabel_ = new QLabel();
    layout->addWidget(debugToolbarLabel_);
    debugToolBarGroupBox->setLayout(layout);
    return debugToolBarGroupBox;
}

/* ************************************************************* */
/* INIT COMPONENTS                                               */
/* ************************************************************* */
/*
 * \brief Create a group box
 * \param groupBox: The group box that will be created
 * \param text: Title of the object
 * \param name: Name of the object
 * \return Return the pointer on the group box
 */
QGroupBox *MainWindow::initGroupBox(QGroupBox *&groupBox,
                                    std::string text,
                                    std::string name)
{
    groupBox = new QGroupBox(tr(text.c_str()));
    groupBox->setObjectName(name.c_str());
    QString style;
    style = QString("#%1").arg(name.c_str()) +
        "{border:2px solid gray;border-radius:3px;margin-top: 1ex;padding-top: "
        "1ex;}";
    style += QString("#%1::title").arg(name.c_str()) +
        "{subcontrol-origin: margin;subcontrol-position: top left;padding:0px "
        "10px 0px 10px;}";
    groupBox->setStyleSheet(style);
    return groupBox;
}

/*
 * \brief Create a line edit
 * \param lineEdit: The line edit that will be created
 * \param text: Title of the object
 * \param layout: The layout to anchor the element after creation
 */
void MainWindow::initLabel(QLabel *&lineEdit,
                           std::string text,
                           QFormLayout *&layout)
{
    lineEdit = new QLabel();
    lineEdit->setFrameStyle(QFrame::StyledPanel);
    lineEdit->setFrameShadow(QFrame::Sunken);
    lineEdit->setTextInteractionFlags(Qt::TextSelectableByMouse |
                                      Qt::TextSelectableByKeyboard);
    lineEdit->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding,
                                        QSizePolicy::MinimumExpanding));
    lineEdit->setAlignment(Qt::AlignRight);
    layout->addRow(new QLabel(tr(text.c_str())), lineEdit);
}

/* ************************************************************* */
/* SET MESSAGE                                                   */
/* ************************************************************* */
/*
 * \brief Change the content of the label in the debug area
 * \param message: Content to set
 */
void MainWindow::setDebugMessage(std::string message)
{
    if (message != "")
    {
        std::cout << message << std::endl;
    }

    if (debugToolbarLabel_ != NULL)
    {
        debugToolbarLabel_->setText(tr(message.c_str()));
    }
}

/* ************************************************************* */
/* ACTIONS COUNTER                                               */
/* ************************************************************* */
/*
 * \brief Add an element to the count of the actions on going
 */
void MainWindow::addActionOnGoing()
{
    ++actionsOnGoing_;
    if (actionsOnGoing_ > 0)
    {
        this->setCursor(Qt::WaitCursor);
    }
}

/*
 * \brief Remove an element to the count of the actions on going
 */
void MainWindow::removeActionOnGoing()
{
    --actionsOnGoing_;
    if (actionsOnGoing_ <= 0)
    {
        this->setCursor(Qt::ArrowCursor);
        // setDebugMessage("");
    }
}

/* ************************************************************* */
/* READ DATA FILE                                                */
/* ************************************************************* */
/*
 * \brief Set the content of the main info group box
 */
void MainWindow::initMainInfo()
{
    runNumberLineEdits_->setText(QString::number(
        FileReader::getInstance()->getNtofLib().getRCTR()->getRunNumber()));
    experimentLineEdits_->setText(QString::fromStdString(
        FileReader::getInstance()->getNtofLib().getRCTR()->getExperiment()));
    setDistance();
}

/*
 * \brief Set the segment number
 */
void MainWindow::setSegmentNumber()
{
    if (FileReader::getInstance()->getNtofLib().isDefinedEVEH())
    {
        const ntof::lib::Header header =
            FileReader::getInstance()->getNtofLib().getEVEH()->getHeader();
        ntof::lib::FILE_INFO fileInfo =
            FileReader::getInstance()->getNtofLib().getFileInfo(header.file);
        // std::cout << "Full path: " << fileInfo.filename << std::endl;
        int32_t segmentNumber = fileInfo.segmentNumber;
        segmentNumberLineEdits_->setText(QString::number(segmentNumber));
    }
}

/*
 * \brief Set the trigger number being display value
 */
void MainWindow::setTriggerDisplay()
{
    try
    {
        eventNumberLineEdits_->setText(QString::number(
            FileReader::getInstance()->getNtofLib().getEVEH()->getEventNumber()));
    }
    catch (ntof::lib::NtofLibException &ex)
    {
        std::cerr << "An error occurred to display the 'trigger number': "
                  << ex.getMessage() << std::endl;
    }
}

/*
 * \brief Set the tGamma value
 */
void MainWindow::setTGamma()
{
    tGammaLineEdits_->setText(
        QString::number(Config::getInstance()->getTGamma()) + " ns");
}

/*
 * \brief Set the distance value
 */
void MainWindow::setDistance()
{
    if (experimentLineEdits_->text() == "EAR1")
    {
        distanceLineEdits_->setText(
            QString::number(Config::getInstance()->getDistanceEAR1()) + " m");
    }
    else if (experimentLineEdits_->text() == "EAR2")
    {
        distanceLineEdits_->setText(
            QString::number(Config::getInstance()->getDistanceEAR2()) + " m");
    }
    else
    {
        distanceLineEdits_->setText("");
    }
}

/*
 * \brief Set the BCT value
 */
void MainWindow::setBCT()
{
    // Get the BCT from file
    try
    {
        BCTLineEdits_->setText(QString::number(FileReader::getInstance()
                                                   ->getNtofLib()
                                                   .getEVEH()
                                                   ->getBeamIntensity()));
    }
    catch (ntof::lib::NtofLibException &ex)
    {
        std::cerr << "[ERROR on BCT intensity]: " << ex.getMessage()
                  << std::endl;
    }
}

/*
 * \brief Initialize the reader
 */
void MainWindow::addToFileReader()
{
    FileReader::getInstance()->initReader();
}

/* ************************************************************* */
/* SLOTS                                                         */
/* ************************************************************* */

/*
 * \brief Select all the detector in the list
 */
void MainWindow::selectAll()
{
    FileReader::getInstance()->setActive();
}

/*
 * \brief Unselect all the detector in the list
 */
void MainWindow::unselectAll()
{
    FileReader::getInstance()->setInactive();
}

/* ************************************************************* */
/* BUTTONS STATES                                                */
/* ************************************************************* */
/*
 * \brief Set the state of the button previous event (enable if previous event
 * has been found, disable else)
 */
void MainWindow::enablePreviousEvent()
{
    if (FileReader::getInstance()->getNtofLib().isDefinedEVEH())
    {
        previousEventButton_->setEnabled(true);
    }
}

/*
 * \brief Set the state of the button next event (enable if next event has been
 * found, disable else)
 */
void MainWindow::enableNextEvent()
{
    if (FileReader::getInstance()->getNtofLib().isDefinedEVEH())
    {
        nextEventButton_->setEnabled(true);
    }
}

/*
 * \brief Enable or disable the plot button if necessary
 */
void MainWindow::enablePlot()
{
    if (FileReader::getInstance()->hasChannelEnabled())
    {
        plotButton_->setEnabled(true);
    }
    else
    {
        plotButton_->setEnabled(false);
    }
}

/*
 * \brief Enable or disable the buttons if necessary
 */
void MainWindow::enableButton()
{
    enablePreviousEvent();
    enableNextEvent();
    enablePlot();
}

/* ************************************************************* */
/* BUTTONS ACTIONS                                               */
/* ************************************************************* */

/*
 * \brief Load an event
 * \param eventNumber: ID of the validated event
 * \return Return true if the event has been reached, to update the plot
 */
bool MainWindow::loadEvent(int32_t eventNumber)
{
    if (eventNumber != -1)
    {
        if (FileReader::getInstance()->getNtofLib().getValidatedEvent(
                eventNumber))
        {
            // std::cerr << "Actual event number: " <<
            // FileReader::getInstance()->getNtofLib().getEVEH()->getEventNumber()
            // << std::endl;
            FileReader::getInstance()->initChannelList();
            FileReader::getInstance()->plotData(false);

            eventNumberLineEdits_->setText(
                QString::number(FileReader::getInstance()
                                    ->getNtofLib()
                                    .getEVEH()
                                    ->getEventNumber()));
            enableButton();

            return true;
        }
        else
        {
            showCriticalMessage("Error", "Validated trigger number not found.");
        }
    }
    return false;
}

/*
 * \brief Go to the previous event
 */
void MainWindow::previousEvent()
{
    addActionOnGoing();
    ntof::lib::NtofLib &ntofLib = FileReader::getInstance()->getNtofLib();

    // Store the actual event number
    uint32_t oldEventNumber = ntofLib.getEVEH()->getEventNumber();

    // Get the data related to the next event
    ntof::lib::EVENT_INFO previousEventInfo = ntofLib.getPreviousEventInfo();

    std::cout << "Old event number = " << oldEventNumber << std::endl;
    std::cout << "Previous event number = " << previousEventInfo.validatedNumber
              << std::endl;

    // If there is no next event info, getNextEventInfo() returns zeroed object
    bool needsNewFile = std::abs(static_cast<int>(
                            oldEventNumber -
                            previousEventInfo.validatedNumber)) > 1;

    if (!needsNewFile)
    {
        // Just loads the previous event
        // If the last event of the list is being plotted
        if (!ntofLib.hasPreviousEvent())
        {
            showCriticalMessage("Error", "There are no previous events to load");
        }
        else
        {
            // Go to next event within the same file.
            if (FileReader::getInstance()->previousEvent())
            {
                eventNumberLineEdits_->setText(
                    QString::number(FileReader::getInstance()
                                        ->getNtofLib()
                                        .getEVEH()
                                        ->getEventNumber()));
                enableButton();
            }
        }
    }
    else
    {
        // Load a new file
        const ntof::lib::Header header = ntofLib.getACQC()->getHeader();
        ntof::lib::FILE_INFO fileInfoACQC = ntofLib.getFileInfo(header.file);
        std::string filename = fileInfoACQC.filename;
        std::string fullpath = fileInfoACQC.fullpath;
        std::string path = fullpath.substr(0, fullpath.find_last_of('/'));
        bool isOnXroot = fileInfoACQC.hasFlag(lib::FILE_INFO::ON_CASTOR);

        // Show a MessageBox if is from xroot
        int32_t result = QMessageBox::AcceptRole;
        if (isOnXroot)
        {
            // Ask user if it wants to load another file from xroot
            QMessageBox msgBox;
            msgBox.setText("The previous event is missing in this file.");
            msgBox.setInformativeText("Do you want to open the previous file?");
            msgBox.addButton(tr("Yes"), QMessageBox::AcceptRole);
            msgBox.addButton(tr("No"), QMessageBox::RejectRole);
            result = msgBox.exec();
        }

        if (result == QMessageBox::AcceptRole)
        {
            // Search previous file
            std::string previousFile;

            if (isOnXroot)
            {
                previousFile = MainWindow::getInstance()
                                   ->getSearchFile()
                                   ->getPreviousCastorFile(path, filename);
            }
            else
            {
                previousFile = MainWindow::getInstance()
                                   ->getSearchFile()
                                   ->getPreviousLocalFile(path, filename);
            }

            // Open next file
            if (previousFile.empty())
            {
                showCriticalMessage("Error", "There are no more events to load");
            }
            else
            {
                // Save current zoom and active channels
                const Zoom savedZoom = customPlot_->getZoom();
                std::vector<int32_t> active_channels = getActiveChannels();

                // Openfile expect a vector of filenames
                if (!FileReader::getInstance()->openFile({previousFile}, true,
                                                         true, isOnXroot))
                {
                    showCriticalMessage(
                        "Error", "The system failed to open another file");
                }

                loadEvent(oldEventNumber - 1);

                // Restore zoom and active channels
                customPlot_->resetZoom();
                if (!active_channels.empty())
                {
                    customPlot_->setZoom(savedZoom);
                    setActiveChannels(active_channels);
                    plot();
                }

                // Update workspace
                Config::getInstance()->setWorkspace(path);
            }
        }
    }
    removeActionOnGoing();
}

/*
 * \brief get Active channels
 */
std::vector<int32_t> MainWindow::getActiveChannels() const
{
    std::vector<int32_t> active_channels;
    // If the legend is visible it means that some channel has been
    // plotted. See plot()
    if (customPlot_->legend->visible())
    {
        for (Channel *channel : FileReader::getInstance()->getChannels())
        {
            if (channel->isActive())
                active_channels.push_back(channel->getId());
        }
    }
    return active_channels;
}

/*
 * \brief set Active channels
 */
void MainWindow::setActiveChannels(const std::vector<int32_t> &channels)
{
    for (int32_t id : channels)
    {
        FileReader::getInstance()->setActive(id);
    }
}

/*
 * \brief Save the actual configuration to be able to reset it when the new
 * event will be loaded
 */
void MainWindow::saveConfiguration()
{
    setDebugMessage("Save the actual configuration");

    // save the actual range
    rangeSave_ = customPlot_->getZoom();
    channelsSave_.clear();

    // save the actual configuration
    std::list<Channel *> channels = FileReader::getInstance()->getChannels();
    for (std::list<Channel *>::iterator it = channels.begin();
         it != channels.end(); ++it)
    {
        if ((*it)->isParamsShown() || (*it)->isActive())
        {
            DetectorSave save;
            save.name = (*it)->getDetectorType();
            save.id = (*it)->getDetectorId();
            save.isActive = (*it)->isActive();
            save.showParams = (*it)->isParamsShown();
            save.showThreshold = (*it)->isShowThreshold();
            save.dataArea = (*it)->isDisplayDataArea();
            channelsSave_.push_back(save);
        }
    }
}

/*
 * \brief Apply the configuration saved
 */
void MainWindow::applyConfiguration()
{
    setDebugMessage("Apply the configuration previously saved");

    // apply the old configuration
    std::list<Channel *> channels = FileReader::getInstance()->getChannels();
    for (std::list<Channel *>::iterator it = channels.begin();
         it != channels.end(); ++it)
    {
        for (std::list<DetectorSave>::iterator save = channelsSave_.begin();
             save != channelsSave_.end(); ++save)
        {
            if ((*save).name == (*it)->getDetectorType() &&
                (*save).id == (*it)->getDetectorId())
            {
                if ((*save).isActive)
                {
                    (*it)->setActive();
                }
                if ((*save).showParams)
                {
                    (*it)->showParams();
                }
                if ((*save).showThreshold)
                {
                    (*it)->showThreshold();
                }
                if ((*save).dataArea)
                {
                    (*it)->displayDataArea(false);
                }
                break;
            }
        }
    }

    setToReplot(false);
    updateElement();
}

/*
 * \brief Reset the range
 */
void MainWindow::rangeSavedReset()
{
    addActionOnGoing();
    setDebugMessage("Reset saved range");

    // Reset the range
    rangeSave_ = Zoom();

    removeActionOnGoing();
}

/*
 * \brief Apply the range saved
 */
void MainWindow::applyRangeSaved()
{
    addActionOnGoing();
    setDebugMessage("Apply saved range");

    if (QCPRange::validRange(rangeSave_.xAxis) &&
        QCPRange::validRange(rangeSave_.yAxis))
    {
        // apply the old range
        customPlot_->setZoom(rangeSave_);

        // Reset the range
        rangeSavedReset();
    }

    removeActionOnGoing();
}
/*
 * \brief Get the run number
 */
std::string MainWindow::getRunNumber()
{
    return runNumberLineEdits_->text().toStdString();
}

/*
 * \brief Get the trigger number
 */
std::string MainWindow::getTriggerNumber()
{
    return eventNumberLineEdits_->text().toStdString();
}

void MainWindow::showCriticalMessage(const std::string &title,
                                     const std::string &message)
{
    QString qTitle = QObject::tr(title.c_str());
    QString qMessage = QObject::tr(message.c_str());
    QMessageBox::critical(MainWindow::getInstance(), qTitle, qMessage,
                          QMessageBox::Ok);
}

/*
 * \brief Go to the next event
 */
void MainWindow::nextEvent()
{
    addActionOnGoing();
    ntof::lib::NtofLib &ntofLib = FileReader::getInstance()->getNtofLib();

    // Store the actual event number
    uint32_t oldEventNumber = ntofLib.getEVEH()->getEventNumber();

    // Get the data related to the next event
    ntof::lib::EVENT_INFO nextEventInfo = ntofLib.getNextEventInfo();

    std::cout << "Old event number = " << oldEventNumber << std::endl;
    std::cout << "Next event number = " << nextEventInfo.validatedNumber
              << std::endl;

    // If there is no next event info, getNextEventInfo() returns zeroed object
    bool needsNewFile = std::abs(static_cast<int>(nextEventInfo.validatedNumber -
                                                  oldEventNumber)) > 1;

    if (!needsNewFile)
    {
        // Just loads the next event
        // If the last event of the list is being plotted
        if (!ntofLib.hasNextEvent())
        {
            showCriticalMessage("Error", "There are no next events to load");
        }
        else
        {
            // Go to next event within the same file.
            if (FileReader::getInstance()->nextEvent())
            {
                eventNumberLineEdits_->setText(
                    QString::number(FileReader::getInstance()
                                        ->getNtofLib()
                                        .getEVEH()
                                        ->getEventNumber()));
                enableButton();
            }
        }
    }
    else
    {
        // Load a new file
        const ntof::lib::Header header = ntofLib.getACQC()->getHeader();
        ntof::lib::FILE_INFO fileInfoACQC = ntofLib.getFileInfo(header.file);
        std::string filename = fileInfoACQC.filename;
        std::string fullpath = fileInfoACQC.fullpath;
        std::string path = fullpath.substr(0, fullpath.find_last_of('/'));
        bool isOnXroot = fileInfoACQC.hasFlag(lib::FILE_INFO::ON_CASTOR);

        // Show messagebox if is from xroot
        int32_t result = QMessageBox::AcceptRole;
        if (isOnXroot)
        {
            // Ask user if it wants to load another file from xroot
            QMessageBox msgBox;
            msgBox.setText("The next event is missing in this file.");
            msgBox.setInformativeText("Do you want to open the next file?");
            msgBox.addButton(tr("Yes"), QMessageBox::AcceptRole);
            msgBox.addButton(tr("No"), QMessageBox::RejectRole);
            result = msgBox.exec();
        }

        if (result == QMessageBox::AcceptRole)
        {
            // Search next file
            std::string nextFile;

            if (isOnXroot)
            {
                nextFile = MainWindow::getInstance()
                               ->getSearchFile()
                               ->getNextCastorFile(path, filename);
            }
            else
            {
                nextFile =
                    MainWindow::getInstance()->getSearchFile()->getNextLocalFile(
                        path, filename);
            }

            // Open next file
            if (nextFile.empty())
            {
                showCriticalMessage("Error", "There are no more events to load");
            }
            else
            {
                // Save current zoom and active channels
                const Zoom savedZoom = customPlot_->getZoom();
                std::vector<int32_t> active_channels;
                // If the legend is visible it means that some channel has been
                // plotted. See plot()
                if (customPlot_->legend->visible())
                {
                    for (Channel *channel :
                         FileReader::getInstance()->getChannels())
                    {
                        if (channel->isActive())
                            active_channels.push_back(channel->getId());
                    }
                }

                // Openfile expect a vector of filenames
                if (!FileReader::getInstance()->openFile({nextFile}, true, true,
                                                         isOnXroot))
                {
                    showCriticalMessage(
                        "Error", "The system failed to open another file");
                }

                // Restore zoom and active channels
                customPlot_->resetZoom();
                if (!active_channels.empty())
                {
                    customPlot_->setZoom(savedZoom);
                    // Reactive saved channels
                    for (int32_t channel_id : active_channels)
                        FileReader::getInstance()->setActive(channel_id);
                    plot();
                }

                // Update workspace
                Config::getInstance()->setWorkspace(path);
            }
        }
    }
    removeActionOnGoing();
}

/*
 * \brief Jump directly to the last event
 */
void MainWindow::lastEvent()
{
    FileReader::getInstance()->lastEvent();
    eventNumberLineEdits_->setText(QString::number(
        FileReader::getInstance()->getNtofLib().getEVEH()->getEventNumber()));
    enableButton();
}

/*
 * \brief Jump to an event
 * \param eventID: ID of the event to set
 */
void MainWindow::setEventID(int32_t eventID)
{
    bool res = FileReader::getInstance()->getNtofLib().getEvent(eventID);
    if (!res)
    {
        showCriticalMessage("Event not found",
                            "The event wanted can't be found in the file.");
    }
}

/*
 * \brief Plot the data relative to the detector selected
 * \param replot: The plot area is not redraw if this parameter is equal to
 * false
 */
void MainWindow::plot(bool replot)
{
    // Update the legend
    if (FileReader::getInstance()->hasChannelEnabled())
    {
        customPlot_->legend->setVisible(true);
    }
    else
    {
        customPlot_->legend->setVisible(false);
    }

    if (!FileReader::getInstance()->getNtofLib().isDefinedEVEH())
    {
        FileReader::getInstance()->nextEvent(replot);
    }
    else
    {
        FileReader::getInstance()->plotData(replot);
    }
    enableButton();
}

/*
 * \brief Reload all the configuration after a data unit change
 * \param replot: The plot area is not redraw if this parameter is equal to
 * false
 */
void MainWindow::reloadFiles(bool replot)
{
    // Save the Event number
    int32_t validatedNumber = -1;
    if (FileReader::getInstance()->getNtofLib().isDefinedEVEH())
    {
        validatedNumber =
            FileReader::getInstance()->getNtofLib().getEVEH()->getEventNumber();
    }

    // save the actual configuration
    saveConfiguration();
    std::vector<std::string> filenames =
        FileReader::getInstance()->getNtofLib().getFilenames();

    FileReader::getInstance()->getNtofLib().closeAllFiles();
    FileReader::getInstance()->openFile(filenames, true);
    updateRangeTitle();

    // Set the good validated event
    if (validatedNumber != -1)
    {
        FileReader::getInstance()->getNtofLib().getValidatedEvent(
            validatedNumber);
    }

    // apply the old configuration
    applyConfiguration();

    plot(replot);
}

/*
 * \brief Update the graph when the user selected an element
 */
void MainWindow::selectionChanged()
{
    addActionOnGoing();
    setDebugMessage("Item selected/deselected");

    // if an axis is selected, only allow the direction of that axis to be
    // zoomed if no axis is selected, both directions may be zoomed
    if (customPlot_->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
    {
        customPlot_->axisRect()->setRangeZoom(customPlot_->xAxis->orientation());
    }
    else if (customPlot_->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
    {
        customPlot_->axisRect()->setRangeZoom(customPlot_->yAxis->orientation());
    }
    else
    {
        customPlot_->axisRect()->setRangeZoom(Qt::Horizontal | Qt::Vertical);
    }

    // synchronize selection of graphs with selection of corresponding legend
    // items:
    for (int i = 0; i < customPlot_->graphCount(); ++i)
    {
        QCPGraph *graph = customPlot_->graph(i);
        QCPPlottableLegendItem *item = customPlot_->legend->itemWithPlottable(
            graph);
        if (graph != NULL && item != NULL &&
            (item->selected() || graph->selected()))
        {
            item->setSelected(true);
            graph->setSelection(
                QCPDataSelection(QCPDataRange(0, graph->dataCount())));
            graph->setLayer("front");
        }
        else
        {
            graph->setLayer("back");
            graph->setSelection(QCPDataSelection());
        }
    }

    removeActionOnGoing();
}

void MainWindow::onZoomChanged()
{
    getMenuBar()->getUndoAction()->setEnabled(customPlot_->hasPreviousZoom());
    getMenuBar()->getRedoAction()->setEnabled(customPlot_->hasNextZoom());
}

/* ************************************************************* */
/* GETTERS AND SETTERS                                           */
/* ************************************************************* */
/*
 * \brief Get the custom plot object
 * \return Return a pointer on the object
 */
CustomPlotZoom *MainWindow::getCustomPlot()
{
    return customPlot_;
}

/*
 * \brief Add a detector to the detector list
 * \param groupBox: element to add to the layout
 */
void MainWindow::addDetectorElement(QWidget *groupBox)
{
    const QMargins &margins = detectorsList_->layout()->contentsMargins();
    detectorsList_->layout()->addWidget(groupBox);
    groupBox->adjustSize(); /* compute size */
    int width = groupBox->width() + margins.left() + margins.right() +
        scrollArea_->verticalScrollBar()->sizeHint().width();
    if (width > scrollArea_->width())
        scrollArea_->setFixedWidth(width);
}

/*
 * \brief Remove a detector to the detector list
 * \param groupBox: element to remove from the layout
 */
void MainWindow::removeDetectorElement(QWidget *groupBox)
{
    detectorsList_->layout()->removeWidget(groupBox);
}

/*
 * \brief Get the option object
 * \return Return a pointer on the object
 */
OptionPanel *MainWindow::getOptionPanel()
{
    return optionsPanel_;
}

/*
 * \brief Get the parameters table object
 * \return Return a pointer on the object
 */
TableParams *MainWindow::getTableParams()
{
    return paramsTable_;
}

/*
 * \brief Get the parameters group box object
 * \return Return a pointer on the object
 */
QGroupBox *MainWindow::getParamsGroupBox()
{
    return paramsGroupBox_;
}

/*
 * \brief Set the energy value in the right panel
 * \param energyRef: value to set
 */
void MainWindow::setEnergyRef(double energyRef)
{
    tGammaLineEdits_->setText(QString::number(energyRef) + tr(" us"));
}

/*
 * \brief Empty the values relative to the energy on the right panel
 */
void MainWindow::resetEnergyRef()
{
    tGammaLineEdits_->setText(tr(""));
    energyLineEdits_->setText(tr(""));
    tofLineEdits_->setText(tr(""));
}

/*
 * \brief Set the values relative to the position of the mouse pointer in the
 * right panel \param x: position of the mouse pointer on the x scale \param y:
 * position of the mouse pointer on the y scale
 */
void MainWindow::setCoords(double x, double y)
{
    QString xFormatted = QString("%L1").arg(x, 0, 'g', 10);
    coordXLineEdits_->setText(xFormatted + " ns");

    QString yFormatted = QString::number(y);
    if (Config::getInstance()->getDataUnit() == "mv")
    {
        yFormatted = yFormatted + " mV";
    }
    else if (Config::getInstance()->getDataUnit() == "adc")
    {
        yFormatted = yFormatted + " (ADC)";
    }
    coordYLineEdits_->setText(yFormatted);
}
/*
 * \brief Calculate the energy according the position of the mouse pointer
 * pointer on the graph \param t: time relative to the position of the mouse
 * pointer
 */
void MainWindow::energyCalculation(int32_t t)
{
    if (FileReader::getInstance()->getNtofLib().getFilename() != "")
    {
        if (t > Config::getInstance()->getTGamma())
        {
            // Define the experimental area
            NTOF_FILE file = searchFile_->toNtofFile(
                FileReader::getInstance()->getNtofLib().getFilename());
            double distance = 0;
            if (file.runNumber >= 200000)
            {
                distance = Config::getInstance()->getDistanceEAR2();
            }
            else
            {
                distance = Config::getInstance()->getDistanceEAR1();
            }

            double diffTime = t -
                (double) Config::getInstance()->getTGamma(); // t - t0
            double tof = diffTime + (distance / c);
            double beta = distance / (c * tof);
            double gamma = 1.0 / sqrt(1.0 - pow(beta, 2));
            double energy = m0 * (gamma - 1.0);

            // Set energy unit intelligently
            QString energyStr = QString::number(energy, 'e', 3) + tr(" eV");

            energyLineEdits_->setText(energyStr);
            tofLineEdits_->setText(QString::number(tof) + " ns");
        }
        else
        {
            energyLineEdits_->setText(tr(""));
            tofLineEdits_->setText(tr(""));
        }
    }
}

/*
 * \brief Get the range selector panel object
 * \return Return a pointer on the object
 */
RangeSelectorPanel *MainWindow::getRangeSelectorPanel()
{
    return rangeSelectorPanel_;
}

/*
 * \brief Get the summary panel object
 * \return Return a pointer on the object
 */
SummaryPanel *MainWindow::getSummaryPanel()
{
    return summaryPanel_;
}
} // namespace eventDisplay
} // namespace ntof
