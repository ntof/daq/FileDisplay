#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QCheckBox>
#include <QMainWindow>
#include <QWidget>

#include <NtofLib.h>

#include "CustomPlotZoom.h"
#include "MenuBar.h"
#include "OptionPanel.h"
#include "RangeSelectorPanel.h"
#include "SearchFile.h"
#include "SummaryPanel.h"

class QAction;
class QDialogButtonBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QTextEdit;
class QVBoxLayout;
class QFormLayout;
class QScrollArea;
class FlowLayout;

namespace ntof {
namespace eventDisplay {
class FileReader;
class TableParams;
// class CustomPlotZoom;
class SearchFile;

typedef struct
{
    std::string name;
    int32_t id;
    bool isActive;
    bool showParams;
    bool showThreshold;
    bool dataArea;
} DetectorSave;

// Value used to calculate the energy
// const double magicNumber = 72.2977;           //!< Magic number
const double c = 0.2997;     //!< Light speed
const double m0 = 939.565E6; //!< Mass

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
     *  \brief Get the instance of the MainWindow object and initialize the
     * instance if the pointer is null \return A pointer on the MainWindow
     * object
     */
    static MainWindow *getInstance();

    /*
     * \brief Destructor of the class
     */
    ~MainWindow();

    /* **************************************************** */
    /* GETTERS                                              */
    /* **************************************************** */
    /*
     * \brief Get the custom plot object
     * \return Return a pointer on the object
     */
    CustomPlotZoom *getCustomPlot();

    /*
     * \brief Get the option object
     * \return Return a pointer on the object
     */
    OptionPanel *getOptionPanel();

    /*
     * \brief Get the parameters group box object
     * \return Return a pointer on the object
     */
    QGroupBox *getParamsGroupBox();

    /*
     * \brief Get the parameters table object
     * \return Return a pointer on the object
     */
    TableParams *getTableParams();

    /*
     * \brief Get the menu bar object
     * \return Return a pointer on the object
     */
    MenuBar *getMenuBar();

    /*
     * \brief Get the range selector panel object
     * \return Return a pointer on the object
     */
    RangeSelectorPanel *getRangeSelectorPanel();

    /*
     * \brief Get the summary panel object
     * \return Return a pointer on the object
     */
    SummaryPanel *getSummaryPanel();

    /*
     * \brief Get the search file object
     * \return Return a pointer on the object
     */
    SearchFile *getSearchFile();

    /* **************************************************** */
    /* GRAPHICAL CONTENT                                    */
    /* **************************************************** */
    /*
     * \brief Set the content of the main info group box
     */
    void initMainInfo();

    /*
     * \brief Add a detector to the detector list
     * \param groupBox: element to add to the layout
     */
    void addDetectorElement(QWidget *groupBox);

    /*
     * \brief Remove a detector to the detector list
     * \param groupBox: element to remove from the layout
     */
    void removeDetectorElement(QWidget *groupBox);

    /*
     * \brief Enable or disable the buttons if necessary
     */
    void enableButton();

    /*
     * \brief Set the energy value in the right panel
     * \param energyRef: value to set
     */
    void setEnergyRef(double energyRef);

    /* **************************************************** */
    /* GRAPHICAL INTERACTIONS                               */
    /* **************************************************** */
    /*
     * \brief Calculate the energy according the position of the mouse pointer
     * pointer on the graph \param t: time relative to the position of the mouse
     * pointer
     */
    void energyCalculation(int32_t t);

    /*
     * \brief Set the values relative to the position of the mouse pointer in
     * the right panel \param x: position of the mouse pointer on the x scale
     * \param y: position of the mouse pointer on the y scale
     */
    void setCoords(double x, double y);

    /*
     * \brief Empty the values relative to the energy on the right panel
     */
    void resetEnergyRef();

    /*
     * \brief Initialize the reader
     */
    void addToFileReader();

    /*
     * \brief Update the element and its children
     */
    void updateElement();

    /*
     * \brief Jump directly to the last event
     */
    void lastEvent();

    /*
     * \brief Set the segment number
     */
    void setSegmentNumber();

    /*
     * \brief Set the trigger number being display value
     */
    void setTriggerDisplay();

    /*
     * \brief Set the tGamma value
     */
    void setTGamma();

    /*
     * \brief Set the distance value
     */
    void setDistance();

    /*
     * \brief Set the BCT value
     */
    void setBCT();

    /*
     * \brief Reload all the configuration after a data unit change
     * \param replot: The plot area is not redraw if this parameter is equal to
     * false
     */
    void reloadFiles(bool replot = true);

    /*
     * \brief Jump to an event
     * \param eventID: ID of the event to set
     */
    void setEventID(int32_t eventID);

    /*
     * \brief Enable or disable the plot button if necessary
     */
    void enablePlot();

    /*
     * \brief Create the content of the main window
     */
    void create();

    /*
     * \brief update the range titles of the custom plot
     */
    void updateRangeTitle();

    /*
     * \brief Create a group box
     * \param groupBox: The group box that will be created
     * \param text: Title of the object
     * \param name: Name of the object
     * \return Return the pointer on the group box
     */
    QGroupBox *initGroupBox(QGroupBox *&groupBox,
                            std::string text,
                            std::string name);

    /*
     * \brief Save the actual configuration to be able to reset it when the new
     * event will be loaded
     */
    void saveConfiguration();

    /*
     * \brief Apply the configuration saved
     */
    void applyConfiguration();

    /*
     * \brief Apply the range saved
     */
    void applyRangeSaved();

    /*
     * \brief Get the run number
     * \return Return the run number
     */
    std::string getRunNumber();

    /*
     * \brief Get the trigger number
     * \return Return the trigger number
     */
    std::string getTriggerNumber();

    /*
     * \brief Allow to know if the graph need to be replotted
     * \return Return true if the system has to be replotted
     */
    bool isToReplot() const;

    /*
     * \brief Set the value of the variable toReplot
     * \param toReplot: new value to set
     */
    void setToReplot(bool toReplot);

    /*
     * \brief Reset the range
     */
    void rangeSavedReset();

    /*
     * \brief Load an event
     * \param eventNumber: ID of the validated event
     * \return Return true if the event has been reached, to update the plot
     */
    bool loadEvent(int32_t eventNumber);

    /*
     * \brief Change the content of the label in the debug area
     * \param message: Content to set
     */
    void setDebugMessage(std::string message);

    /*
     * \brief shot critical QMessageBox
     * \param title of the box
     * \param message
     */
    void showCriticalMessage(const std::string &title,
                             const std::string &message);

    /* ************************************************************* */
    /* ACTIONS COUNTER                                               */
    /* ************************************************************* */
    /*
     * \brief Add an element to the count of the actions on going
     */
    void addActionOnGoing();

    /*
     * \brief Remove an element to the count of the actions on going
     */
    void removeActionOnGoing();

public slots:
    /*
     * \brief Go to the next event
     */
    void nextEvent();

private slots:

    /*
     * \brief Go to the previous event
     */
    void previousEvent();

    /*
     * \brief Plot the data relative to the detector selected
     * \param replot: The plot area is not redraw if this parameter is equal to
     * false
     */
    void plot(bool replot = true);

    /*
     * \brief Select all the detector in the list
     */
    void selectAll();

    /*
     * \brief Unselect all the detector in the list
     */
    void unselectAll();

    /*
     * \brief Update the graph when the user selected an element
     */
    void selectionChanged();

    /**
     * @brief zoom changed.
     */
    void onZoomChanged();

private:
    /*
     * \brief Constructor of the class
     */
    MainWindow(QWidget *parent = 0);

    /*
     * \brief Create the main widget
     */
    void createWidget();

    /*
     * \brief Create the main info group box
     * \return Return the object created
     */
    QGroupBox *createMainInfoBox();

    /*
     * \brief Create the position group box
     * \return Return the object created
     */
    QGroupBox *createPositionBox();

    /*
     * \brief Create the detector group box
     * \return Return the object created
     */
    QGroupBox *createDetectorBox();

    /*
     * \brief Create the chart group box
     * \return Return the object created
     */
    QGroupBox *createChartBox();

    /*
     * \brief Create the parameters group box
     * \return Return the object created
     */
    QGroupBox *createParamsBox();

    /*
     * \brief Create the debug toolbar group box
     * \return Return the object initialized
     */
    QGroupBox *createDebugToolBarBox();

    /*
     * \brief Create a label
     * \param text: content of the label
     */
    //                QLabel *createLabel(const QString &text);

    /*
     * \brief Create a line edit
     * \param lineEdit: The line edit that will be created
     * \param text: Title of the object
     * \param layout: The layout to anchor the element after creation
     */
    void initLabel(QLabel *&lineEdit, std::string text, QFormLayout *&layout);

    /*
     * \brief Set the state of the button previous event (enable if previous
     * event has been found, disable else)
     */
    void enablePreviousEvent();

    /*
     * \brief Set the state of the button next event (enable if next event has
     * been found, disable else)
     */
    void enableNextEvent();

    /*
     * \brief Create the menu of the windows
     */
    void createMenu();

    /*
     * \brief get Active channels
     */
    std::vector<int32_t> getActiveChannels() const;

    /*
     * \brief set Active channels
     */
    void setActiveChannels(const std::vector<int32_t> &channels);

    static MainWindow *instance_; //!< MainWindow instance
    MenuBar *menu_;               //!< Menu bar
    SearchFile *searchFile_;      //!< Search engine

    /* **************************************************** */
    /* MAIN INFO GROUP BOX ELEMENTS                         */
    /* **************************************************** */
    QLabel *runNumberLineEdits_;     //!< Line edit for the run number
    QLabel *experimentLineEdits_;    //!< Line edit for the experimental area
    QLabel *segmentNumberLineEdits_; //!< Line edit for the segment number of
                                     //!< the trigger displayed
    QLabel *eventsInRunLineEdits_;   //!< Line edit for the number of events in
                                     //!< the run
    QLabel *eventNumberLineEdits_;   //!< Line edit for the event numbers
    QLabel *BCTLineEdits_;           //!< Line edit for the BCT intensity
    QLabel *tGammaLineEdits_;        //!< Line edit for the value of the energy
                                     //!< reference
    QLabel *distanceLineEdits_;      //!< Line edit for the distance

    /* **************************************************** */
    /* POSITION GROUP BOX ELEMENTS                          */
    /* **************************************************** */
    QLabel *coordXLineEdits_; //!< Line edit for the x coordinate
    QLabel *coordYLineEdits_; //!< Line edit for the y coordinate
    QLabel *energyLineEdits_; //!< Line edit for the energy calculated value
    QLabel *tofLineEdits_;    //!< Line edit for the time of flight

    /* **************************************************** */
    /* DETECTORS GROUP BOX ELEMENTS                         */
    /* **************************************************** */
    QWidget *detectorsList_;   //!< Detectors container
    QPushButton *plotButton_;  //!< Button to plot the data
    QPushButton *selectAll_;   //!< Button to select all the detectors
    QPushButton *unselectAll_; //!< Button to unselect all the detectors
    QScrollArea *scrollArea_;  //!< Scroll area used to the detectors list

    /* **************************************************** */
    /* CHART GROUP BOX ELEMENTS                             */
    /* **************************************************** */
    QPushButton *previousEventButton_; //!< Button to reach the previous event
    QPushButton *nextEventButton_;     //!< Button to reach the next event

    /* **************************************************** */
    /* PARAMETER TABLE GROUP BOX ELEMENTS                   */
    /* **************************************************** */
    QGroupBox *paramsGroupBox_; //!< Group box use to display the parameters
                                //!< table
    TableParams *paramsTable_;  //!< Parameters table

    /* **************************************************** */
    /* DEBUG TOOLBAR ELEMENTS                               */
    /* **************************************************** */
    QGroupBox *debugToolbarGroupBox_; //!< Group box use to display the
                                      //!< parameters table
    QLabel *debugToolbarLabel_;       //!< Parameters table

    /* **************************************************** */
    /* OTHERS ELEMENTS                                      */
    /* **************************************************** */
    CustomPlotZoom *customPlot_; //!< Library used to create the plot
    OptionPanel *optionsPanel_;  //!< Setting panel
    RangeSelectorPanel *rangeSelectorPanel_; //!< Range selector panel
    SummaryPanel *summaryPanel_;             //!< Summary panel
    std::list<DetectorSave> channelsSave_;   //!< Configurations saved
    Zoom rangeSave_;         //!< Save of the actual range selection
    bool toReplot_;          //!< Allow to know if a replot is needed
    int32_t actionsOnGoing_; //!< Count of the actions on going
};
} // namespace eventDisplay
} // namespace ntof
#endif // MAINWINDOW_H
