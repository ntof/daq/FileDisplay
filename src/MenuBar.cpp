#include "MenuBar.h"

#include <iostream>
#include <sstream>

#include <QApplication>
#include <QDesktopServices>
#include <QFileDialog>
#include <QFileInfo>
#include <QIcon>
#include <QMenuBar>
#include <QMessageBox>
#include <QSize>
#include <QUrl>

#include <unistd.h>

#include "Config.h"
#include "CustomPlotZoom.h"
#include "FileReader.h"
#include "MainWindow.h"
#include "SearchFile.h"
#include "WriterFile.h"

namespace ntof {
namespace eventDisplay {
std::string MenuBar::versionFileDisplay_ = "UNDEFINED";
std::string MenuBar::versionQCustomPlot_ = "UNDEFINED";
std::string MenuBar::versionQt_ = "UNDEFINED";
std::string MenuBar::versionBoost_ = "UNDEFINED";
std::string MenuBar::versionCMake_ = "UNDEFINED";

/* ************************************************************* */
/* CONSTRUCTOR & DESTRUCTOR                                      */
/* ************************************************************* */
/*
 * \brief Constructor of the class
 * \param parent: Parent node in the tree
 */
MenuBar::MenuBar() :
    xrootFileDialog_(NULL),
    menuBar_(NULL),
    fileMenu_(NULL),
    debugMenu_(NULL),
    toolBar_(NULL),
    openAction_(NULL),
    zoomModeAction_(NULL),
    dragNDropModeAction_(NULL),
    undoZoomAction_(NULL),
    redoZoomAction_(NULL),
    quitAction_(NULL),
    textFileAction_(NULL),
    paramsAction_(NULL)
{
    createActions();
    createMenu();
    createToolbar();
    updateElement();
}

/*
 * \brief Destructor of the class
 */
MenuBar::~MenuBar()
{
    // The life time of this object if the lifetime of the application but ...
    delete xrootFileDialog_;
    delete menuBar_;
    delete fileMenu_;
    delete optionsMenu_;
    delete debugMenu_;
    delete toolBar_;
    delete openAction_;
    delete openCastorAction_;
    delete openLastRunAction_;
    delete zoomModeAction_;
    delete dragNDropModeAction_;
    delete scatterPlotAction_;
    delete polylinePlotAction_;
    delete undoZoomAction_;
    delete redoZoomAction_;
    delete resetZoomAction_;
    delete zoomSettingsAction_;
    delete quitAction_;
    delete textFileAction_;
    delete debugFileAction_;
    delete paramsAction_;
    delete aboutAction_;
    delete pngExportAction_;
    delete userDocAction_;
}

/* ************************************************************* */
/* GETTERS                                                       */
/* ************************************************************* */
/*
 * \brief Get the menu bar object
 * \return Return a pointer of the menu bar object
 */
QMenuBar *MenuBar::getMenuBar()
{
    return menuBar_;
}

/*
 * \brief Get the tool bar object
 * \return Return a pointer of the tool bar object
 */
QToolBar *MenuBar::getToolBar()
{
    return toolBar_;
}

/*
 * \brief Get the zoom action
 * \return Return a pointer of the zoom action
 */
QAction *MenuBar::getZoomModeAction()
{
    return zoomModeAction_;
}

/*
 * \brief Get the drag'n'drop action
 * \return Return a pointer of the drag'n'drop action
 */
QAction *MenuBar::getDragNDropModeAction()
{
    return dragNDropModeAction_;
}

/*
 * \brief Get the undo action
 * \return Return a pointer of the undo action
 */
QAction *MenuBar::getUndoAction()
{
    return undoZoomAction_;
}

/*
 * \brief Get the redo action
 * \return Return a pointer of the redo action
 */
QAction *MenuBar::getRedoAction()
{
    return redoZoomAction_;
}

/*
 * \brief Get the action which write the debug file
 * \return Return a pointer of the action which write the debug file
 */
QAction *MenuBar::getDebugFileAction()
{
    return debugFileAction_;
}

/*
 * \brief Get the action which write the info file
 * \return Return a pointer of the action which write the info file
 */
QAction *MenuBar::getTextFileAction()
{
    return textFileAction_;
}
/* ************************************************************* */
/* CREATE ELEMENTS                                               */
/* ************************************************************* */
/*
 * \brief Initialize the actions
 */
void MenuBar::createActions()
{
    openAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/places/folder-documents.png")),
        tr("&Open"), this);
    openAction_->setShortcuts(QKeySequence::Open);
    openAction_->setToolTip(tr("Open an existing file [CTRL+O]"));
    connect(openAction_, SIGNAL(triggered()), this, SLOT(openFile()));

    openCastorAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/document-open-remote.png")),
        tr("Open on Castor"), this);
    openCastorAction_->setToolTip(tr("Open an file from CASTOR"));
    connect(openCastorAction_, SIGNAL(triggered()), this,
            SLOT(openCastorFile()));

    zoomModeAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/zoom-select.png")),
        tr("Zoom mode"), this);
    zoomModeAction_->setToolTip(tr("Zoom mode"));
    zoomModeAction_->setCheckable(true);
    zoomModeAction_->setChecked(true);
    connect(zoomModeAction_, SIGNAL(triggered(bool)), this,
            SLOT(zoomAction(bool)));

    dragNDropModeAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/transform-move.png")),
        tr("Selection mode"), this);
    dragNDropModeAction_->setToolTip(tr("Selection mode [CTRL]"));
    dragNDropModeAction_->setCheckable(true);
    connect(dragNDropModeAction_, SIGNAL(triggered(bool)), this,
            SLOT(dragNDropAction(bool)));

    scatterPlotAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/office-chart-scatter.png")),
        tr("Scatter plot"), this);
    scatterPlotAction_->setToolTip(tr("Show the points"));
    scatterPlotAction_->setCheckable(true);
    connect(scatterPlotAction_, SIGNAL(triggered(bool)), this,
            SLOT(scatterPlot(bool)));

    polylinePlotAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/office-chart-line.png")),
        tr("Polyline"), this);
    polylinePlotAction_->setToolTip(tr("Show the lines"));
    polylinePlotAction_->setCheckable(true);
    polylinePlotAction_->setChecked(true);
    connect(polylinePlotAction_, SIGNAL(triggered(bool)), this,
            SLOT(linePlot(bool)));

    undoZoomAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/zoom-previous.png")),
        tr("Undo &zoom"), this);
    undoZoomAction_->setShortcuts(QKeySequence::Undo);
    undoZoomAction_->setToolTip(tr("Undo the last zoom you made [CTRL+Z]"));
    undoZoomAction_->setEnabled(false);
    connect(undoZoomAction_, SIGNAL(triggered()), this, SLOT(undoAction()));

    redoZoomAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/zoom-next.png")),
        tr("Redo zoom"), this);
    redoZoomAction_->setShortcuts(QKeySequence::Redo);
    redoZoomAction_->setToolTip(tr("Redo the next zoom [CTRL+SHIFT+Z]"));
    redoZoomAction_->setEnabled(false);
    connect(redoZoomAction_, SIGNAL(triggered()), this, SLOT(redoAction()));

    resetZoomAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/custom/zoom-off.png")), tr("Reset zoom"),
        this);
    resetZoomAction_->setToolTip(tr("Reset the zoom to the initial one"));
    connect(resetZoomAction_, SIGNAL(triggered()), this, SLOT(resetZoom()));

    zoomSettingsAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/document-edit-verify.png")),
        tr("Range selection"), this);
    zoomSettingsAction_->setToolTip(tr("Define the range selection manually"));
    connect(zoomSettingsAction_, SIGNAL(triggered()), this,
            SLOT(zoomSettings()));

    quitAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/window-close.png")), tr("&Quit"),
        this);
    quitAction_->setShortcuts(QKeySequence::Quit);
    quitAction_->setToolTip(tr("Close the application [CTRL+Q]"));
    connect(quitAction_, SIGNAL(triggered()), this, SLOT(quitAction()));

    textFileAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/document-edit.png")),
        tr("Generate text file"), this);
    textFileAction_->setToolTip(
        tr("Generate a text file readable by any text editor"));
    connect(textFileAction_, SIGNAL(triggered()), this, SLOT(textFileAction()));

    debugFileAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/story-editor.png")),
        tr("Generate debug file"), this);
    debugFileAction_->setToolTip(tr("Generate a debug file"));
    debugFileAction_->setEnabled(false);
    connect(debugFileAction_, SIGNAL(triggered()), this,
            SLOT(debugFileAction()));

    paramsAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/categories/preferences-system.png")),
        tr("Parameters"), this);
    paramsAction_->setToolTip(tr("Open the option panel"));
    connect(paramsAction_, SIGNAL(triggered()), this, SLOT(paramsAction()));

    aboutAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/status/dialog-information.png")),
        tr("About"), this);
    aboutAction_->setToolTip(tr("About the software..."));
    connect(aboutAction_, SIGNAL(triggered()), this, SLOT(about()));

    pngExportAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/document-save-as.png")),
        tr("Export as PNG"), this);
    pngExportAction_->setToolTip(tr("Export the graph as a PNG file"));
    connect(pngExportAction_, SIGNAL(triggered()), this, SLOT(pngExport()));

    userDocAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/bookmarks-organize.png")),
        tr("User documentation"), this);
    userDocAction_->setToolTip(tr("Open the user documentation file"));
    connect(userDocAction_, SIGNAL(triggered()), this, SLOT(openDocUserFile()));

    summaryAction_ = new QAction(
        QIcon(tr(":/icons/oxygen/48x48/actions/view-calendar-list.png")),
        tr("Summary panel"), this);
    summaryAction_->setToolTip(tr("Open the summary panel"));
    connect(summaryAction_, SIGNAL(triggered()), this, SLOT(summary()));
}

/*
 * \brief Initialize the menu
 */
void MenuBar::createMenu()
{
    menuBar_ = new QMenuBar();

    // Menu 'File'
    fileMenu_ = menuBar_->addMenu(tr("File"));
    fileMenu_->addAction(openAction_);
    fileMenu_->addAction(openCastorAction_);
    fileMenu_->addAction(quitAction_);

    // Menu 'Options'
    optionsMenu_ = menuBar_->addMenu(tr("Options"));
    optionsMenu_->addAction(paramsAction_);
    optionsMenu_->addAction(pngExportAction_);
    optionsMenu_->addAction(textFileAction_);
    optionsMenu_->addSeparator();
    optionsMenu_->addAction(undoZoomAction_);
    optionsMenu_->addAction(redoZoomAction_);
    optionsMenu_->addAction(resetZoomAction_);
    optionsMenu_->addAction(zoomSettingsAction_);

    // Menu 'Debug'
    debugMenu_ = menuBar_->addMenu(tr("?"));
    debugMenu_->addAction(paramsAction_);
    debugMenu_->addAction(summaryAction_);
    debugMenu_->addAction(userDocAction_);
    debugMenu_->addAction(aboutAction_);
}

/*
 * \brief Create the toolbar
 */
void MenuBar::createToolbar()
{
    toolBar_ = new QToolBar(tr("toolbar"));
    toolBar_->setIconSize(QSize(35, 35));
    toolBar_->addAction(openAction_);
    toolBar_->addAction(openCastorAction_);
    toolBar_->addSeparator();
    toolBar_->addAction(zoomModeAction_);
    toolBar_->addAction(dragNDropModeAction_);
    toolBar_->addSeparator();
    toolBar_->addAction(scatterPlotAction_);
    toolBar_->addAction(polylinePlotAction_);
    toolBar_->addSeparator();
    toolBar_->addAction(undoZoomAction_);
    toolBar_->addAction(redoZoomAction_);
    toolBar_->addAction(resetZoomAction_);
    toolBar_->addAction(zoomSettingsAction_);
    toolBar_->addSeparator();
    toolBar_->addAction(pngExportAction_);
    toolBar_->addAction(textFileAction_);
    toolBar_->addAction(debugFileAction_);
    toolBar_->addSeparator();
    toolBar_->addAction(paramsAction_);
    toolBar_->addAction(summaryAction_);
    toolBar_->addAction(userDocAction_);
    toolBar_->addAction(aboutAction_);
    toolBar_->addAction(quitAction_);
}

/*
 * \brief Update this element and its children
 */
void MenuBar::updateElement()
{
    if (FileReader::getInstance()->getNtofLib().getFilenames().size() != 0)
    {
        if (Config::getInstance()->getModeDebug() == "on")
        {
            debugFileAction_->setVisible(true);
        }
        else
        {
            debugFileAction_->setVisible(false);
        }
    }
}

/* ************************************************************* */
/* SLOTS                                                         */
/* ************************************************************* */
/*
 * \brief Open a file on a local storage
 */
void MenuBar::openFile()
{
    MainWindow::getInstance()->addActionOnGoing();

    // Open the default workspace
    std::string path = Config::getInstance()->getWorkspace();

    // If there is not default workspace or if it is inaccessible, the home path
    // is used
    if (chdir(path.c_str()) != 0)
    {
        path = QDir::homePath().toStdString();
    }
    // Open the file dialog
    QString defFilter = tr("Raw files (*.raw*)");
    QStringList fileName = QFileDialog::getOpenFileNames(
        this, tr("Open File"), tr(path.c_str()),
        tr("All files (*.*);;Raw files (*.raw*);;"
           "Index files (*.idx.finished)"),
        &defFilter);
    if (!fileName.isEmpty())
    {
        MainWindow::getInstance()->setDebugMessage("Open file(s)");

        // Convert the list to a vector
        std::vector<std::string> filenames;
        for (QStringList::iterator filenameIt = fileName.begin();
             filenameIt != fileName.end(); ++filenameIt)
        {
            filenames.push_back((*filenameIt).toStdString());
        }

        // Open the file and save the workspace if needed
        FileReader::getInstance()->openFile(filenames);
        FileReader::getInstance()->resetZoom();
    }

    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Change the state of the zoom mode
 * \param state: New state to set
 */
void MenuBar::zoomAction(bool state)
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage(
        "Activate or deactivate the zoom mode");

    // Disable Drag'n'drop mode
    getZoomModeAction()->setChecked(state);
    getDragNDropModeAction()->setChecked(!state);
    MainWindow::getInstance()->getCustomPlot()->setZoomMode(state);

    if (state)
    {
        getZoomModeAction()->setToolTip(tr("Zoom mode"));
        getDragNDropModeAction()->setToolTip(tr("Selection mode "
                                                "[CTRL]"));
    }
    else
    {
        getZoomModeAction()->setToolTip(tr("Zoom mode [CTRL]"));
        getDragNDropModeAction()->setToolTip(tr("Selection mode"));
    }

    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Change the state of the drag'n'drop mode
 * \param state: New state to set
 */
void MenuBar::dragNDropAction(bool state)
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage(
        "Activate or deactivate the selection mode");

    // Disable Zoom mode
    getDragNDropModeAction()->setChecked(state);
    getZoomModeAction()->setChecked(!state);
    MainWindow::getInstance()->getCustomPlot()->setZoomMode(!state);

    if (state)
    {
        getZoomModeAction()->setToolTip(tr("Zoom mode [CTRL]"));
        getDragNDropModeAction()->setToolTip(tr("Selection mode"));
    }
    else
    {
        getZoomModeAction()->setToolTip(tr("Zoom mode"));
        getDragNDropModeAction()->setToolTip(tr("Selection mode "
                                                "[CTRL]"));
    }

    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Undo the last zoom action
 */
void MenuBar::undoAction()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage(
        "Set the previous zoom selection");
    MainWindow::getInstance()->getCustomPlot()->previousZoom();
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Redo the last zoom action
 */
void MenuBar::redoAction()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Set the next zoom selection");
    MainWindow::getInstance()->getCustomPlot()->nextZoom();
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Quit the application
 */
void MenuBar::quitAction()
{
    MainWindow::getInstance()->setDebugMessage("Please don't kill me !");
    QApplication::quit();
}

/*
 * \brief Create a text file from the ones opened
 */
void MenuBar::textFileAction()
{
    MainWindow::getInstance()->addActionOnGoing();
    QString filename = QFileDialog::getSaveFileName();
    if (filename != NULL)
    {
        WriterFile(filename.toStdString(), DATA);
    }
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Create a debug file from the ones opened
 */
void MenuBar::debugFileAction()
{
    MainWindow::getInstance()->addActionOnGoing();
    QString filename = QFileDialog::getSaveFileName();
    if (filename != NULL)
    {
        WriterFile(filename.toStdString(), DEBUG);
    }
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Open the option panel
 */
void MenuBar::paramsAction()
{
    MainWindow::getInstance()->getOptionPanel()->open();
}

/*
 * \brief Open the info panel
 */
void MenuBar::about()
{
    // Initialize the content of the info message box
    std::ostringstream message("");
    message << "This software must be used to read a raw data files from the "
               "n_TOF experiment."
            << std::endl;
    message << std::endl;
    message << "Software version: " << versionFileDisplay_ << std::endl;
    message << "Ntoflib version: "
            << FileReader::getInstance()->getNtofLib().getVersion()
            << std::endl;
    message << "Qt version: " << versionQt_ << std::endl;
    message << "Boost version: " << versionBoost_ << std::endl;
    message << "CMake version: " << versionCMake_ << std::endl;
    message << "QCustomPlot version: " << versionQCustomPlot_ << std::endl;

    QMessageBox::information(this, QObject::tr("About"),
                             QObject::tr(message.str().c_str()),
                             QMessageBox::Ok);
}

/*
 * \brief Export the contain of the graph as a png
 */
void MenuBar::pngExport()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Export as PNG file");
    // Get the destination file name
    QString filename = QFileDialog::getSaveFileName();
    if (filename != NULL)
    {
        CustomPlotZoom *customPlot = MainWindow::getInstance()->getCustomPlot();
        QFileInfo fi(filename);

        // Create the title of the graphic
        std::ostringstream title("");
        title << "Run " << MainWindow::getInstance()->getRunNumber()
              << " - Trigger " << MainWindow::getInstance()->getTriggerNumber();

        // Add temporarily a title to the graph and remove it after the export
        customPlot->setTitle(title.str());
        customPlot->showTitle();
        customPlot->savePng(fi.path() + tr("/") + fi.baseName() + tr(".png"));
        customPlot->hideTitle();
    }
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Open a file from CASTOR
 */
void MenuBar::openCastorFile()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Open a file on CASTOR");

    // Init the Castor file dialog if it has never been called
    if (xrootFileDialog_ == NULL)
    {
        xrootFileDialog_ = new XRootFileDialog();
    }

    xrootFileDialog_->setVisible(true);
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Open automatically the last run
 */
//        void MenuBar::openLastRunFile() {
// quickOpenLastFile_->setVisible(true);
//        }
/*
 * \brief Open a defined run
 */
void MenuBar::openParticularRunFile()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Open a particular file");
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Open the user documentation
 */
void MenuBar::openDocUserFile()
{
    // Convert the filename to a QFileInfo to have a quick access to the details
    // of the filename (path/extension/...)
    QString userDoc = tr(Config::getInstance()->getUserDoc().c_str());
    QFileInfo checkFile(userDoc);
    if (checkFile.exists() && checkFile.isFile())
    {
        QDesktopServices::openUrl(QUrl(userDoc));
    }
    else
    {
        QMessageBox::critical(
            this, QObject::tr("Error"),
            QObject::tr("The user documentation file is unreachable"),
            QMessageBox::Ok);
    }
}

/*
 * \brief Reset the zoom
 */
void MenuBar::resetZoom()
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage("Reset zoom");
    MainWindow::getInstance()->getCustomPlot()->resetZoom();
    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Change the state of the scatter plot mode
 * \param state: change the state of the scatter plot mode
 */
void MenuBar::scatterPlot(bool state)
{
    // Can be disabled only if polyline is enable
    if (state == Qt::Unchecked && !polylinePlotAction_->isChecked())
    {
        scatterPlotAction_->setChecked(true);
        return;
    }

    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage(
        "Activate or deactivate the scatter plot");

    FileReader::getInstance()->setPlotPoints(state);
    FileReader::getInstance()->updateChannelsAppearance();
    MainWindow::getInstance()->getCustomPlot()->replot();

    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Change the state of the line plot mode
 * \param state: change the state of the line plot mode
 */
void MenuBar::linePlot(bool state)
{
    // Can be disabled only if polyline is enable
    if (state == Qt::Unchecked && !scatterPlotAction_->isChecked())
    {
        polylinePlotAction_->setChecked(true);
        return;
    }

    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage(
        "Activate or deactivate the scatter plot");

    FileReader::getInstance()->setPlotLine(state);
    FileReader::getInstance()->updateChannelsAppearance();
    MainWindow::getInstance()->getCustomPlot()->replot();

    MainWindow::getInstance()->removeActionOnGoing();
}

/*
 * \brief Open the range panel in order to set manually the zoom
 */
void MenuBar::zoomSettings()
{
    MainWindow::getInstance()->getRangeSelectorPanel()->open();
}

/*
 * \brief Open the summary panel
 */
void MenuBar::summary()
{
    MainWindow::getInstance()->getSummaryPanel()->open();
}
} // namespace eventDisplay
} // namespace ntof
