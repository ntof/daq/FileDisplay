#ifndef MENUBAR_H
#define MENUBAR_H

#include <QMenu>
#include <QMenuBar>
#include <QObject>
#include <QToolBar>
#include <QWidget>

#include "XRootFileDialog.hpp"

class QWidget;
class QMenu;
class QToolBar;
class QMenuBar;

namespace ntof {
namespace eventDisplay {
class MainWindow;

class MenuBar : public QWidget
{
    Q_OBJECT
public:
    static std::string versionFileDisplay_; //!< Version number of the
                                            //!< application
    static std::string versionQCustomPlot_; //!< Version number of the
                                            //!< application
    static std::string versionQt_;          //!< Version number of the Qt
    static std::string versionBoost_; //!< Version number of the Boost library
    static std::string versionCMake_; //!< Version number of the CMake

    /*
     * \brief Constructor of the class
     */
    MenuBar();

    /*
     * \brief Destructor of the class
     */
    ~MenuBar();

    /* *********************************************************** */
    /* GETTERS                                                     */
    /* *********************************************************** */
    /*
     * \brief Get the menu bar object
     * \return Return a pointer of the menu bar object
     */
    QMenuBar *getMenuBar();

    /*
     * \brief Get the tool bar object
     * \return Return a pointer of the tool bar object
     */
    QToolBar *getToolBar();

    /*
     * \brief Get the zoom action
     * \return Return a pointer of the zoom action
     */
    QAction *getZoomModeAction();

    /*
     * \brief Get the drag'n'drop action
     * \return Return a pointer of the drag'n'drop action
     */
    QAction *getDragNDropModeAction();

    /*
     * \brief Get the undo action
     * \return Return a pointer of the undo action
     */
    QAction *getUndoAction();

    /*
     * \brief Get the redo action
     * \return Return a pointer of the redo action
     */
    QAction *getRedoAction();

    /*
     * \brief Get the action which write the debug file
     * \return Return a pointer of the action which write the debug file
     */
    QAction *getDebugFileAction();

    /*
     * \brief Get the action which write the info file
     * \return Return a pointer of the action which write the info file
     */
    QAction *getTextFileAction();

    /*
     * \brief Update this element and its children
     */
    void updateElement();

public slots:
    /*
     * \brief Open a file on a local storage
     */
    void openFile();

    /*
     * \brief Open a file from CASTOR
     */
    void openCastorFile();

private slots:
    /*
     * \brief Change the state of the zoom mode
     * \param state: New state to set
     */
    void zoomAction(bool state);

    /*
     * \brief Change the state of the drag'n'drop mode
     * \param state: New state to set
     */
    void dragNDropAction(bool state);

    /*
     * \brief Undo the last zoom action
     */
    void undoAction();

    /*
     * \brief Redo the last zoom action
     */
    void redoAction();

    /*
     * \brief Quit the application
     */
    void quitAction();

    /*
     * \brief Create a text file from the ones opened
     */
    void textFileAction();

    /*
     * \brief Create a debug file from the ones opened
     */
    void debugFileAction();

    /*
     * \brief Open the option panel
     */
    void paramsAction();

    /*
     * \brief Open the info panel
     */
    void about();

    /*
     * \brief Export the contain of the graph as a png
     */
    void pngExport();

    /*
     * \brief Open automatically the last run
     */
    //                void openLastRunFile();

    /*
     * \brief Open a defined run
     */
    void openParticularRunFile();

    /*
     * \brief Open the user documentation
     */
    void openDocUserFile();

    /*
     * \brief Reset the zoom
     */
    void resetZoom();

    /*
     * \brief Change the state of the scatter plot mode
     * \param state: change the state of the scatter plot mode
     */
    void scatterPlot(bool state);

    /*
     * \brief Change the state of the line plot mode
     * \param state: change the state of the line plot mode
     */
    void linePlot(bool state);

    /*
     * \brief Open the range panel in order to set manually the zoom
     */
    void zoomSettings();

    /*
     * \brief Open the summary panel
     */
    void summary();

private:
    /*
     * \brief Initialize the actions
     */
    void createActions();

    /*
     * \brief Initialize the menu
     */
    void createMenu();

    /*
     * \brief Create the toolbar
     */
    void createToolbar();

    XRootFileDialog *xrootFileDialog_; //!< Window allowing to open a file on
                                       //!< CASTOR
    QMenuBar *menuBar_;                //!< Menu bar object
    QMenu *fileMenu_;                  //!< Menu 'file'
    QMenu *optionsMenu_;               //!< Menu 'options'
    QMenu *debugMenu_;                 //!< Menu 'debug'
    QToolBar *toolBar_;                //!< Tool bar object
    QAction *openAction_;              //!< Open a file on a local storage
    QAction *openCastorAction_;        //!< Open a file on CASTOR
    QAction *openLastRunAction_;       //!< Allow to open the last run
    QAction *zoomModeAction_;          //!< Change the state of the zoom action
    QAction *dragNDropModeAction_;     //!< Change the state of the drag'n'drop
                                       //!< action
    QAction *scatterPlotAction_;       //!< Change the state of the scatter plot
    QAction *polylinePlotAction_;      //!< Change the state of the line plot
    QAction *undoZoomAction_;          //!< Undo the last zoom action
    QAction *redoZoomAction_;          //!< Redo the last zoom action
    QAction *resetZoomAction_;         //!< Reset the zoom in its original state
    QAction *zoomSettingsAction_; //!< Open a windows to set the zoom manually
    QAction *quitAction_;         //!< Quit the application
    QAction *textFileAction_;     //!< Create a text file (for the files opened)
    QAction *debugFileAction_; //!< Create a debug file (for the files opened)
    QAction *paramsAction_;    //!< Open the setting panel
    QAction *aboutAction_;     //!< Open the info panel
    QAction *pngExportAction_; //!< Export the content of the plot area into a
                               //!< png file
    QAction *userDocAction_;   //!< Open the user documentation
    QAction *summaryAction_;   //!< Open the summary panel
};
} // namespace eventDisplay
} // namespace ntof
#endif // MENUBAR_H
