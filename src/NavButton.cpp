#include "NavButton.h"

namespace ntof {
namespace eventDisplay {
/*
 * \brief Constructor of the class
 * \param parent: Parent element in the tree
 * \param name: Name to write on the button
 * \param path: path to reach when the button clicked
 */
NavButton::NavButton(XRootFileDialog *parent,
                     const std::string &name,
                     const std::string &path) :
    QPushButton(tr(name.c_str())), parent_(parent), name_(name), path_(path)
{
    QObject::connect(this, SIGNAL(clicked()), this, SLOT(click()));
}

/*
 * \brief Action to perform when the button is clicked
 */
void NavButton::click()
{
    parent_->getFileList(path_);
}
} // namespace eventDisplay
} // namespace ntof
