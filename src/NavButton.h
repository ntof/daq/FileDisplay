#ifndef NAVBUTTON_H
#define NAVBUTTON_H

#include <string>

#include <QObject>
#include <QPushButton>

#include "XRootFileDialog.hpp"

namespace ntof {
namespace eventDisplay {
class NavButton : public QPushButton
{
    Q_OBJECT
public:
    /*
     * \brief Constructor of the class
     * \param parent: Parent element in the tree
     * \param name: Name to write on the button
     * \param path: path to reach when the button clicked
     */
    NavButton(XRootFileDialog *parent,
              const std::string &name,
              const std::string &path);
private slots:
    /*
     * \brief Action to perform when the button is clicked
     */
    void click();

private:
    XRootFileDialog *parent_; //!< Parent in the tree
    std::string name_;        //!< Name to write on the button
    std::string path_;        //!< path to reach when the button clicked
};
} // namespace eventDisplay
} // namespace ntof

#endif // NAVBUTTON_H
