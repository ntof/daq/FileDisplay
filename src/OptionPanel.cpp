#include "OptionPanel.h"

#include <iostream>
#include <sstream>

#include <QColorDialog>
#include <QFileDialog>
#include <QFormLayout>
#include <QLabel>
#include <QMessageBox>
#include <QScrollArea>
#include <QTabWidget>

#include <unistd.h>

#include "Colorpicker.h"
#include "FormLayout.h"
#include "MainWindow.h"
#include "MenuBar.h"
#include "flowlayout.h"
#include "widgets/FileSelector.h"

namespace ntof {
namespace eventDisplay {
/*
 * \brief Constructor of the class
 */
OptionPanel::OptionPanel() :
    signalMapper_(new QSignalMapper()),
    detectors_(Config::getInstance()->getDetectors())
{
    //            dataUnit_ = Config::getInstance()->getDataUnit();
    //            modeDebug_ = Config::getInstance()->getModeDebug();
    //            userDoc_ = Config::getInstance()->getUserDoc();
    //            distanceEAR1_ = Config::getInstance()->getDistanceEAR1();
    //            distanceEAR2_ = Config::getInstance()->getDistanceEAR2();

    connect(signalMapper_, SIGNAL(mapped(QString)), this,
            SLOT(colorPickerAction(QString)));

    createPanel();
    initPanel();
}

/*
 * \brief Destructor of the class
 */
OptionPanel::~OptionPanel() {}

/*
 * \brief Initialize the content of the panel
 */
void OptionPanel::createPanel()
{
    QTabWidget *qtab = new QTabWidget();
    QString style = "*[hasFrame=\"true\"]{border:2px solid "
                    "gray;border-radius:3px;margin-top: 1ex;}";
    style += "*::title[hasFrame=\"true\"]{subcontrol-origin: "
             "margin;subcontrol-position:left top;padding:0px 10px 0px 10px;}";
    qtab->setStyleSheet(style);

    // GENERAL PANEL
    QGroupBox *generalGroupBox = new QGroupBox();
    {
        FormLayout *layout = new FormLayout(generalGroupBox);

        QButtonGroup *group = new QButtonGroup(
            generalGroupBox); /* handles exclusive states */
        QGroupBox *unitBox = new QGroupBox;
        unitBox->setLayout(new QHBoxLayout);

        buttonADC_ = new QPushButton(tr("ADC"));
        buttonADC_->setCheckable(true);
        buttonMV_ = new QPushButton(tr("mV"));
        buttonMV_->setCheckable(true);
        group->addButton(buttonADC_);
        group->addButton(buttonMV_);
        unitBox->layout()->addWidget(buttonADC_);
        unitBox->layout()->addWidget(buttonMV_);
        layout->addRow(tr("Data Unit"), unitBox);

        workspaceDir_ = new ExistingDirSelector(tr("Select workspace"), this);
        layout->addRow(tr("Default Workspace"), workspaceDir_);

        QWidget *cacheBox = new QWidget;
        cacheBox->setLayout(new QHBoxLayout);
        cacheBox->setSizePolicy(QSizePolicy::MinimumExpanding,
                                QSizePolicy::Preferred);

        cacheDir_ = new ExistingDirSelector(
            tr("Select directory for cache files"), this);
        layout->addRow(tr("Cache directory"), cacheDir_);

        cacheSizeLineEdit_ = new QLineEdit();
        cacheSizeLineEdit_->setAlignment(Qt::AlignRight);
        layout->addRow(tr("Cache size (GB)"), cacheSizeLineEdit_);

        widthLineEdit_ = new QLineEdit();
        widthLineEdit_->setAlignment(Qt::AlignRight);
        layout->addRow(tr("Width"), widthLineEdit_);

        heightLineEdit_ = new QLineEdit();
        heightLineEdit_->setAlignment(Qt::AlignRight);
        layout->addRow(tr("Height"), heightLineEdit_);

        xrootPath_ = new QLineEdit(generalGroupBox);
        layout->addRow(tr("XRoot uri"), xrootPath_);
    }
    qtab->addTab(generalGroupBox, QString("General"));

    // COLOR PANEL
    QGroupBox *colorGroupBox = new QGroupBox();
    QScrollArea *scrollArea = new QScrollArea(this);
    scrollArea->setWidgetResizable(true);
    scrollArea->setWidget(colorGroupBox);
    colorLayout_ = new QVBoxLayout();

    for (std::vector<Detector>::iterator detector = detectors_.begin();
         detector != detectors_.end(); ++detector)
    {
        addColorButton((*detector).name, (*detector).id, (*detector).red,
                       (*detector).green, (*detector).blue);
    }

    colorLayout_->addStretch();
    colorGroupBox->setLayout(colorLayout_);
    qtab->addTab(scrollArea, QString("Color"));

    // ADVANCED PANEL
    QGroupBox *advancedGroupBox = new QGroupBox;
    advancedGroupBox->setLayout(new QVBoxLayout());
    {
        QGroupBox *group = new QGroupBox(tr("Software configuration"));
        group->setProperty("hasFrame", true);
        FormLayout *layout = new FormLayout(group);

        debugButtons_ = new QCheckBox(group);
        layout->addRow(tr("Debug Mode"), debugButtons_);

        openGLButtons_ = new QCheckBox(group);
        layout->addRow(tr("OpenGL Rendering"), openGLButtons_);

        measurementHints_ = new QCheckBox(group);
        layout->addRow(tr("Measurement hints"), measurementHints_);

        aggregateDaqFiles_ = new QCheckBox(group);
        layout->addRow(tr("Aggregate DAQ files"), aggregateDaqFiles_);

        QWidget *docBox = new QWidget(group);
        docBox->setLayout(new QHBoxLayout);
        docBox->setSizePolicy(QSizePolicy::MinimumExpanding,
                              QSizePolicy::Preferred);

        userDoc_ = new FileSelector(tr("Select the user documentation file"),
                                    "PDF (*.pdf)", this);
        layout->addRow(tr("User documentation"), userDoc_);

        advancedGroupBox->layout()->addWidget(group);
    }
    {
        QGroupBox *group = new QGroupBox(tr("Expert configuration"));
        group->setProperty("hasFrame", true);
        FormLayout *layout = new FormLayout(group);

        distanceEAR1LineEdit_ = new QLineEdit();
        distanceEAR1LineEdit_->setAlignment(Qt::AlignRight);
        layout->addRow(tr("Distance EAR1 (m)"), distanceEAR1LineEdit_);

        distanceEAR2LineEdit_ = new QLineEdit();
        distanceEAR2LineEdit_->setAlignment(Qt::AlignRight);
        layout->addRow(tr("Distance EAR2 (m)"), distanceEAR2LineEdit_);

        tGammaLineEdit_ = new QLineEdit();
        tGammaLineEdit_->setAlignment(Qt::AlignRight);
        layout->addRow(tr("T gamma (ns)"), tGammaLineEdit_);

        advancedGroupBox->layout()->addWidget(group);
    }
    qtab->addTab(advancedGroupBox, QString("Advanced"));

    // VALIDATION PART
    QDialogButtonBox *buttonBox = new QDialogButtonBox(
        QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    // CREATION OF THE WINDOW
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(qtab);
    mainLayout->addWidget(buttonBox);
    setLayout(mainLayout);
    resize(700, 500);
}

/*
 * \brief Set the color of a detector
 * \param detectorName: Name of the detector
 * \param detectorId: ID of the detector
 * \param red: Red value
 * \param green: Green value
 * \param blue: Blue value
 */
void OptionPanel::setColor(const std::string &detectorName,
                           uint32_t detectorId,
                           int32_t red,
                           int32_t green,
                           int32_t blue)
{
    for (std::vector<Detector>::iterator detector = detectors_.begin();
         detector != detectors_.end(); ++detector)
    {
        if ((*detector).name == detectorName && (*detector).id == detectorId)
        {
            (*detector).red = red;
            (*detector).green = green;
            (*detector).blue = blue;
            break;
        }
    }

    for (std::vector<DetectorButton>::iterator detector =
             detectorsButton_.begin();
         detector != detectorsButton_.end(); ++detector)
    {
        if ((*detector).name == detectorName && (*detector).id == detectorId)
        {
            std::ostringstream ossColor;
            ossColor << "background-color: rgb(" << red << ", " << green << ", "
                     << blue << ")";
            (*detector).button->setStyleSheet(QString(ossColor.str().c_str()));
            break;
        }
    }
}

/*
 * \brief Add a color button in the color tab
 * \param name: Name of the detector
 * \param id: ID of the detector
 * \param red: Red value
 * \param green: Green value
 * \param blue: Blue value
 */
void OptionPanel::addColorButton(const std::string &name,
                                 uint32_t id,
                                 int32_t red,
                                 int32_t green,
                                 int32_t blue)
{
    std::ostringstream oss;
    oss << name << "_" << id;
    FlowLayout *detectorLayout = new FlowLayout();
    QGroupBox *detectorGroupBox = new QGroupBox();
    QLabel *detectorName = new QLabel(QString(oss.str().c_str()));
    detectorName->setFixedWidth(100);
    detectorName->setContentsMargins(0, 4, 0, 0);
    detectorLayout->addWidget(detectorName);

    QPushButton *button = new QPushButton();
    std::ostringstream ossColor;
    ossColor << "background-color: rgb(" << red << ", " << green << ", " << blue
             << ")";
    button->setStyleSheet(QString(ossColor.str().c_str()));
    connect(button, SIGNAL(clicked()), signalMapper_, SLOT(map()));
    signalMapper_->setMapping(button, QString(oss.str().c_str()));
    detectorLayout->addWidget(button);

    DetectorButton detectorButton;
    detectorButton.name = name;
    detectorButton.id = id;
    detectorButton.button = button;
    detectorsButton_.push_back(detectorButton);

    detectorGroupBox->setLayout(detectorLayout);
    colorLayout_->addWidget(detectorGroupBox);

    detectorLayout->setContentsMargins(0, 0, 0, 0);
    detectorGroupBox->setContentsMargins(0, 0, 0, 0);
}

/*
 * \brief Update the list of the detectors
 */
void OptionPanel::updateDetectorList()
{
    detectors_ = Config::getInstance()->getDetectors();
    for (std::vector<Detector>::iterator detector = detectors_.begin();
         detector != detectors_.end(); ++detector)
    {
        bool found = false;
        for (std::vector<DetectorButton>::iterator detectorButton =
                 detectorsButton_.begin();
             detectorButton != detectorsButton_.end(); ++detectorButton)
        {
            if ((*detector).name == (*detectorButton).name &&
                (*detector).id == (*detectorButton).id)
            {
                found = true;
                break;
            }
        }
        if (!found)
        {
            addColorButton((*detector).name, (*detector).id, (*detector).red,
                           (*detector).green, (*detector).blue);
        }
    }
}

/*
 * \brief Valid the modifications
 */
void OptionPanel::accept()
{
    Config *cfg = Config::getInstance();
    std::string dataUnitActual = cfg->getDataUnit();
    std::string dataUnit = buttonMV_->isChecked() ? "mv" : "adc";
    MainWindow::getInstance()->setToReplot(false);
    if (dataUnit != dataUnitActual)
    {
        cfg->setDataUnit(dataUnit);
        QMessageBox msgBox;
        msgBox.setText(
            "The file(s) need to be reloaded to apply the new data type.");
        msgBox.setInformativeText("Do you want to reload the file now ?");
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Ok);
        int32_t result = msgBox.exec();
        this->setCursor(Qt::WaitCursor);
        if (result == QMessageBox::Ok)
        {
            MainWindow::getInstance()->reloadFiles(false);
            MainWindow::getInstance()->setToReplot(true);
        }
    }

    // Repeat the modification of the cursor if it has not been done before
    this->setCursor(Qt::WaitCursor);
    std::string modeDebug = debugButtons_->isChecked() ? "on" : "off";
    cfg->setOpenGLRendering(openGLButtons_->isChecked());
    cfg->setMeasurementHints(measurementHints_->isChecked());
    cfg->setAggregateDaqFiles(aggregateDaqFiles_->isChecked());
    cfg->setXRootPath(xrootPath_->text().toStdString());
    cfg->setWorkspace(workspaceDir_->path().toStdString());
    cfg->setModeDebug(modeDebug);
    cfg->setDetectors(detectors_);
    cfg->setUserDoc(userDoc_->path().toStdString());
    cfg->setDistanceEAR1(distanceEAR1LineEdit_->text().toDouble());
    cfg->setDistanceEAR2(distanceEAR2LineEdit_->text().toDouble());
    cfg->setTGamma(tGammaLineEdit_->text().toInt());
    cfg->setCacheDir(cacheDir_->path().toStdString());
    cfg->setCacheSize(cacheSizeLineEdit_->text().toInt());

    int32_t height = heightLineEdit_->text().toInt();
    if (height > 0)
    {
        cfg->setHeight(height);
    }

    int32_t width = widthLineEdit_->text().toInt();
    if (width > 0)
    {
        cfg->setWidth(width);
    }

    if (!cfg->writeFile())
    {
        QMessageBox::warning(this, QObject::tr("Warning"),
                             QObject::tr("Error to save the configuration"),
                             QMessageBox::Ok);
    }

    // Update the interface
    MainWindow::getInstance()->setTGamma();
    MainWindow::getInstance()->updateElement();
    MainWindow::getInstance()->updateRangeTitle();

    this->setCursor(Qt::ArrowCursor);
    close();
}

/*
 * \brief Initialize the content of the panel
 */
void OptionPanel::initPanel()
{
    Config *cfg = Config::getInstance();
    if (cfg->getDataUnit() == "adc")
    {
        buttonADC_->setChecked(true);
        buttonMV_->setChecked(false);
    }
    else
    {
        buttonADC_->setChecked(false);
        buttonMV_->setChecked(true);
    }

    workspaceDir_->setPath(cfg->getWorkspace().c_str());
    cacheDir_->setPath(cfg->getCacheDir().c_str());
    cacheSizeLineEdit_->setText(QString::number(cfg->getCacheSize()));

    if (cfg->getModeDebug() == "on")
    {
        debugButtons_->setChecked(true);
    }
    openGLButtons_->setChecked(cfg->getOpenGLRendering());
    measurementHints_->setChecked(cfg->getMeasurementHints());
    aggregateDaqFiles_->setChecked(cfg->isAggregateDaqFiles());
    xrootPath_->setText(cfg->getXRootPath().c_str());

    userDoc_->setPath(cfg->getUserDoc().c_str());
    distanceEAR1LineEdit_->setText(QString::number(cfg->getDistanceEAR1()));
    distanceEAR2LineEdit_->setText(QString::number(cfg->getDistanceEAR2()));
    tGammaLineEdit_->setText(QString::number(cfg->getTGamma()));
    heightLineEdit_->setText(QString::number(cfg->getHeight()));
    widthLineEdit_->setText(QString::number(cfg->getWidth()));
}

/*
 * \brief Close the options panel without saving the data
 */
void OptionPanel::reject()
{
    initPanel();
    close();
}

/*
 * \brief Open the color picker with the good color
 * \param detectorQStr: name of the detector to set
 */
void OptionPanel::colorPickerAction(QString detectorQStr)
{
    std::string detectorStr = detectorQStr.toStdString();
    // std::cout << detectorStr << std::endl;
    std::size_t found = detectorStr.find("_");
    if (found != std::string::npos)
    {
        std::string name = detectorStr.substr(0, found);
        std::string idStr = detectorStr.substr(
            found + 1, (detectorStr.length() - (found + 1)));
        std::istringstream buffer(idStr);
        uint32_t id;
        buffer >> id;
        Detector detector = getDetectorConf(name, id);
        if (detector.name != "")
        {
            // std::cout << "Detector " << detector.name << "_" << detector.id
            // << " [" << detector.red << ";" << detector.green << ";" <<
            // detector.blue << "]" << std::endl;

            ColorPicker *colorPicker = ColorPicker::getInstance();
            colorPicker->setParent(this);
            colorPicker->setDetector(detector.name, detector.id);
            colorPicker->setCurrentColor(
                QColor(detector.red, detector.green, detector.blue));
            colorPicker->open();
            // colorPicker->setVisible(true);
        }
        else
        {
            std::cout << "Unknown detector" << std::endl;
        }
    }
}

/*
 * \brief Get the configuration of a particular detector
 * \param detectorName: Name of the detector
 * \param detectorId: ID of the detector
 */
Detector OptionPanel::getDetectorConf(const std::string &detectorName,
                                      uint32_t detectorId)
{
    for (std::vector<Detector>::iterator detector = detectors_.begin();
         detector != detectors_.end(); ++detector)
    {
        if ((*detector).name == detectorName && (*detector).id == detectorId)
        {
            return (*detector);
        }
    }
    return Detector();
}

/*
 * \brief Open the options panel
 */
void OptionPanel::open()
{
    updateDetectorList();
    initPanel();
    this->setVisible(true);
}

/*
 * \brief Close the options panel
 */
void OptionPanel::close()
{
    ColorPicker::getInstance()->setVisible(false);
    this->setVisible(false);
}
} // namespace eventDisplay
} // namespace ntof
