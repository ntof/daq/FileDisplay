#ifndef OPTIONPANEL_H
#define OPTIONPANEL_H

#include <QCheckBox>
#include <QDialog>
#include <QDialogButtonBox>
#include <QGroupBox>
#include <QLayout>
#include <QLineEdit>
#include <QObject>
#include <QPushButton>
#include <QSignalMapper>

#include <stdint.h>

#include "Config.h"

class ExistingDirSelector;
class FileSelector;

namespace ntof {
namespace eventDisplay {
/* TYPEDEF */
typedef struct
{
    std::string name;
    uint32_t id;
    QPushButton *button;
} DetectorButton;

class MainWindow;

class OptionPanel : public QDialog
{
    Q_OBJECT
public:
    /*
     * \brief Constructor of the class
     */
    OptionPanel();

    /*
     * \brief Destructor of the class
     */
    ~OptionPanel();

    /*
     * \brief Initialize the content of the panel
     */
    void initPanel();

    /*
     * \brief Set the color of a detector
     * \param detectorName: Name of the detector
     * \param detectorId: ID of the detector
     * \param red: Red value
     * \param green: Green value
     * \param blue: Blue value
     */
    void setColor(const std::string &detectorName,
                  uint32_t detectorId,
                  int32_t red,
                  int32_t green,
                  int32_t blue);

    /*
     * \brief Add a color button in the color tab
     * \param name: Name of the detector
     * \param id: ID of the detector
     * \param red: Red value
     * \param green: Green value
     * \param blue: Blue value
     */
    void addColorButton(const std::string &name,
                        uint32_t id,
                        int32_t red,
                        int32_t green,
                        int32_t blue);

    /*
     * \brief Update the list of the detectors
     */
    void updateDetectorList();

    /*
     * \brief Get the configuration of a particular detector
     * \param detectorName: Name of the detector
     * \param detectorId: ID of the detector
     */
    Detector getDetectorConf(const std::string &detectorName,
                             uint32_t detectorId);

    /*
     * \brief Open the options panel
     */
    void open();

    /*
     * \brief Close the options panel
     */
    void close();

private slots:
    /*
     * \brief Valid the modifications
     */
    void accept();

    /*
     * \brief Close the options panel without saving the data
     */
    void reject();

    /*
     * \brief Open the color picker with the good color
     * \param detectorQStr: name of the detector to set
     */
    void colorPickerAction(QString detectorQStr);

private:
    /*
     * \brief Initialize the content of the panel
     */
    void createPanel();

    QVBoxLayout *colorLayout_; //!< Layout of the color tab
    QPushButton *buttonADC_;   //!< Button used to set the data unit in ADC
    QPushButton *buttonMV_; //!< Button used to set the data unit in millivolts
    ExistingDirSelector *workspaceDir_; //!< Line edit to set the workspace

    ExistingDirSelector *cacheDir_;
    QLineEdit *cacheSizeLineEdit_;

    QCheckBox *debugButtons_;      //!< Debug mode status
    QCheckBox *openGLButtons_;     //!< OpenGL rendering toggle button
    QCheckBox *measurementHints_;  //!< Zoom ToolTips toggle button
    QCheckBox *aggregateDaqFiles_; //!< Aggregate files from DAQ
    QLineEdit *xrootPath_;         //!< base XRoot path for CASTOR/CTA
    QSignalMapper *signalMapper_;  //!< Allow to send a signal with a parameter
    FileSelector *userDoc_;        //!< Full path of the user documentation
    QLineEdit *distanceEAR1LineEdit_; //!< Distance between the target and the
                                      //!< detectors of EAR1
    QLineEdit *distanceEAR2LineEdit_; //!< Distance between the target and the
                                      //!< detectors of EAR2
    QLineEdit *tGammaLineEdit_; //!< Value of the time reference of the energy
                                //!< calculations
    QLineEdit *widthLineEdit_;  //!< Width of the main window at the opening
    QLineEdit *heightLineEdit_; //!< Height of the main window at the opening
    std::vector<Detector> detectors_;             //!< List of the detectors
    std::vector<DetectorButton> detectorsButton_; //!< List of the detectors
                                                  //!< color buttons
};
} // namespace eventDisplay
} // namespace ntof

#endif // OPTIONPANEL_H
