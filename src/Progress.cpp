/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-03T17:43:04+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "Progress.h"

#include <iostream>

#include <QProgressDialog>
#include <QtConcurrent>

namespace ntof {

namespace eventDisplay {

Progress::Progress(lib::NtofLib &lib, const QString &title, const QString &text) :
    QObject(), m_lib(lib), m_dialog(new QProgressDialog()), m_cancel(false)
{
    m_dialog->setWindowTitle(title);
    m_dialog->setLabelText(text);
    m_dialog->setAutoReset(false);
    m_dialog->setMaximum(0);
    m_dialog->setValue(0); /* spin mode */

    connect(m_dialog, SIGNAL(canceled()), this, SLOT(onCanceled()));
    m_lib.setNotificationListener(this);
}

Progress::~Progress()
{
    m_dialog->deleteLater();
    m_lib.setNotificationListener(0);
}

bool Progress::onProgress(lib::Notification::State state,
                          size_t current,
                          size_t total)
{
    QString text;
    switch (state)
    {
    case lib::Notification::FILE_FETCH:
        text = QObject::tr("Fetching file ...");
        break;
    case lib::Notification::HEADER_PARSE:
        text = QObject::tr("Parsing headers ...");
        break;
    case lib::Notification::ACQC_INIT:
        text = QObject::tr("Parsing ACQC ...");
        break;
    case lib::Notification::ADDH_INIT:
        text = QObject::tr("Parsing ADDH ...");
        break;
    case lib::Notification::MODH_INIT:
        text = QObject::tr("Parsing MODH ...");
        break;
    default: text = QObject::tr("In progress ..."); break;
    }

    QMetaObject::invokeMethod(this, "update", Q_ARG(QString, text),
                              Q_ARG(int, current), Q_ARG(int, total));
    return !m_cancel;
}

template<typename T>
void Progress::exec(T &future)
{
    QFutureWatcher<typename T::const_iterator::value_type> watcher;
    connect(&watcher, SIGNAL(finished()), m_dialog, SLOT(accept()));

    watcher.setFuture(future);
    m_dialog->exec();
    watcher.waitForFinished(); /* if we cancel, this must stop */
}

template void Progress::exec(QFuture<bool> &future);

void Progress::update(const QString &info, int current, int total)
{
    m_dialog->setMaximum(total);
    m_dialog->setValue(current);
    m_dialog->setLabelText(info);
}

void Progress::onCanceled()
{
    m_cancel = true;
}

Progress::Exception::Exception(const ntof::lib::NtofLibException &ex) :
    m_what(ex.getMessage())
{}

} // namespace eventDisplay
} // namespace ntof
