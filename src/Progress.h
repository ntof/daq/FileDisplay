/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-03T17:43:21+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef PROGRESS_H_
#define PROGRESS_H_

#include <QException>
#include <QObject>
#include <QString>

#include <NtofLib.h>

class QProgressDialog;

namespace ntof {
namespace eventDisplay {

class Progress : public QObject, public ntof::lib::Notification::Listener
{
    Q_OBJECT
public:
    Progress(ntof::lib::NtofLib &lib, const QString &title, const QString &text);
    ~Progress();

    template<typename T>
    void exec(T &future);

    /**
     * @brief ntoflib progress callback
     */
    bool onProgress(ntof::lib::Notification::State state,
                    size_t current,
                    size_t total);

    inline QProgressDialog &dialog() { return *m_dialog; }

    inline bool isCanceled() const { return m_cancel; }

    class Exception : public QException
    {
    public:
        explicit Exception(const ntof::lib::NtofLibException &ex);

        inline const QString &message() const { return m_what; }

        void raise() const override { throw *this; }
        Exception *clone() const override { return new Exception(*this); }

    protected:
        QString m_what;
    };

protected slots:
    void update(const QString &state, int current, int total);
    void onCanceled();

protected:
    ntof::lib::NtofLib &m_lib;
    QProgressDialog *m_dialog;
    bool m_cancel;
};

} // namespace eventDisplay
} // namespace ntof

#endif // PROGRESS_H_
