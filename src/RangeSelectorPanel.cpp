#include "RangeSelectorPanel.h"

#include <iostream>

#include <QDialogButtonBox>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include "CustomPlotZoom.h"
#include "MainWindow.h"

namespace ntof {
namespace eventDisplay {
/*
 * \brief Constructor of the class
 */
RangeSelectorPanel::RangeSelectorPanel()
{
    initPanel();
}

/*
 * \brief Destructor of the class
 */
RangeSelectorPanel::~RangeSelectorPanel()
{
    delete px1_;
    delete px2_;
    delete py1_;
    delete py2_;
}

/*
 * \brief Create the content of the panel
 */
void RangeSelectorPanel::initPanel()
{
    QGroupBox *px1Group = new QGroupBox();
    px1_ = new QLineEdit();
    px1_->setMinimumWidth(150);
    QHBoxLayout *px1Layout = new QHBoxLayout();
    QPushButton *px1Button = new QPushButton();
    px1Button->setIcon(
        QIcon(QPixmap(":/icons/oxygen/48x48/actions/measure.png")));
    px1Button->setIconSize(QSize(20, 20));
    QObject::connect(px1Button, SIGNAL(clicked()), this, SLOT(setPX1Default()));
    px1Layout->addWidget(new QLabel("Width min: "));
    px1Layout->addWidget(px1_);
    px1Layout->addWidget(px1Button);
    px1Group->setLayout(px1Layout);

    QGroupBox *px2Group = new QGroupBox();
    px2_ = new QLineEdit();
    px2_->setMinimumWidth(150);
    QHBoxLayout *px2Layout = new QHBoxLayout();
    QPushButton *px2Button = new QPushButton();
    px2Button->setIcon(
        QIcon(QPixmap(":/icons/oxygen/48x48/actions/measure.png")));
    px2Button->setIconSize(QSize(20, 20));
    QObject::connect(px2Button, SIGNAL(clicked()), this, SLOT(setPX2Default()));
    px2Layout->addWidget(new QLabel("Width max: "));
    px2Layout->addWidget(px2_);
    px2Layout->addWidget(px2Button);
    px2Group->setLayout(px2Layout);

    QGroupBox *py1Group = new QGroupBox();
    py1_ = new QLineEdit();
    py1_->setMinimumWidth(150);
    QHBoxLayout *py1Layout = new QHBoxLayout();
    QPushButton *py1Button = new QPushButton();
    py1Button->setIcon(
        QIcon(QPixmap(":/icons/oxygen/48x48/actions/measure.png")));
    py1Button->setIconSize(QSize(20, 20));
    QObject::connect(py1Button, SIGNAL(clicked()), this, SLOT(setPY1Default()));
    py1Layout->addWidget(new QLabel("Height min: "));
    py1Layout->addWidget(py1_);
    py1Layout->addWidget(py1Button);
    py1Group->setLayout(py1Layout);

    QGroupBox *py2Group = new QGroupBox();
    py2_ = new QLineEdit();
    py2_->setMinimumWidth(150);
    QHBoxLayout *py2Layout = new QHBoxLayout();
    QPushButton *py2Button = new QPushButton();
    py2Button->setIcon(
        QIcon(QPixmap(":/icons/oxygen/48x48/actions/measure.png")));
    py2Button->setIconSize(QSize(20, 20));
    QObject::connect(py2Button, SIGNAL(clicked()), this, SLOT(setPY2Default()));
    py2Layout->addWidget(new QLabel("Height max: "));
    py2Layout->addWidget(py2_);
    py2Layout->addWidget(py2Button);
    py2Group->setLayout(py2Layout);

    // VALIDATION PART
    QDialogButtonBox *buttonBox = new QDialogButtonBox(
        QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(valid()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(cancel()));

    // CREATION OF THE PANEL
    QGroupBox *contentGroup = new QGroupBox();
    QGridLayout *contentLayout = new QGridLayout();
    contentLayout->addWidget(py1Group, 0, 0);
    contentLayout->addWidget(py2Group, 0, 1);
    contentLayout->addWidget(px1Group, 1, 0);
    contentLayout->addWidget(px2Group, 1, 1);
    contentGroup->setLayout(contentLayout);

    // CREATION OF THE WINDOW
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(contentGroup);
    mainLayout->addWidget(buttonBox);
    setLayout(mainLayout);
    resize(500, 300);
}

/* ******************************************************* */
/* SLOTS                                                   */
/* ******************************************************* */
/*
 * \brief Apply the modifications and close the panel
 */
void RangeSelectorPanel::valid()
{
    this->setCursor(Qt::WaitCursor);
    double px1 = px1_->text().toDouble();
    double px2 = px2_->text().toDouble();
    double py1 = py1_->text().toDouble();
    double py2 = py2_->text().toDouble();
    MainWindow::getInstance()->getCustomPlot()->setZoom(px1, py1, px2, py2);
    MainWindow::getInstance()->setToReplot(true);
    MainWindow::getInstance()->updateElement();
    this->setCursor(Qt::ArrowCursor);
    setVisible(false);
}

/*
 * \brief Close the panel without apply the modifications
 */
void RangeSelectorPanel::cancel()
{
    setVisible(false);
}

/*
 * \brief Reset the minimal default value of the X axis
 */
void RangeSelectorPanel::setPX1Default()
{
    Zoom range = MainWindow::getInstance()->getCustomPlot()->getInitialZoom();
    std::ostringstream oss;
    oss << range.xAxis.lower;
    setPX1(oss.str());
}

/*
 * \brief Reset the maximal default value of the X axis
 */
void RangeSelectorPanel::setPX2Default()
{
    Zoom range = MainWindow::getInstance()->getCustomPlot()->getInitialZoom();
    std::ostringstream oss;
    oss << range.xAxis.upper;
    setPX2(oss.str());
}

/*
 * \brief Reset the minimal default value of the Y axis
 */
void RangeSelectorPanel::setPY1Default()
{
    Zoom range = MainWindow::getInstance()->getCustomPlot()->getInitialZoom();
    std::ostringstream oss;
    oss << range.yAxis.lower;
    setPY1(oss.str());
}

/*
 * \brief Reset the maximal default value of the Y axis
 */
void RangeSelectorPanel::setPY2Default()
{
    Zoom range = MainWindow::getInstance()->getCustomPlot()->getInitialZoom();
    std::ostringstream oss;
    oss << range.yAxis.upper;
    setPY2(oss.str());
}

/*
 * \brief Fill the edit line with the minimal default value of the X axis
 * \param value: value to set
 */
void RangeSelectorPanel::setPX1(std::string value)
{
    px1_->setText(tr(value.c_str()));
}

/*
 * \brief Fill the edit line with the maximal default value of the X axis
 * \param value: value to set
 */
void RangeSelectorPanel::setPX2(std::string value)
{
    px2_->setText(tr(value.c_str()));
}

/*
 * \brief Fill the edit line with the minimal default value of the Y axis
 * \param value: value to set
 */
void RangeSelectorPanel::setPY1(std::string value)
{
    py1_->setText(tr(value.c_str()));
}

/*
 * \brief Fill the edit line with the maximal default value of the Y axis
 * \param value: value to set
 */
void RangeSelectorPanel::setPY2(std::string value)
{
    py2_->setText(tr(value.c_str()));
}

/*
 * \brief Fill the content and open the panel
 */
void RangeSelectorPanel::open()
{
    Zoom range = MainWindow::getInstance()->getCustomPlot()->getZoom();
    std::ostringstream ossPX1;
    ossPX1 << range.xAxis.lower;
    setPX1(ossPX1.str());

    std::ostringstream ossPX2;
    ossPX2 << range.xAxis.upper;
    setPX2(ossPX2.str());

    std::ostringstream ossPY1;
    ossPY1 << range.yAxis.lower;
    setPY1(ossPY1.str());

    std::ostringstream ossPY2;
    ossPY2 << range.yAxis.upper;
    setPY2(ossPY2.str());
    this->setVisible(true);
}
} // namespace eventDisplay
} // namespace ntof
