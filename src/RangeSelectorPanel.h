#ifndef RANGESELECTORPANEL_H
#define RANGESELECTORPANEL_H

#include <QDialog>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QObject>

namespace ntof {
namespace eventDisplay {
class MainWindow;

class RangeSelectorPanel : public QDialog
{
    Q_OBJECT
public:
    /*
     * \brief Constructor of the class
     */
    RangeSelectorPanel();

    /*
     * \brief Destructor of the class
     */
    ~RangeSelectorPanel();

    /*
     * \brief Fill the content and open the panel
     */
    void open();

private slots:
    /*
     * \brief Reset the minimal default value of the X axis
     */
    void setPX1Default();

    /*
     * \brief Reset the maximal default value of the X axis
     */
    void setPX2Default();

    /*
     * \brief Reset the minimal default value of the Y axis
     */
    void setPY1Default();

    /*
     * \brief Reset the maximal default value of the Y axis
     */
    void setPY2Default();

    /*
     * \brief Apply the modifications and close the panel
     */
    void valid();

    /*
     * \brief Close the panel without apply the modifications
     */
    void cancel();

private:
    /*
     * \brief Create the content of the panel
     */
    void initPanel();

    /*
     * \brief Fill the edit line with the minimal default value of the X axis
     * \param value: value to set
     */
    void setPX1(std::string value);

    /*
     * \brief Fill the edit line with the maximal default value of the X axis
     * \param value: value to set
     */
    void setPX2(std::string value);

    /*
     * \brief Fill the edit line with the minimal default value of the Y axis
     * \param value: value to set
     */
    void setPY1(std::string value);

    /*
     * \brief Fill the edit line with the maximal default value of the Y axis
     * \param value: value to set
     */
    void setPY2(std::string value);

    QLineEdit *px1_; //!< Container of the minimal default value of the X axis
    QLineEdit *px2_; //!< Container of the maximal default value of the X axis
    QLineEdit *py1_; //!< Container of the minimal default value of the Y axis
    QLineEdit *py2_; //!< Container of the maximal default value of the Y axis
};
} // namespace eventDisplay
} // namespace ntof
#endif // RANGESELECTORPANEL_H
