#include "SearchFile.h"

#include <iostream>
#include <sstream>

#include <QFileInfo>
#include <QString>

#include <NtofLib.h>
#include <dirent.h>
#include <stdio.h>

#include "Config.h"
#include "FileReader.h"
#include "MainWindow.h"
#include "XRootFileDialog.hpp"

namespace ntof {
namespace eventDisplay {
/*
 * \brief Constructor of the class
 */
SearchFile::SearchFile() {}

/*
 * \brief Fill the list of files found at a particular path
 * \param path: Localization where the files are searched
 */
void SearchFile::listFiles(const std::string &path)
{
    files_.clear();
    DIR *repertory = opendir(path.c_str());
    if (repertory != NULL)
    {
        struct dirent *entry;

        while ((entry = readdir(repertory)) != NULL)
        {
            QFileInfo info(QObject::tr(entry->d_name));
            if (info.completeSuffix() == "raw.finished" ||
                info.completeSuffix() == "raw")
            {
                std::string filename = entry->d_name;
                files_.push_back(toNtofFile(path + "/" + filename));
            }
        }

        closedir(repertory);
    }
}

/*
 * \brief Convert a file to a structure to reach easily the event number, run
 * number, ... \param filepath: full path of the file \return Return the
 * structure completed
 */
NTOF_FILE SearchFile::toNtofFile(const std::string &filepath)
{
    NTOF_FILE file;
    if (filepath != "")
    {
        QFileInfo info(QObject::tr(filepath.c_str()));
        file.filepath = info.path().toStdString();
        file.filename = info.fileName().toStdString();

        size_t posFirstUnderscore = file.filename.find_first_of('_');
        size_t posSecondUnderscore = file.filename.find_first_of(
            '_', posFirstUnderscore + 1);
        size_t posPoint = file.filename.find_first_of('.');

        std::string runNumber = file.filename.substr(3, posFirstUnderscore);
        std::string extensionNumber = file.filename.substr(
            posFirstUnderscore + 1, posSecondUnderscore);
        std::string streamNumber = file.filename.substr(posSecondUnderscore + 2,
                                                        posPoint);

        std::istringstream runNumberBuffer(runNumber);
        runNumberBuffer >> file.runNumber;
        std::istringstream extensionNumberBuffer(extensionNumber);
        extensionNumberBuffer >> file.extensionNumber;
        std::istringstream streamNumberBuffer(streamNumber);
        streamNumberBuffer >> file.streamNumber;
    }
    return file;
}

/* ************************************************************************* */
/* LOCAL                                                                     */
/* ************************************************************************* */
/*
 * \brief Find the full path of the next file on local storage
 * searched in the Default Workspace
 * \param path: actual path
 * \param filename: actual filename
 * \return Return the full path of the next file
 */
std::string SearchFile::getNextLocalFile(const std::string &path,
                                         const std::string &filename)
{
    std::string res = "";
    if (filename == "")
    {
        return res;
    }

    listFiles(path);
    NTOF_FILE actualFile = toNtofFile(path + "/" + filename);
    NTOF_FILE nextFile;
    for (const NTOF_FILE &file : files_)
    {
        if (file.runNumber == actualFile.runNumber &&
            file.streamNumber == actualFile.streamNumber &&
            file.extensionNumber > actualFile.extensionNumber)
        {
            if (nextFile.filename.empty() ||
                nextFile.extensionNumber > file.extensionNumber)
            {
                nextFile = file;
            }
        }
    }
    if (nextFile.filename != "")
    {
        res = path + "/" + nextFile.filename;
    }
    return res;
}

/*
 * \brief Find the full path of the previous file on local storage
 * searched in the Default Workspace
 * \param path: actual path
 * \param filename: actual filename
 * \return Return the full path of the previous file
 */
std::string SearchFile::getPreviousLocalFile(const std::string &path,
                                             const std::string &filename)
{
    std::string res = "";
    if (filename == "")
    {
        return res;
    }

    listFiles(path);
    NTOF_FILE actualFile = toNtofFile(path + "/" + filename);
    NTOF_FILE previousFile;
    for (const NTOF_FILE &file : files_)
    {
        if (file.runNumber == actualFile.runNumber &&
            file.streamNumber == actualFile.streamNumber &&
            file.extensionNumber < actualFile.extensionNumber)
        {
            if (previousFile.filename.empty() ||
                previousFile.extensionNumber < file.extensionNumber)
            {
                previousFile = file;
            }
        }
    }
    if (previousFile.filename != "")
    {
        res = previousFile.filepath + "/" + previousFile.filename;
    }
    return res;
}

/*
 * \brief Find the full path of the first file related to a stream and a run
 * number on local storage \param streamID: ID of the stream \param runNumber:
 * Run number \return Return the full path of the file or an empty string
 */
std::string SearchFile::getFirstLocalFile(int32_t streamID, int32_t runNumber)
{
    NTOF_FILE firstFile;
    if (runNumber != 0)
    {
        listFiles(Config::getInstance()->getWorkspace());
        for (std::vector<NTOF_FILE>::iterator it = files_.begin();
             it != files_.end(); ++it)
        {
            if ((*it).runNumber == runNumber && (*it).streamNumber == streamID)
            {
                if (firstFile.filename == "" ||
                    firstFile.extensionNumber > (*it).extensionNumber)
                {
                    firstFile = (*it);
                }
            }
        }
    }
    std::string res = "";
    if (firstFile.filename != "")
    {
        res = firstFile.filepath + "/" + firstFile.filename;
    }
    return res;
}

/*
 * \brief Allow to know if there is a next file in the list
 * \return Return true if a next file has been found
 */
bool SearchFile::isNextLocalFile()
{
    bool results = false;
    std::vector<std::string> filenames =
        FileReader::getInstance()->getNtofLib().getFilenames();
    for (const std::string file : filenames)
    {
        std::string nextFilePath = file.substr(0, file.find_last_of('/'));
        std::string nextFile = getNextLocalFile(nextFilePath, file);
        if (nextFile != "")
        {
            results = true;
        }
        else
        {
            return false;
        }
    }
    return results;
}

/*
 * \brief Allow to know if there is a previous file in the list
 * \return Return true if a previous file has been found
 */
bool SearchFile::isPreviousLocalFile()
{
    bool results = false;
    std::vector<std::string> filenames =
        FileReader::getInstance()->getNtofLib().getFilenames();
    for (const std::string file : filenames)
    {
        std::string nextFilePath = file.substr(0, file.find_last_of('/'));
        std::string nextFile = getPreviousLocalFile(nextFilePath, file);
        if (nextFile != "")
        {
            results = true;
        }
        else
        {
            return false;
        }
    }
    return results;
}

/* ************************************************************************* */
/* CASTOR                                                                    */
/* ************************************************************************* */
/*
 * \brief Find the full path of the next file on CASTOR
 * \param xrootPath: actual xrootPath
 * \param filename: actual filename
 * \return Return the full path of the next file
 */
std::string SearchFile::getNextCastorFile(const std::string &xRootPath,
                                          const std::string &filename)
{
    std::string fullpath = xRootPath + "/" + filename;
    std::string result = "";
    NTOF_FILE actualFile = toNtofFile(fullpath);
    NTOF_FILE nextFile;
    XRootBrowser::SharedFileInfo xInfo = XRootBrowser::getInfo(fullpath);

    try
    {
        browser_.chdir(*(xInfo->path));
        for (XRootBrowser::SharedFileInfo &info : browser_.files())
        {
            NTOF_FILE file = toNtofFile(info->fullpath());
            if (file.runNumber == actualFile.runNumber &&
                file.streamNumber == actualFile.streamNumber &&
                file.extensionNumber > actualFile.extensionNumber)
            {
                if (nextFile.filename.empty() ||
                    nextFile.extensionNumber > file.extensionNumber)
                {
                    nextFile = file;
                }
            }
        }
        if (nextFile.filename != "")
        {
            result = xRootPath + "/" + nextFile.filename;
            if (XRootBrowser::getInfo(result)->isOffline())
            {
                result = "";
            }
        }
    }
    catch (XRootBrowser::Exception &ex)
    {
        std::cerr << "Unable to read XRoot directory: " << ex.what()
                  << std::endl;
    }
    return result;
}

/*
 * \brief Find the full path of the previous file on CASTOR
 * \param xrootPath: actual xrootPath
 * \param filename: actual filename
 * \return Return the full path of the previous file
 */
std::string SearchFile::getPreviousCastorFile(const std::string &xRootPath,
                                              const std::string &filename)
{
    std::string fullpath = xRootPath + "/" + filename;
    std::string result = "";
    NTOF_FILE actualFile = toNtofFile(filename);
    NTOF_FILE previousFile;
    XRootBrowser::SharedFileInfo xInfo = XRootBrowser::getInfo(fullpath);

    try
    {
        browser_.chdir(*(xInfo->path));
        for (XRootBrowser::SharedFileInfo &info : browser_.files())
        {
            NTOF_FILE file = toNtofFile(info->fullpath());
            if (file.runNumber == actualFile.runNumber &&
                file.streamNumber == actualFile.streamNumber &&
                file.extensionNumber < actualFile.extensionNumber)
            {
                if (previousFile.filename.empty() ||
                    previousFile.extensionNumber < file.extensionNumber)
                {
                    previousFile = file;
                }
            }
        }
        if (previousFile.filename != "")
        {
            result = xRootPath + "/" + previousFile.filename;
            if (XRootBrowser::getInfo(result)->isOffline())
            {
                result = "";
            }
        }
    }
    catch (XRootBrowser::Exception &ex)
    {
        std::cerr << "Unable to read XRoot directory: " << ex.what()
                  << std::endl;
    }
    return result;
}

/*
 * \brief Allow to know if there is a next file in the list
 * \return Return true if a next file has been found
 */
bool SearchFile::isNextCastorFile()
{
    bool results = false;
    std::vector<std::string> filenames =
        FileReader::getInstance()->getNtofLib().getFilenames();
    for (const std::string file : filenames)
    {
        std::string nextFilePath = file.substr(0, file.find_last_of('/'));
        std::string nextFile = getNextCastorFile(nextFilePath, file);
        if (nextFile != "")
        {
            results = true;
        }
        else
        {
            return false;
        }
    }
    return results;
}

/*
 * \brief Allow to know if there is a previous file in the list
 * \return Return true if a previous file has been found
 */
bool SearchFile::isPreviousCastorFile()
{
    bool results = false;
    std::vector<std::string> filenames =
        FileReader::getInstance()->getNtofLib().getFilenames();
    for (const std::string file : filenames)
    {
        std::string nextFilePath = file.substr(0, file.find_last_of('/'));
        std::string nextFile = getPreviousCastorFile(nextFilePath, file);
        if (nextFile != "")
        {
            results = true;
        }
        else
        {
            return false;
        }
    }
    return results;
}

/*
 * \brief DEBUG FUNCTION used to test the class
 */
void SearchFile::toString()
{
    std::cout << "FILES :" << std::endl;
    for (std::vector<NTOF_FILE>::iterator it = files_.begin();
         it != files_.end(); ++it)
    {
        std::cout << (*it).filename;
        std::cout << " [" << (*it).runNumber << "]";
        std::cout << " [" << (*it).extensionNumber << "]";
        std::cout << " [" << (*it).streamNumber << "]";
        std::cout << std::endl;
    }
    std::cout << std::endl;
}
} // namespace eventDisplay
} // namespace ntof
