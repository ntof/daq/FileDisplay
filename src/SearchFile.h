#ifndef SEARCHFILE_H
#define SEARCHFILE_H

#include <iostream>
#include <string>
#include <vector>

#include <stdint.h>

#include "XRootBrowser.hpp"

namespace ntof {
namespace eventDisplay {

struct NTOF_FILE
{
    NTOF_FILE() :
        filepath(""),
        filename(""),
        runNumber(0),
        extensionNumber(0),
        streamNumber(0)
    {}

    std::string filepath;
    std::string filename;
    int32_t runNumber;
    int32_t extensionNumber;
    int32_t streamNumber;
};

class MainWindow;

class SearchFile
{
public:
    /*
     * \brief Constructor of the class
     */
    SearchFile();

    /*
     * \brief Fill the list of files found at a particular path
     * \param path: Localization where the files are searched
     */
    void listFiles(const std::string &path);

    /*
     * \brief Find the full path of the next file on local storage
     * searched in the Default Workspace \param filename: actual
     * filename \return Return the full path of the next file
     */
    std::string getNextLocalFile(const std::string &path,
                                 const std::string &filename);

    /*
     * \brief Find the full path of the previous file on local storage
     * searched in the Default Workspace \param filename: actual
     * filename \return Return the full path of the previous file
     */
    std::string getPreviousLocalFile(const std::string &path,
                                     const std::string &filename);

    /*
     * \brief Find the full path of the first file related to a stream and a run
     * number on local storage \param streamID: ID of the stream \param
     * runNumber: Run number \return Return the full path of the file or an
     * empty string
     */
    std::string getFirstLocalFile(int32_t streamID, int32_t runNumber);

    /*
     * \brief Allow to know if there is a next file in the list
     * \return Return true if a next file has been found
     */
    bool isNextLocalFile();

    /*
     * \brief Allow to know if there is a previous file in the list
     * \return Return true if a previous file has been found
     */
    bool isPreviousLocalFile();

    /*
     * \brief Find the full path of the next file on CASTOR/CTA
     * \param xRootPath: actual xRootPath
     * \param filename: actual filename
     * \return Return the full path of the next file
     */
    std::string getNextCastorFile(const std::string &xRootPath,
                                  const std::string &filename);

    /*
     * \brief Find the full path of the previous file on CASTOR/CTA
     * \param xrootPath: actual xrootPath
     * \param filename: actual filename
     * \return Return the full path of the previous file
     */
    std::string getPreviousCastorFile(const std::string &xRootPath,
                                      const std::string &filename);

    /*
     * \brief Allow to know if there is a next file in the list
     * \return Return true if a next file has been found
     */
    bool isNextCastorFile();

    /*
     * \brief Allow to know if there is a previous file in the list
     * \return Return true if a previous file has been found
     */
    bool isPreviousCastorFile();

    /*
     * \brief DEBUG FUNCTION used to test the class
     */
    void toString();

    /*
     * \brief Convert a file to a structure to reach easily the event number,
     * run number, ... \param filepath: full path of the file \return Return the
     * structure completed
     */
    NTOF_FILE toNtofFile(const std::string &filepath);

private:
    std::vector<NTOF_FILE> files_; //!< List of the files found
    XRootBrowser browser_;         //!< Object used to communicate with CASTOR
};
} // namespace eventDisplay
} // namespace ntof
#endif // SEARCHFILE_H
