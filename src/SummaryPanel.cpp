#include "SummaryPanel.h"

#include <iostream>

#include <QDialogButtonBox>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include "CustomPlotZoom.h"
#include "FileReader.h"
#include "MainWindow.h"

namespace ntof {
namespace eventDisplay {
/*
 * \brief Constructor of the class
 */
SummaryPanel::SummaryPanel()
{
    initPanel();
    initTree();
}

/*
 * \brief Destructor of the class
 */
SummaryPanel::~SummaryPanel() {}

/*
 * \brief Create the content of the panel
 */
void SummaryPanel::initPanel()
{
    // FILES
    QGroupBox *filesGroupBox = new QGroupBox();
    filesTree_ = new QTreeWidget();
    filesTree_->setHeaderLabel(tr("Summary"));
    QHBoxLayout *filesLayout = new QHBoxLayout();
    filesLayout->addWidget(filesTree_);
    filesGroupBox->setLayout(filesLayout);
    filesGroupBox->setContentsMargins(0, 0, 0, 0);

    // VALIDATION PART
    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(valid()));

    // CREATION OF THE PANEL
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(filesGroupBox);
    mainLayout->addWidget(buttonBox);
    setLayout(mainLayout);
    resize(1000, 700);
}

/*
 * \brief Fill the content of the tree with the list of the open files
 */
void SummaryPanel::initTree()
{
    this->setCursor(Qt::WaitCursor);
    MainWindow::getInstance()->setDebugMessage("List the open files");

    // Remove the previous list
    filesTree_->clear();

    // Define the full path as the parent element
    QTreeWidgetItem *fullPath = NULL;
    std::string prevFullPath = "";
    // Fill the content of the tree
    std::vector<ntof::lib::EVENT_INFO> events =
        FileReader::getInstance()->getNtofLib().getListOfEvents();
    for (std::vector<ntof::lib::EVENT_INFO>::iterator eventIt = events.begin();
         eventIt != events.end(); ++eventIt)
    {
        ntof::lib::FILE_INFO fileInfo =
            FileReader::getInstance()->getNtofLib().getFileInfo((*eventIt).file);

        // Define the full path as the parent element
        if (prevFullPath != fileInfo.fullpath)
        {
            prevFullPath = fileInfo.fullpath;
            fullPath = new QTreeWidgetItem(filesTree_);
            fullPath->setText(0, tr((fileInfo.fullpath).c_str()));
        }

        if (fullPath != NULL)
        {
            QTreeWidgetItem *event = new QTreeWidgetItem(fullPath);
            std::ostringstream eventStr;
            eventStr << "Trigger " << (*eventIt).validatedNumber;
            event->setText(0, tr(eventStr.str().c_str()));

            QTreeWidgetItem *acqcCount = new QTreeWidgetItem(event);
            std::ostringstream acqcCountStr;
            acqcCountStr << (*eventIt).acqcCount << " detector(s) found";
            acqcCount->setText(0, tr(acqcCountStr.str().c_str()));
        }
    }

    // Open all the tree
    // filesTree_->expandAll();

    this->setCursor(Qt::ArrowCursor);
}
/* ******************************************************* */
/* SLOTS                                                   */
/* ******************************************************* */
/*
 * \brief Apply the modifications and close the panel
 */
void SummaryPanel::valid()
{
    setVisible(false);
}

/*
 * \brief Fill the content and open the panel
 */
void SummaryPanel::open()
{
    initTree();
    this->setVisible(true);
}
} // namespace eventDisplay
} // namespace ntof
