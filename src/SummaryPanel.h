#ifndef SUMMARYPANEL_H
#define SUMMARYPANEL_H

#include <QDialog>
#include <QDialogButtonBox>
#include <QObject>
#include <QTreeWidget>

namespace ntof {
namespace eventDisplay {
class MainWindow;

class SummaryPanel : public QDialog
{
    Q_OBJECT
public:
    /*
     * \brief Constructor of the class
     */
    SummaryPanel();

    /*
     * \brief Destructor of the class
     */
    ~SummaryPanel();

    /*
     * \brief Fill the content and open the panel
     */
    void open();

private slots:
    /*
     * \brief Close the panel without apply the modifications
     */
    void valid();

private:
    /*
     * \brief Create the content of the panel
     */
    void initPanel();

    /*
     * \brief Fill the content of the tree with the list of the open files
     */
    void initTree();

    QTreeWidget *filesTree_; //!< Display the list of the open files
};
} // namespace eventDisplay
} // namespace ntof
#endif // SUMMARYPANEL_H
