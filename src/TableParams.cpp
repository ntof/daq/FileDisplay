#include "TableParams.h"

#include <iostream>

#include <QHeaderView>
#include <QString>
#include <QStringList>

#include "Channel.h"
#include "MainWindow.h"

namespace ntof {
namespace eventDisplay {
/*
 * \brief Constructor of the class
 */
TableParams::TableParams() : count_(0)
{
    initTable();
}

/*
 * \brief Create the group box and the empty table
 */
void TableParams::initTable()
{
    setColumnCount(20);
    tableHeaders_ << "DAQ ID";
    tableHeaders_ << "Card ID";
    tableHeaders_ << "Card Type";
    tableHeaders_ << "Time window (ms)";
    tableHeaders_ << "Sample rate (MS/s)";
    tableHeaders_ << "Sample size (KS)";
    tableHeaders_ << "Channel ID";
    tableHeaders_ << "Detector name";
    tableHeaders_ << "Detector ID";
    tableHeaders_ << "Delay (ns)";
    tableHeaders_ << "FullScale (V)";
    tableHeaders_ << "Lower limit (mV)";
    tableHeaders_ << "Offset (mV)";
    tableHeaders_ << "Threshold sign";
    tableHeaders_ << "Zero suppression type";
    tableHeaders_ << "Master group";
    tableHeaders_ << "Threshold (mV)";
    tableHeaders_ << "Zero suppression start (ns)";
    tableHeaders_ << "PreSamples (S)";
    tableHeaders_ << "PostSamples (S)";
    setHorizontalHeaderLabels(tableHeaders_);
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    resizeColumnsToContents();
    horizontalHeader()->setStretchLastSection(true);
}

/*
 * \brief Update the element and its children
 */
void TableParams::updateElement()
{
    if (count_ == 0)
    {
        MainWindow::getInstance()->getParamsGroupBox()->setVisible(false);
    }
    else
    {
        MainWindow::getInstance()->getParamsGroupBox()->setVisible(true);
        resizeColumnsToContents();
        horizontalHeader()->setStretchLastSection(true);
    }
}

/*
 * \brief Empty the table
 */
void TableParams::clear()
{
    count_ = 0;
    MainWindow::getInstance()->getParamsGroupBox()->setVisible(false);
}

/*
 * \brief Add a new row to the table
 * \param channel: source of data to fill the row
 */
void TableParams::addRow(Channel *channel)
{
    MainWindow::getInstance()->addActionOnGoing();
    MainWindow::getInstance()->setDebugMessage(
        "Add a new row to the parameter table");

    setRowCount(count_ + 1);

    setItem(count_, DAQ_ID,
            new QTableWidgetItem(QString::number(channel->getChassis())));
    setItem(count_, CARD_ID,
            new QTableWidgetItem(QString::number(channel->getModule())));
    setItem(count_, CARD_TYPE,
            new QTableWidgetItem(channel->getModuleType().c_str()));
    setItem(count_, TIME_WINDOW,
            new QTableWidgetItem(
                QString::number(round(channel->getTimeWindow() * 100) / 100)));
    setItem(count_, SAMPLE_RATE,
            new QTableWidgetItem(QString::number(channel->getSampleRate())));
    setItem(count_, SAMPLE_SIZE,
            new QTableWidgetItem(QString::number(channel->getSampleSize())));
    setItem(count_, CHANNEL_ID,
            new QTableWidgetItem(QString::number(channel->getChannel())));
    setItem(count_, DETECTOR_NAME,
            new QTableWidgetItem(channel->getDetectorType().c_str()));
    setItem(count_, DETECTOR_ID,
            new QTableWidgetItem(QString::number(channel->getDetectorId())));
    setItem(count_, DELAY_TIME,
            new QTableWidgetItem(QString::number(channel->getDelayTime())));
    setItem(count_, FULL_SCALE,
            new QTableWidgetItem(
                QString::number(channel->getFullScaleInMilliVolts() / 1000)));
    setItem(count_, LOWER_LIMIT,
            new QTableWidgetItem(
                QString::number(round(channel->getLowerLimit() * 100) / 100)));
    setItem(count_, OFFSET,
            new QTableWidgetItem(QString::number(channel->getOffset())));
    setItem(count_, ZERO_SUPPRESSION_MODE,
            new QTableWidgetItem(channel->getZeroSuppression().c_str()));
    setItem(count_, THRESHOLD_SIGN,
            new QTableWidgetItem(QString::number(channel->getThresholdSign())));
    setItem(count_, THRESHOLD,
            new QTableWidgetItem(
                QString::number(round(channel->getThreshold() * 100) / 100)));
    setItem(count_, ZERO_SUPPRESSION_START,
            new QTableWidgetItem(
                QString::number(channel->getZeroSuppressionStart())));
    setItem(count_, PRE_SAMPLES,
            new QTableWidgetItem(QString::number(channel->getPreSamples())));
    setItem(count_, POST_SAMPLES,
            new QTableWidgetItem(QString::number(channel->getPostSamples())));
    setItem(count_, MASTER_GROUP,
            new QTableWidgetItem(channel->getMasterGroup().c_str()));

    ++count_;
    MainWindow::getInstance()->removeActionOnGoing();
}
} // namespace eventDisplay
} // namespace ntof
