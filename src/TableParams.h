#ifndef TABLEPARAMS_H
#define TABLEPARAMS_H

#include "QTableWidget"

namespace ntof {
namespace eventDisplay {
class Channel;
class MainWindow;

enum
{
    DAQ_ID = 0,
    CARD_ID,
    CARD_TYPE,
    TIME_WINDOW,
    SAMPLE_RATE,
    SAMPLE_SIZE,
    CHANNEL_ID,
    DETECTOR_NAME,
    DETECTOR_ID,
    DELAY_TIME,
    FULL_SCALE,
    LOWER_LIMIT,
    OFFSET,
    THRESHOLD_SIGN,
    ZERO_SUPPRESSION_MODE,
    MASTER_GROUP,
    THRESHOLD,
    ZERO_SUPPRESSION_START,
    PRE_SAMPLES,
    POST_SAMPLES,
};

class TableParams : public QTableWidget
{
public:
    /*
     * \brief Constructor of the class
     */
    TableParams();

    /*
     * \brief Create the group box and the empty table
     */
    void initTable();

    /*
     * \brief Update the element and its children
     */
    void updateElement();

    /*
     * \brief Empty the table
     */
    void clear();

    /*
     * \brief Add a new row to the table
     * \param channel: source of data to fill the row
     */
    void addRow(Channel *channel);

private:
    QStringList tableHeaders_; //!< List of the headers of the table
    int count_;                //!< Number of row in the table
};
} // namespace eventDisplay
} // namespace ntof
#endif // TABLEPARAMS_H
