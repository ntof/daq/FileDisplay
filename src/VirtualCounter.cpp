/*
 * VirtualCounter.cpp
 *
 *  Created on: Jun 15, 2016
 *      Author: agiraud
 */

#include "VirtualCounter.h"

namespace ntof {
namespace eventDisplay {

/*
 * \brief Constructor of the class
 */
VirtualCounter::VirtualCounter() : actionsOnGoing_(0) {}

/*
 * \brief Destructor of the class
 */
VirtualCounter::~VirtualCounter() {}

/* ************************************************************* */
/* ACTIONS COUNTER                                               */
/* ************************************************************* */
/*
 * \brief Add an element to the count of the actions on going
 */
void VirtualCounter::addActionOnGoing()
{
    ++actionsOnGoing_;
    if (actionsOnGoing_ > 0)
    {
        this->setCursor(Qt::WaitCursor);
    }
}

/*
 * \brief Remove an element to the count of the actions on going
 */
void VirtualCounter::removeActionOnGoing()
{
    --actionsOnGoing_;
    if (actionsOnGoing_ <= 0)
    {
        this->setCursor(Qt::ArrowCursor);
        // setDebugMessage("");
    }
}
} /* namespace eventDisplay */
} /* namespace ntof */
