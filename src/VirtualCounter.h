/*
 * VirtualCounter.h
 *
 *  Created on: Jun 15, 2016
 *      Author: agiraud
 */

#ifndef VIRTUALCOUNTER_H_
#define VIRTUALCOUNTER_H_

#include <QDialog>

#include <unistd.h>

namespace ntof {
namespace eventDisplay {

class VirtualCounter : public QDialog
{
public:
    /*
     * \brief Constructor of the class
     */
    VirtualCounter();

    /*
     * \brief Destructor of the class
     */
    virtual ~VirtualCounter();

    /* ************************************************************* */
    /* ACTIONS COUNTER                                               */
    /* ************************************************************* */
    /*
     * \brief Add an element to the count of the actions on going
     */
    void addActionOnGoing();

    /*
     * \brief Remove an element to the count of the actions on going
     */
    void removeActionOnGoing();

private:
    int32_t actionsOnGoing_; //!< Count of the actions on going
};

} /* namespace eventDisplay */
} /* namespace ntof */

#endif /* VIRTUALCOUNTER_H_ */
