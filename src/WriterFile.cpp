#include "WriterFile.h"

#include <algorithm>
#include <cmath>
#include <sstream>

#include <QMessageBox>

#include "Channel.h"
#include "CustomPlotZoom.h"
#include "FileDisplayException.h"
#include "FileReader.h"
#include "MainWindow.h"
#include "NtofLib.h"
#include "NtofLibException.h"
#include "qcustomplot.h"

using namespace ntof::eventDisplay;

static std::string getChannelDetectorTypeID(Channel *channel)
{
    std::stringstream ss;
    ss << channel->getDetectorType() << "/" << channel->getDetectorId();
    return ss.str();
}

static double getNextValue(GraphDataContainer::const_iterator &currentIter,
                           const GraphDataContainer::const_iterator &endIter,
                           ssize_t key)
{
    for (; currentIter != endIter; ++currentIter)
    {
        if (currentIter->key >= key)
            break;
    }
    if ((currentIter == endIter) || ((ssize_t) currentIter->key != key))
        return std::nan("no value");
    else
        return currentIter->value;
}

namespace ntof {
namespace eventDisplay {
/*
 * \brief Constructor of the class
 * \param filename: Full path of the output file
 * \param filetype: Enum used to select the file to create
 */
WriterFile::WriterFile(std::string filename, int32_t filetype) : file_(NULL)
{
    if (FileReader::getInstance()->getNtofLib().getFilename() == "")
    {
        std::string message = "No file opened";
        QMessageBox::critical(MainWindow::getInstance(), QObject::tr("Error"),
                              QObject::tr(message.c_str()), QMessageBox::Ok);
    }
    else
    {
        // change the cursor shape
        MainWindow::getInstance()->setCursor(Qt::WaitCursor);
        file_.open(filename.c_str());
        if (file_)
        {
            // Write the file
            switch (filetype)
            {
            case DEBUG:
                MainWindow::getInstance()->setDebugMessage(
                    "Write a debug file");
                writeDebugFile();
                break;
            case DATA:
                MainWindow::getInstance()->setDebugMessage("Write a text file");
                writeDataFile();
                break;
            default: break;
            }

            // Close the file
            file_.close();

            MainWindow::getInstance()->setDebugMessage("");
        }
        MainWindow::getInstance()->setCursor(Qt::ArrowCursor);
    }
}

/*
 * \brief Write the RCTR
 */
void WriterFile::readRCTR()
{
    ntof::lib::NtofLib &ntofLib = FileReader::getInstance()->getNtofLib();
    if (!ntofLib.isDefinedRCTR())
        return;

    ntof::lib::ReaderStructRCTR *rctr = ntofLib.getRCTR();
    file_ << " ******  RCTR  ****** " << std::endl;
    file_ << "TITLE = " << rctr->getTitle() << std::endl;
    file_ << "REVISION NUMBER = " << rctr->getRevisionNumber() << std::endl;
    file_ << "RESERVED = " << rctr->getReserved() << std::endl;
    file_ << "LENGTH = " << rctr->getLength() << std::endl;
    file_ << "Run number = " << rctr->getRunNumber() << std::endl;
    file_ << "Extension number = " << rctr->getExtensionNumber() << std::endl;
    file_ << "Stream id = " << rctr->getStreamNumber() << std::endl;
    file_ << "Experiment id = " << rctr->getExperiment() << std::endl;
    file_ << "Date of start = " << rctr->getDate() << std::endl;
    file_ << "Time of start = " << rctr->getTime() << std::endl;
    int32_t nbModules = rctr->getNumberOfModules();
    file_ << "Number of modules = " << nbModules << std::endl;
    file_ << "Number of channels = " << rctr->getNumberOfChannels()
          << std::endl;
    uint32_t numberOfStreams = rctr->getTotalNumberOfStreams();
    file_ << "Number of streams = " << numberOfStreams << std::endl;
    file_ << "Number of chassis = " << rctr->getTotalNumberOfChassis()
          << std::endl;
    file_ << "Number of modules = " << rctr->getTotalNumberOfModules()
          << std::endl;
    int32_t moduleID = 0;
    for (const auto &module : rctr->getNumberOfUsedChannelByModule())
    {
        file_ << "Nb channel in the module " << moduleID << " = " << module
              << std::endl;
        ++moduleID;
    }
    file_ << std::endl;
}

/*
 * \brief Write the MODH
 */
void WriterFile::readMODH()
{
    ntof::lib::NtofLib &ntofLib = FileReader::getInstance()->getNtofLib();
    if (!ntofLib.isDefinedMODH())
        return;

    ntof::lib::ReaderStructMODH *modh = ntofLib.getMODH();
    file_ << " ******  MODH  ****** " << std::endl;
    file_ << "TITLE = " << modh->getTitle() << std::endl;
    file_ << "REVISION NUMBER = " << modh->getRevisionNumber() << std::endl;
    file_ << "RESERVED = " << modh->getReserved() << std::endl;
    file_ << "LENGTH = " << modh->getLength() << std::endl;
    file_ << "Number of channel = " << modh->getNbChannels() << std::endl;
    modh->setIndex(0);
    do
    {
        file_ << "*** " << modh->getIndex() << " ***" << std::endl;
        file_ << "    Detector type = " << modh->getDetectorType() << std::endl;
        file_ << "    Detector id = " << modh->getDetectorId() << std::endl;
        file_ << "    Module type = " << modh->getModuleType() << std::endl;
        file_ << "    Channel id = " << modh->getChannel() << std::endl;
        file_ << "    Module id = " << modh->getModule() << std::endl;
        file_ << "    Chassis id = " << modh->getChassis() << std::endl;
        file_ << "    Stream id = " << modh->getStream() << std::endl;
        file_ << "    Sample Rate = " << modh->getSampleRate() << std::endl;
        file_ << "    Sample size = " << modh->getSampleSize() << std::endl;
        file_ << "    Full scale = " << modh->getFullScale() << std::endl;
        file_ << "    Full scale in mV = " << modh->getFullScaleInMilliVolts()
              << std::endl;
        file_ << "    Delay time = " << modh->getDelayTime() << std::endl;
        file_ << "    Threshold ADC = " << modh->getThresholdADC() << std::endl;
        file_ << "    Threshold in mV = " << modh->getThresholdMilliVolts()
              << std::endl;
        file_ << "    Threshold sign = " << modh->getThresholdSign()
              << std::endl;
        file_ << "    Offset = " << modh->getOffset() << std::endl;
        file_ << "    Presample = " << modh->getPreSample() << std::endl;
        file_ << "    Postsample = " << modh->getPostSample() << std::endl;
        file_ << "    Clock state = " << modh->getClockState() << std::endl;
        try
        {
            file_ << "    Zero suppression start = "
                  << modh->getZeroSuppressionStart() << std::endl;
        }
        catch (ntof::lib::NtofLibException &ex)
        {
            // std::cerr << ex.what() << std::endl;
        }

        try
        {
            file_ << "    Master detector type = "
                  << modh->getMasterDetectorType() << std::endl;
            file_ << "    Master detector id = " << modh->getMasterDetectorId()
                  << std::endl;
        }
        catch (ntof::lib::NtofLibException &ex)
        {
            // std::cerr << ex.what() << std::endl;
        }

        file_ << std::endl;
    } while (modh->getNextChannel());
}

/*
 * \brief Write the EVEH
 * \return Return the revision number of the EVEH
 */
int32_t WriterFile::readEVEH()
{
    int32_t revisionNumber = -1;
    ntof::lib::NtofLib &ntofLib = FileReader::getInstance()->getNtofLib();
    if (!ntofLib.isDefinedEVEH())
        return revisionNumber;

    ntof::lib::ReaderStructEVEH *eveh = ntofLib.getEVEH();
    file_ << " ******  EVEH  ****** " << std::endl;
    file_ << "POSITION = " << eveh->getCurrentSeek() << std::endl;
    file_ << "TITLE = " << eveh->getTitle() << std::endl;
    revisionNumber = eveh->getRevisionNumber();
    file_ << "REVISION NUMBER = " << revisionNumber << std::endl;
    file_ << "RESERVED = " << eveh->getReserved() << std::endl;
    file_ << "LENGTH = " << eveh->getLength() << std::endl;
    file_ << "Size of event = " << eveh->getSizeOfEvent() << std::endl;
    file_ << "Event number = " << eveh->getEventNumber() << std::endl;
    file_ << "Run number = " << eveh->getRunNumber() << std::endl;
    file_ << "Event time = " << eveh->getTime() << std::endl;
    file_ << "Event date = " << eveh->getDate() << std::endl;
    if (revisionNumber >= 1)
    {
        file_ << "Computer time stamp = " << eveh->getComputerTimestamp()
              << std::endl;
        file_ << "BCT readout time stamp = " << eveh->getBCTTimestamp()
              << std::endl;
        file_ << "BCT.468 Beam intensity = " << eveh->getBeamIntensity()
              << std::endl;
        file_ << "Beam type (as int) = " << eveh->getBeamType() << std::endl;
        file_ << "Beam type (as string) = " << eveh->getBeamTypeName()
              << std::endl;
    }
    file_ << std::endl;

    return revisionNumber;
}

/*
 * \brief Write the INFO
 */
void WriterFile::readINFO()
{
    ntof::lib::NtofLib &ntofLib = FileReader::getInstance()->getNtofLib();
    if (!ntofLib.isDefinedINFO())
        return;

    ntof::lib::ReaderStructINFO *info = ntofLib.getINFO();
    try
    {
        file_ << " ******  INFO  ****** " << std::endl;
        file_ << "TITLE = " << info->getTitle() << std::endl;
        file_ << "REVISION NUMBER = " << info->getRevisionNumber() << std::endl;
        file_ << "RESERVED = " << info->getReserved() << std::endl;
        file_ << "LENGTH = " << info->getLength() << std::endl;
        file_ << "Data = " << info->getData() << std::endl;
        file_ << std::endl;
    }
    catch (ntof::lib::NtofLibException &ex)
    {
        file_ << "no data" << std::endl;
        file_ << std::endl;
    }
}

/*
 * \brief Write the INDX
 */
void WriterFile::readINDX()
{
    ntof::lib::NtofLib &ntofLib = FileReader::getInstance()->getNtofLib();
    if (!ntofLib.isDefinedINDX())
        return;

    ntof::lib::ReaderStructINDX *indx = ntofLib.getINDX();
    try
    {
        file_ << " ******  INDX  ****** " << std::endl;
        file_ << "TITLE = " << indx->getTitle() << std::endl;
        uint32_t revisionNumber = indx->getRevisionNumber();
        file_ << "REVISION NUMBER = " << revisionNumber << std::endl;
        file_ << "RESERVED = " << indx->getReserved() << std::endl;
        file_ << "LENGTH = " << indx->getLength() << std::endl;
        if (revisionNumber == 0)
        {
            std::list<int32_t> list = indx->getIndexList();
            for (const auto &index : indx->getIndexList())
            {
                file_ << "Position = " << index << std::endl;
            }
        }
        else if (revisionNumber == 1)
        {
            std::vector<ntof::lib::INDEX_FIELD> list = indx->getIndexFields();
            int32_t index = 0;
            for (const auto &indexField : indx->getIndexFields())
            {
                file_ << "   *** " << index << " ***" << std::endl;
                file_ << "   Stream number = " << indexField.streamNumber
                      << std::endl;
                file_ << "   Trigger number = " << indexField.triggerNumber
                      << std::endl;
                file_ << "   Validated number = " << indexField.validatedNumber
                      << std::endl;
                file_ << "   Segment number = " << indexField.segmentNumber
                      << std::endl;
                file_ << "   Offset = " << indexField.offset << std::endl;
                ++index;
            }
        }
        file_ << std::endl;
    }
    catch (ntof::lib::NtofLibException &ex)
    {
        file_ << "no data" << std::endl;
        file_ << std::endl;
    }
}

/*
 * \brief Write the BEAM
 */
void WriterFile::readBEAM()
{
    ntof::lib::NtofLib &ntofLib = FileReader::getInstance()->getNtofLib();
    if (!ntofLib.isDefinedBEAM())
        return;

    ntof::lib::ReaderStructBEAM *beam = ntofLib.getBEAM();
    try
    {
        file_ << " ******  BEAM  ****** " << std::endl;
        file_ << "TITLE = " << beam->getTitle() << std::endl;
        file_ << "REVISION NUMBER = " << beam->getRevisionNumber() << std::endl;
        file_ << "RESERVED = " << beam->getReserved() << std::endl;
        file_ << "LENGTH = " << beam->getLength() << std::endl;
        file_ << "Timestamp = " << beam->getTimestamp() << std::endl;
        file_ << "Intensity = " << beam->getIntensity() << std::endl;
        file_ << "Type = " << beam->getType() << std::endl;
        std::string typeName = beam->getTypeName();
        file_ << "Type (as string) = " << typeName << std::endl;
        file_ << "Type (as int) = " << beam->typeToInt(typeName) << std::endl;
        file_ << std::endl;
    }
    catch (ntof::lib::NtofLibException &ex)
    {
        file_ << "no data" << std::endl;
        file_ << std::endl;
    }
}

/*
 * \brief Write the SLOW
 */
void WriterFile::readSLOW()
{
    ntof::lib::NtofLib &ntofLib = FileReader::getInstance()->getNtofLib();

    if (!ntofLib.isDefinedSLOW())
        return;

    ntof::lib::ReaderStructSLOW *slow = ntofLib.getSLOW();
    try
    {
        file_ << " ******  SLOW  ****** " << std::endl;
        file_ << "TITLE = " << slow->getTitle() << std::endl;
        file_ << "REVISION NUMBER = " << slow->getRevisionNumber() << std::endl;
        file_ << "RESERVED = " << slow->getReserved() << std::endl;
        file_ << "LENGTH = " << slow->getLength() << std::endl;
        file_ << "CT1= " << slow->getCT1() << std::endl;
        file_ << "CT2= " << slow->getCT2() << std::endl;
        file_ << "CT3= " << slow->getCT3() << std::endl;
        file_ << "CT4= " << slow->getCT4() << std::endl;
        file_ << "CT5= " << slow->getCT5() << std::endl;
        file_ << "FT1= " << slow->getFT1() << std::endl;
        file_ << "FT2= " << slow->getFT2() << std::endl;
        file_ << "O2T1= " << slow->getO2T1() << std::endl;
        file_ << "O2T2= " << slow->getO2T2() << std::endl;
        file_ << "PT1= " << slow->getPT1() << std::endl;
        file_ << "PT2= " << slow->getPT2() << std::endl;
        file_ << "PT3= " << slow->getPT3() << std::endl;
        file_ << "PT4= " << slow->getPT4() << std::endl;
        file_ << "PT5= " << slow->getPT5() << std::endl;
        file_ << "PT6= " << slow->getPT6() << std::endl;
        file_ << "PT7= " << slow->getPT7() << std::endl;
        file_ << "PH1= " << slow->getPH1() << std::endl;
        file_ << "PH2= " << slow->getPH2() << std::endl;
        file_ << "PH3= " << slow->getPH3() << std::endl;
        file_ << "PH4= " << slow->getPH4() << std::endl;
        file_ << "TT2= " << slow->getTT2() << std::endl;
        file_ << "TT3= " << slow->getTT3() << std::endl;
        file_ << "TT4= " << slow->getTT4() << std::endl;
        file_ << "TT5= " << slow->getTT5() << std::endl;
        file_ << "TT6= " << slow->getTT6() << std::endl;
        file_ << "TT7= " << slow->getTT7() << std::endl;
        file_ << "TT8= " << slow->getTT8() << std::endl;
        file_ << "TT9= " << slow->getTT9() << std::endl;
        file_ << "TT10= " << slow->getTT10() << std::endl;
        file_ << "PAXTOF01= " << slow->getPAXTOF01() << std::endl;
        file_ << "PAXTOF02= " << slow->getPAXTOF02() << std::endl;
        file_ << "PAXTOF03= " << slow->getPAXTOF03() << std::endl;
        file_ << "PAXTOF04= " << slow->getPAXTOF04() << std::endl;
        file_ << "PAXTOF05= " << slow->getPAXTOF05() << std::endl;
        file_ << "PMIBL01= " << slow->getPMIBL01() << std::endl;
        file_ << "PMIBL02= " << slow->getPMIBL02() << std::endl;
        file_ << "PMITOF01= " << slow->getPMITOF01() << std::endl;
        file_ << "PMITOF02= " << slow->getPMITOF02() << std::endl;
        file_ << "PMITOF03= " << slow->getPMITOF03() << std::endl;
        file_ << "PMITOF04= " << slow->getPMITOF04() << std::endl;
        file_ << "PMITOF05= " << slow->getPMITOF05() << std::endl;
        file_ << "MagnetIntensity= " << slow->getMagnetIntensity() << std::endl;
        file_ << "MagnetStatus= " << slow->getMagnetStatus() << std::endl;
        file_ << "VAC_TOF_VGR_1_PR= " << slow->getVAC_TOF_VGR_1_PR()
              << std::endl;
        file_ << "VAC_TOF_VGR_1_Stat= " << slow->getVAC_TOF_VGR_1_Stat()
              << std::endl;
        file_ << "VAC_TOF_VGR_1A_PR= " << slow->getVAC_TOF_VGR_1A_PR()
              << std::endl;
        file_ << "VAC_TOF_VGR_1A_Stat= " << slow->getVAC_TOF_VGR_1A_Stat()
              << std::endl;
        file_ << "VAC_TOF_VGR_2_PR= " << slow->getVAC_TOF_VGR_2_PR()
              << std::endl;
        file_ << "VAC_TOF_VGR_2_Stat= " << slow->getVAC_TOF_VGR_2_Stat()
              << std::endl;
        file_ << "VAC_TOF_VGR_2A_PR= " << slow->getVAC_TOF_VGR_2A_PR()
              << std::endl;
        file_ << "VAC_TOF_VGR_2A_Stat= " << slow->getVAC_TOF_VGR_2A_Stat()
              << std::endl;
        file_ << "VAC_TOF_VGR_3_PR= " << slow->getVAC_TOF_VGR_3_PR()
              << std::endl;
        file_ << "VAC_TOF_VGR_3_Stat= " << slow->getVAC_TOF_VGR_3_Stat()
              << std::endl;
        file_ << "VAC_TOF_VGR_3A_PR= " << slow->getVAC_TOF_VGR_3A_PR()
              << std::endl;
        file_ << "VAC_TOF_VGR_3A_Stat= " << slow->getVAC_TOF_VGR_3A_Stat()
              << std::endl;
        file_ << "VAC_TOF_VGR_4_PR= " << slow->getVAC_TOF_VGR_4_PR()
              << std::endl;
        file_ << "VAC_TOF_VGR_4_Stat= " << slow->getVAC_TOF_VGR_4_Stat()
              << std::endl;
        file_ << "VAC_TOF_VGR_4A_PR= " << slow->getVAC_TOF_VGR_4A_PR()
              << std::endl;
        file_ << "VAC_TOF_VGR_4A_Stat= " << slow->getVAC_TOF_VGR_4A_Stat()
              << std::endl;
        file_ << "VAC_TOF_EXP_PR= " << slow->getVAC_TOF_EXP_PR() << std::endl;
        file_ << "VAC_TOF_EXP_Stat= " << slow->getVAC_TOF_EXP_Stat()
              << std::endl;
        file_ << "VAC_TOF_EXP_A_PR= " << slow->getVAC_TOF_EXP_A_PR()
              << std::endl;
        file_ << "VAC_TOF_EXP_A_Stat= " << slow->getVAC_TOF_EXP_A_Stat()
              << std::endl;
        file_ << "VAC_TOF_EXP_B_PR= " << slow->getVAC_TOF_EXP_B_PR()
              << std::endl;
        file_ << "VAC_TOF_EXP_B_Stat= " << slow->getVAC_TOF_EXP_B_Stat()
              << std::endl;
        file_ << "VAC_TOFVVS01_Stat= " << slow->getVAC_TOFVVS01_Stat()
              << std::endl;
        file_ << "VAC_TOFVVS02_Stat= " << slow->getVAC_TOFVVS02_Stat()
              << std::endl;
        file_ << "VAC_TOFVVS01= " << slow->getVAC_TOFVVS01() << std::endl;
        file_ << "VAC_TOFVVS02= " << slow->getVAC_TOFVVS02() << std::endl;
        file_ << "VAC_TOF2VVS01= " << slow->getVAC_TOF2VVS01() << std::endl;
        file_ << "VAC_TOF2_VGR_1_PR= " << slow->getVAC_TOF2_VGR_1_PR()
              << std::endl;
        file_ << "VAC_TOF2_VGR_2_PR= " << slow->getVAC_TOF2_VGR_2_PR()
              << std::endl;
        file_ << "VAC_TOF2_VGR_3_PR= " << slow->getVAC_TOF2_VGR_3_PR()
              << std::endl;
        file_ << "VAC_TOF2_VGR_4_PR= " << slow->getVAC_TOF2_VGR_4_PR()
              << std::endl;
        file_ << "ExpTemp_EAR1_IN= " << slow->getExpTemp_EAR1_IN() << std::endl;
        file_ << "ExpTemp_EAR1_OUT= " << slow->getExpTemp_EAR1_OUT()
              << std::endl;
        file_ << "ExpTemp_EAR2_IN= " << slow->getExpTemp_EAR2_IN() << std::endl;
        file_ << "ExpTemp_EAR2_OUT= " << slow->getExpTemp_EAR2_OUT()
              << std::endl;
        file_ << std::endl;
    }
    catch (ntof::lib::NtofLibException &ex)
    {
        file_ << "no data" << std::endl;
        file_ << std::endl;
    }
}

/*
 * \brief Write the ADDH
 */
void WriterFile::readADDH()
{
    ntof::lib::NtofLib &ntofLib = FileReader::getInstance()->getNtofLib();
    if (!ntofLib.isDefinedADDH())
        return;

    ntof::lib::ReaderStructADDH *addh = ntofLib.getADDH();
    try
    {
        file_ << " ******  ADDH  ****** " << std::endl;
        file_ << "TITLE = " << addh->getTitle() << std::endl;
        file_ << "REVISION NUMBER = " << addh->getRevisionNumber() << std::endl;
        file_ << "RESERVED = " << addh->getReserved() << std::endl;
        file_ << "LENGTH = " << addh->getLength() << std::endl;
        int32_t index = 0;
        while (addh->getNextField())
        {
            file_ << "    *** ADDH field " << index << " ***" << std::endl;
            file_ << "   Name = " << addh->getName() << std::endl;
            int32_t type = addh->getType();
            file_ << "   Type = " << type << std::endl;
            switch (type)
            {
            case ntof::lib::TypeString:
                file_ << "   Value = " << addh->getDataAsString() << std::endl;
                break;
            case ntof::lib::TypeInt:
                file_ << "   Value = " << addh->getDataAsInt32() << std::endl;
                break;
            case ntof::lib::TypeLong:
                file_ << "   Value = " << addh->getDataAsInt64() << std::endl;
                break;
            case ntof::lib::TypeFloat:
                file_ << "   Value = " << addh->getDataAsFloat() << std::endl;
                break;
            case ntof::lib::TypeDouble:
                file_ << "   Value = " << addh->getDataAsDouble() << std::endl;
                break;
            default: file_ << "   [ERROR] Type undefined" << std::endl; break;
            }
            ++index;
        }
        file_ << std::endl;
    }
    catch (ntof::lib::NtofLibException &ex)
    {
        file_ << "no data" << std::endl;
        file_ << std::endl;
    }
}

/*
 * \brief Write the configuration from the MODH initialized through the ACQC
 */
void WriterFile::readConfiguration()
{
    ntof::lib::NtofLib &ntofLib = FileReader::getInstance()->getNtofLib();
    // Set the MODH to focus on this channel params
    if (!ntofLib.getACQC()->setChannelConfiguration())
    {
        std::cerr << "Error to set the channel configuration." << std::endl;
    }
    else
    {
        ntof::lib::ReaderStructMODH *modh = ntofLib.getMODH();
        file_ << "*** channel configuration ***" << std::endl;
        file_ << "    Channel ID = " << modh->getIndex() << std::endl;
        file_ << "    Detector type = " << modh->getDetectorType() << std::endl;
        file_ << "    Detector id = " << modh->getDetectorId() << std::endl;
        file_ << "    Module type = " << modh->getModuleType() << std::endl;
        file_ << "    Channel id = " << modh->getChannel() << std::endl;
        file_ << "    Module id = " << modh->getModule() << std::endl;
        file_ << "    Chassis id = " << modh->getChassis() << std::endl;
        file_ << "    Stream id = " << modh->getStream() << std::endl;
        file_ << "    Sample Rate = " << modh->getSampleRate() << std::endl;
        file_ << "    Sample size = " << modh->getSampleSize() << std::endl;
        file_ << "    Full scale = " << modh->getFullScale() << std::endl;
        file_ << "    Full scale in mV = " << modh->getFullScaleInMilliVolts()
              << std::endl;
        file_ << "    Delay time = " << modh->getDelayTime() << std::endl;
        file_ << "    Threshold ADC = " << modh->getThresholdADC() << std::endl;
        file_ << "    Threshold in mV = " << modh->getThresholdMilliVolts()
              << std::endl;
        file_ << "    Threshold sign = " << modh->getThresholdSign()
              << std::endl;
        file_ << "    Offset = " << modh->getOffset() << std::endl;
        file_ << "    Presample = " << modh->getPreSample() << std::endl;
        file_ << "    Postsample = " << modh->getPostSample() << std::endl;
        file_ << "    clock state = " << modh->getClockState() << std::endl;
        try
        {
            file_ << "    Zero suppression start = "
                  << modh->getZeroSuppressionStart() << std::endl;
        }
        catch (ntof::lib::NtofLibException &ex)
        {
            // std::cerr << ex.what() << std::endl;
        }
        file_ << "    value max = " << modh->getMaxDataValue() << std::endl;
        file_ << "    value min = " << modh->getMinDataValue() << std::endl;
        file_ << std::endl;
    }
}

/*
 * \brief Write the ACQC
 */
void WriterFile::readACQC()
{
    ntof::lib::NtofLib &ntofLib = FileReader::getInstance()->getNtofLib();
    if (!ntofLib.isDefinedACQC())
        return;

    ntof::lib::ReaderStructACQC *acqc = ntofLib.getACQC();
    file_ << "******  ACQC  ****** " << std::endl;
    file_ << "POSITION = " << acqc->getCurrentSeek() << std::endl;
    file_ << "TITLE = " << acqc->getTitle() << std::endl;
    file_ << "REVISION NUMBER = " << acqc->getRevisionNumber() << std::endl;
    file_ << "RESERVED = " << acqc->getReserved() << std::endl;
    file_ << "LENGTH = " << acqc->getLength() << std::endl;
    file_ << "Detector type = " << acqc->getDetectorType() << std::endl;
    file_ << "Detector id = " << acqc->getDetectorId() << std::endl;
    file_ << "Channel id = " << acqc->getChannel() << std::endl;
    file_ << "Module id = " << acqc->getModule() << std::endl;
    file_ << "Chassis id = " << acqc->getChassis() << std::endl;
    file_ << "Stream id = " << acqc->getStream() << std::endl;
    file_ << std::endl;

    // Set the MODH to focus on this channel params
    readConfiguration();

    /*
     //Select the pulse by using the pulse id
     bool res = acqc->setPulseId(0);
     if (res){
     try{
     file_ << "*** Pulse ***" << std::endl;
     file_ << "    PULSE ID = " << acqc->getPulseId() << std::endl;
     file_ << "    TIMESTAMP = " << acqc->getPulseTimestamp() << std::endl;
     file_ << "    LENGTH = " << acqc->getPulseLength() << std::endl;

     file_ << "    DATA [] = " << std::endl;
     for (uint64_t i = 0; i < 20; ++i){
     file_ << ((*acqc)[i]) << " ; ";
     }
     file_ << std::endl;
     } catch (ntof::lib::NtofLibException& ex){
     std::cerr << ex.what() << std::endl;
     }
     } else{
     std::cerr << "The pulse id selected doesn't exist" << std::endl;
     }
     */

    // For each ACQC pulse in the file
    while (acqc->getNextPulse())
    {
        file_ << "*** Pulse ***" << std::endl;
        file_ << "    PULSE ID = " << acqc->getPulseId() << std::endl;
        file_ << "    TIMESTAMP = " << acqc->getPulseTimestamp() << std::endl;
        file_ << "    LENGTH = " << acqc->getPulseLength() << std::endl;

        file_ << "    DATA [] = " << std::endl;
        for (uint64_t i = 0; i < 20; ++i)
        {
            file_ << ((*acqc)[i]) << " ; ";
        }
        file_ << std::endl;
    } // End while Pulse
}

/*
 * \brief Try to write all the headers
 */
void WriterFile::readEverything()
{
    readRCTR();
    readMODH();
    readINDX();

    // Save the actual event number to reset it after
    int32_t eventId = 0;
    if (FileReader::getInstance()->getNtofLib().isDefinedEVEH())
    {
        eventId =
            FileReader::getInstance()->getNtofLib().getEVEH()->getEventNumber();
    }

    FileReader::getInstance()->getNtofLib().getEvent(0);
    // For each EVENT header in the file
    do
    {
        int32_t revisionNumber = readEVEH();

        readADDH();
        readINFO();
        if (revisionNumber == 0)
        {
            readINDX();
        }
        readBEAM();
        readSLOW();

        // For each ACQC header in the file
        while (FileReader::getInstance()->getNtofLib().getNextACQC())
        {
            readACQC();
        }
    } while (FileReader::getInstance()->getNtofLib().getNextEvent());

    // Reset the event with the good event
    if (eventId == 0)
    {
        FileReader::getInstance()->getNtofLib().getEvent(0);
    }
    else
    {
        FileReader::getInstance()->getNtofLib().getEvent(eventId);
    }
}

/*
 * \brief Initialize and fill a debug file
 */
void WriterFile::writeDebugFile()
{
    try
    {
        std::vector<std::string> paths =
            FileReader::getInstance()->getNtofLib().getFilenames();
        if (paths.size() == 1)
        {
            file_ << "FILE : " << paths[0] << std::endl;
        }
        else if (paths.size() > 1)
        {
            file_ << "FILES : " << std::endl;
            for (std::vector<std::string>::iterator file = paths.begin();
                 file != paths.end(); ++file)
            {
                file_ << "   - " << (*file) << std::endl;
            }
        }
        else
        {
            file_ << "NO FILE FOUND" << std::endl;
        }

        file_ << std::endl;

        readEverything();

        file_ << std::endl;
        file_ << "*** Reading finished successfully ***" << std::endl;
        file_ << std::endl;
    }
    catch (ntof::lib::NtofLibException &ex)
    {
        file_ << "ntofException : " << ex.getMessage() << " at line "
              << ex.getLine() << " in the file " << ex.getFile() << std::endl;
    }
    catch (...)
    {
        file_ << "An unexpected exception occurred" << std::endl;
    }
}

/*
 * \brief Initialize and fill a file with all the data selected (only the data
 * displayed in the plot area)
 */
void WriterFile::writeDataFile()
{
    try
    {
        ntof::lib::NtofLib &ntofLib = FileReader::getInstance()->getNtofLib();
        ntof::lib::FILE_INFO fileInfo = ntofLib.getFileInfo(
            ntofLib.getEVEH()->getHeader().file);
        file_ << "Run Number = \"" << ntofLib.getRCTR()->getRunNumber() << "\""
              << std::endl;
        file_ << "Segment Number = \"" << fileInfo.segmentNumber << "\""
              << std::endl;
        file_ << "Trigger Number = \"" << ntofLib.getEVEH()->getEventNumber()
              << "\"" << std::endl;
        if (ntofLib.getEVEH()->getRevisionNumber() >= 1)
        {
            file_
                << "Beam intensity = " << ntofLib.getEVEH()->getBeamIntensity()
                << std::endl;
        }
        file_ << "Path = \"" << fileInfo.fullpath << "\"" << std::endl;

        const auto IFS = "\t";
        std::list<Channel *> channels = FileReader::getInstance()->getChannels();
        std::vector<Channel *> activeChannels;

        for (const auto &channel : channels)
            if (channel->isActive())
                activeChannels.push_back(channel);

        if (activeChannels.size() == 0)
            return; // Nothing to do.

        // Write common header of channel information.
        file_ << std::endl
              << "Detector Type" << IFS << "Detector Id" << IFS << "Module Type"
              << IFS << "Channel" << IFS << "Module" << IFS << "Chassis" << IFS
              << "Stream" << IFS << "Sample Rate" << IFS << "Sample Size" << IFS
              << "Full Scale" << IFS << "Delay Time" << IFS << "Threshold"
              << IFS << "Pulse Sign" << IFS << "Pre-sample" << IFS
              << "Post-sample" << IFS << "Clock State" << IFS
              << "Zero Suppr. Start" << IFS << "Master Detector Type" << IFS
              << "Master Detector Id" << std::endl;

        for (const auto &channel : activeChannels)
        {
            file_
                << channel->getDetectorType() << IFS << channel->getDetectorId()
                << IFS << channel->getModuleType() << IFS
                << channel->getChannel() << IFS << channel->getModule() << IFS
                << channel->getChassis() << IFS << channel->getStream() << IFS
                << channel->getSampleRate() << IFS << channel->getSampleSize()
                << IFS << channel->getFullScaleInMilliVolts() << IFS
                << channel->getDelayTime() << IFS << channel->getThreshold()
                << IFS << channel->getThresholdSign() << IFS
                << channel->getPreSamples() << IFS << channel->getPostSamples()
                << IFS << channel->getClockState() << IFS
                << channel->getZeroSuppressionStart() << IFS
                << channel->getMasterDetectorType() << IFS
                << channel->getMasterDetectorId() << std::endl;
        }

        std::vector<GraphDataContainer::const_iterator> datas;
        datas.reserve(activeChannels.size());
        // Get the actual range
        Zoom range = MainWindow::getInstance()->getCustomPlot()->getZoom();
        const ssize_t maxRange = (ssize_t) range.xAxis.upper + 1;
        double startTime = std::numeric_limits<double>::max();

        // Header of Data Points section
        file_ << std::endl << "Time (ns)";

        for (const auto &channel : activeChannels)
        {
            GraphDataContainer::const_iterator it =
                channel->getData()->findBegin(range.xAxis.lower, true);
            datas.push_back(it);
            if (it->key < startTime)
                startTime = it->key;

            std::string channelDetTypeId = getChannelDetectorTypeID(channel);
            file_ << IFS << channelDetTypeId << ": Amplitude (V)" << IFS
                  << channelDetTypeId << ": Amplitude (ADC ch)";
        }
        file_ << std::endl;

        // Data
        if (startTime > std::numeric_limits<ssize_t>::max())
            startTime = maxRange + 1;

        const bool isAdc = (Config::getInstance()->getDataUnit() == "adc");
        std::ostringstream oss;
        for (ssize_t timeIndex = (ssize_t) startTime; timeIndex <= maxRange;
             timeIndex++)
        {
            oss.seekp(0);
            bool hasValue = false;

            for (size_t i = 0; i < activeChannels.size(); i++)
            {
                double value = getNextValue(
                    datas[i], activeChannels[i]->getData()->constEnd(),
                    timeIndex);

                try
                {
                    if (std::isnan(value))
                    {
                        oss << IFS << '-' << IFS << '-';
                    }
                    else if (isAdc)
                    {
                        hasValue = true;
                        oss << IFS << activeChannels[i]->ADCToMv(value) << IFS
                            << value;
                    }
                    else
                    {
                        hasValue = true;
                        oss << IFS << value << IFS
                            << activeChannels[i]->mvToADC(value);
                    }
                }
                catch (FileDisplayException &ex)
                {
                    QString title("Conversion error");
                    QString message((isAdc) ? "Error during the conversion of "
                                              "the data from ADC code to mV" :
                                              "Error during the conversion of "
                                              "the data from mV to ADC code");
                    QMessageBox::critical(MainWindow::getInstance(), title,
                                          message, QMessageBox::Ok);
                    return;
                }
            }
            if (hasValue)
            {
                file_ << timeIndex;
                file_.write(oss.str().c_str(), oss.tellp());
                file_ << '\n';
            }
            if (timeIndex % 100 == 0)
            {
                file_ << std::flush;
            }
        }
    }
    catch (ntof::lib::NtofLibException &ex)
    {
        file_ << "ntofException : " << ex.getMessage() << " at line "
              << ex.getLine() << " in the file " << ex.getFile() << std::endl;
    }
    catch (...)
    {
        file_ << "An unexpected exception occurred" << std::endl;
    }
}

} // namespace eventDisplay
} // namespace ntof
