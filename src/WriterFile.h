#ifndef WRITERFILE_H
#define WRITERFILE_H

#include <fstream>
#include <iostream>
#include <vector>

#include <NtofLib.h>

namespace ntof {
namespace eventDisplay {
enum FILE_TYPE
{
    DEBUG = 0,
    DATA
};

class MainWindow;

struct ExportDataPoint
{
    double valueMV;
    double valueADC;
};

class WriterFile
{
public:
    /*
     * \brief Constructor of the class
     * \param filename: Full path of the output file
     * \param filetype: Enum used to select the file to create
     */
    WriterFile(std::string filename, int32_t filetype);

private:
    /*
     * \brief Write the RCTR
     */
    void readRCTR();

    /*
     * \brief Write the MODH
     */
    void readMODH();

    /*
     * \brief Write the EVEH
     * \return Return the revision number of the EVEH
     */
    int32_t readEVEH();

    /*
     * \brief Write the INFO
     */
    void readINFO();

    /*
     * \brief Write the INDX
     */
    void readINDX();

    /*
     * \brief Write the BEAM
     */
    void readBEAM();

    /*
     * \brief Write the SLOW
     */
    void readSLOW();

    /*
     * \brief Write the ADDH
     */
    void readADDH();

    /*
     * \brief Write the configuration from the MODH initialized through the ACQC
     */
    void readConfiguration();

    /*
     * \brief Write the ACQC
     */
    void readACQC();

    /*
     * \brief Try to write all the headers
     */
    void readEverything();

    /*
     * \brief Initialize and fill a file with all the data selected
     */
    void writeDataFile();

    /*
     * \brief Initialize and fill a debug file
     */
    void writeDebugFile();

    std::ofstream file_; //!< File opened
};
} // namespace eventDisplay
} // namespace ntof

#endif // WRITERFILE_H
