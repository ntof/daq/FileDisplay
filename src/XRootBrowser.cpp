/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-04-20T09:36:08+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "XRootBrowser.hpp"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>

#include <stdio.h>

#include "MainWindow.h"

#include <XrdCl/XrdClFileSystem.hh>

namespace ntof {
namespace eventDisplay {

XRootBrowser::XRootBrowser(const std::string &url) :
    m_fs(0), m_path(new std::string())
{
    XrdCl::URL xurl(url);
    m_server.reset(new std::string(xurl.GetHostId()));
    m_params.reset(new std::string(xurl.GetParamsAsString()));

    const std::string &path = xurl.GetPath();
    if (!path.empty() && path.at(0) != '/')
        m_path.reset(new std::string("/" + path));
    else
        m_path.reset(new std::string("/" + path));
    m_fs = new XrdCl::FileSystem(xurl);
}

XRootBrowser::XRootBrowser() :
    XRootBrowser(Config::getInstance()->getXRootPath())
{}

XRootBrowser::~XRootBrowser()
{
    if (m_fs)
        delete m_fs;
}

void XRootBrowser::chdir(const std::string &dir)
{
    std::string path;
    if (dir.empty())
        path = *m_path;
    else if (dir == "..")
    {
        size_t sz = m_path->find_last_of('/');
        if (sz == std::string::npos)
            throw Exception("Can't find parent directory");
        path = *m_path;
        path.resize(sz);
    }
    else if (dir[0] == '/')
        path = dir;
    else
        path = *m_path + "/" + dir;
    if (path.empty())
        path = "/";
    SharedPath sPath(new std::string(path));
    load(sPath, m_files);
    m_path = sPath;
}

void XRootBrowser::chdir(const SharedFileInfo &info)
{
    if (!info)
        throw Exception("Invalid FileInfo");
    chdir(info->fullpath());
}

void XRootBrowser::prepare(const SharedFileInfo &info)
{
    if (!info)
        throw Exception("Invalid FileInfo");
    std::vector<std::string> files(1, info->fullpath());
    XrdCl::Buffer *buff;
    XrdCl::XRootDStatus status = m_fs->Prepare(
        files, XrdCl::PrepareFlags::Stage, 0, buff);
    if (!status.IsOK())
        throw Exception(status.ToStr());
    delete buff;
}

void XRootBrowser::load(const SharedPath &path, FileInfoList &out)
{
    XrdCl::DirectoryList *dirList;
    XrdCl::XRootDStatus status = m_fs->DirList(
        *path + *m_params, XrdCl::DirListFlags::Stat, dirList);
    if (!status.IsOK())
        throw Exception(status.ToStr());

    std::unique_ptr<XrdCl::DirectoryList> dirListPtr(dirList);

    out.clear();
    XrdCl::DirectoryList::Iterator it;
    for (it = dirList->Begin(); it != dirList->End(); ++it)
    {
        SharedFileInfo info(
            new FileInfo(m_server, path, (*it)->GetName(), m_params));
        XrdCl::StatInfo *stat = (*it)->GetStatInfo();
        info->flags = stat->GetFlags();
        info->size = stat->GetSize();
        info->mod = stat->GetModTime();
        out.push_back(info);
    }
}

XRootBrowser::SharedFileInfo &XRootBrowser::updateInfo(SharedFileInfo &info)
{
    if (!info)
        throw Exception("Invalid FileInfo");
    XrdCl::StatInfo *stat;
    XrdCl::XRootDStatus status = m_fs->Stat(info->fullpath(), stat);

    if (!status.IsOK())
        throw Exception(status.ToStr());

    info->flags = stat->GetFlags();
    info->size = stat->GetSize();
    info->mod = stat->GetModTime();
    return info;
}

XRootBrowser::SharedFileInfo XRootBrowser::getInfo(const std::string &url)
{
    XRootBrowser browser(url);
    XrdCl::URL xurl(url);
    XrdCl::FileSystem fs(xurl);

    SharedPath server(new std::string(xurl.GetHostId()));
    SharedPath params(new std::string(xurl.GetParamsAsString()));
    std::string path = xurl.GetPath();
    SharedPath spath;
    std::string name;
    const size_t sz = path.find_last_of('/');
    if (sz == std::string::npos)
    {
        name = path;
        spath.reset(new std::string());
    }
    else
    {
        name = path.substr(sz + 1);
        path.resize(sz);
        spath.reset(new std::string(path));
    }

    SharedFileInfo info(new FileInfo(server, spath, name, params));
    try
    {
        browser.updateInfo(info);
    }
    catch (Exception &ex)
    {
        std::cerr << "Error on " << info->fullpath() << ": " << ex.what()
                  << std::endl;
        info->flags = XrdCl::StatInfo::Offline;
    }
    return info;
}

XRootBrowser::FileInfo::FileInfo(const XRootBrowser::SharedPath &server,
                                 const XRootBrowser::SharedPath &path,
                                 const std::string &name,
                                 const XRootBrowser::SharedPath &params) :
    name(name),
    server(server),
    path(path),
    params(params),
    flags(0),
    size(0),
    mod(0)
{}

std::string XRootBrowser::FileInfo::fullpath() const
{
    return *path + "/" + name;
}

std::string XRootBrowser::FileInfo::url() const
{
    return "xroot://" + *server + "/" + fullpath() + *params;
}

bool XRootBrowser::FileInfo::isDir() const
{
    return flags & XrdCl::StatInfo::IsDir;
}

bool XRootBrowser::FileInfo::isOffline() const
{
    return flags & XrdCl::StatInfo::Offline;
}

XRootBrowser::Exception::Exception(const std::string &what) : m_what(what) {}

const char *XRootBrowser::Exception::what() const throw()
{
    return m_what.c_str();
}

} // namespace eventDisplay
} // namespace ntof
