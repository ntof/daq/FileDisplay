/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-04-20T09:37:32+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef XROOT_BROWSER_HPP__
#define XROOT_BROWSER_HPP__

#include <list>
#include <memory>
#include <string>
#include <vector>

namespace XrdCl {
class FileSystem;
}

namespace ntof {
namespace eventDisplay {

class XRootBrowser
{
public:
    explicit XRootBrowser(const std::string &url);
    XRootBrowser();
    ~XRootBrowser();

    typedef std::shared_ptr<std::string> SharedPath;

    /**
     * @brief file information
     */
    struct FileInfo
    {
        FileInfo(const SharedPath &server,
                 const SharedPath &path,
                 const std::string &name,
                 const SharedPath &params);

        std::string name;
        const std::shared_ptr<std::string> server;
        const std::shared_ptr<std::string> path;
        const std::shared_ptr<std::string> params;
        uint32_t flags;
        size_t size;
        time_t mod;

        std::string fullpath() const;
        std::string url() const;

        bool isDir() const;
        bool isOffline() const;
    };

    typedef std::shared_ptr<FileInfo> SharedFileInfo;
    typedef std::list<SharedFileInfo> FileInfoList;

    /**
     * @brief get current path
     */
    inline const std::string &path() const { return *m_path; };

    /**
     * @brief list files and directories
     */
    inline const FileInfoList &files() const { return m_files; };
    inline FileInfoList &files() { return m_files; };

    /**
     * @brief change directory
     * @param[in] directory where to go
     *
     * @details if directory starts with a "/" the path is assumed to be absolute
     *
     * @details special value ".." goes up one directory
     */
    void chdir(const std::string &directory);
    void chdir(const SharedFileInfo &file);

    /**
     * @brief send a file staging request
     * @param[in] file the file to check
     */
    void prepare(const SharedFileInfo &file);

    /**
     * @brief stat a file and update its information
     * @param[in,out]  file the file to stat
     * @return file
     *
     * @details Offline information is not set when listing a directory, thus
     * it must be updated with this method.
     */
    SharedFileInfo &updateInfo(SharedFileInfo &file);

    /**
     * @brief retrieve file info from a complete URL
     * @param[in]  url
     * @return file
     */
    static SharedFileInfo getInfo(const std::string &url);

    class Exception : public std::exception
    {
    public:
        explicit Exception(const std::string &what);

        const char *what() const throw();

    protected:
        std::string m_what;
    };

private:
    explicit XRootBrowser(const XRootBrowser &other);
    XRootBrowser &operator=(const XRootBrowser &other);

protected:
    void load(const SharedPath &path, FileInfoList &out);

    XrdCl::FileSystem *m_fs;
    FileInfoList m_files;
    SharedPath m_server;
    SharedPath m_path;
    SharedPath m_params;
};

} // namespace eventDisplay
} // namespace ntof

#endif // XROOT_BROWSER_HPP__
