#include "XRootFileDialog.hpp"

#include <iomanip>
#include <iostream>
#include <istream>
#include <sstream>
#include <string>
#include <vector>

#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QFileInfo>
#include <QGroupBox>
#include <QHeaderView>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QTableWidget>

#include <stdio.h>

#include "Config.h"
#include "FileReader.h"
#include "MainWindow.h"
#include "NavButton.h"
#include "flowlayout.h"

#include <XrdCl/XrdClFileSystem.hh>

namespace ntof {
namespace eventDisplay {

class XRootFileDialogItem : public QTableWidgetItem
{
public:
    XRootFileDialogItem(const QString &text, const QVariant &data);

    bool operator<(const QTableWidgetItem &other) const;

    static QString toStrSize(size_t);
    static QString toStrTime(time_t mod);
};

XRootFileDialogItem::XRootFileDialogItem(const QString &text,
                                         const QVariant &data) :
    QTableWidgetItem(text)
{
    setData(Qt::UserRole, data);
}

bool XRootFileDialogItem::operator<(const QTableWidgetItem &other) const
{
    return data(Qt::UserRole) < other.data(Qt::UserRole);
}

QString XRootFileDialogItem::toStrSize(size_t value)
{
    // Define the exponent
    int32_t exp = 0;
    float tmpValue = value;
    while (tmpValue > 1024)
    {
        ++exp;
        tmpValue /= 1024;
    }

    switch (exp)
    {
    case 1: return QString("%1 KB").arg(QString().setNum(tmpValue, 'f', 3));
    case 2: return QString("%1 MB").arg(QString().setNum(tmpValue, 'f', 3));
    case 3: return QString("%1 GB").arg(QString().setNum(tmpValue, 'f', 3));
    case 4: return QString("%1 TB").arg(QString().setNum(tmpValue, 'f', 3));
    default: return QString("%1 B").arg(value);
    }
}

QString XRootFileDialogItem::toStrTime(time_t ltime)
{
    struct tm *tm;
    char time[64];
    tm = localtime(&ltime);
    strftime(time, 64, "%b %d %Y %H:%M:%S", tm);
    return QString(time);
}

/*
 * \brief Constructor of the class
 */
XRootFileDialog::XRootFileDialog() :
    selectedPath_(""),
    url_(new XrdCl::URL(Config::getInstance()->getXRootPath()))
{
    resize(1000, 700);
    setModal(true);
    init();
    getFileList(getRootPath());
    filesTab_->sortByColumn(MODIF, Qt::DescendingOrder);
    filesTab_->setShowGrid(false);
    filesTab_->setWordWrap(false);
}

/*
 * \brief Destructor of the class
 */
XRootFileDialog::~XRootFileDialog()
{
    delete url_;
    this->setVisible(false);
}

/*
 * \brief Display the list of files
 */
void XRootFileDialog::show()
{
    this->setVisible(true);
}

/*
 * \brief Action of opening a file
 */
void XRootFileDialog::open()
{
    this->setCursor(Qt::WaitCursor);
    QList<QTableWidgetItem *> selectedItems = filesTab_->selectedItems();
    if (selectedItems.size() == 0)
    {
        std::string location = location_->text().toStdString();
        getFileList(location);
        this->setCursor(Qt::ArrowCursor);
        return;
    }

    std::vector<std::string> filenames;
    for (QList<QTableWidgetItem *>::iterator it = selectedItems.begin();
         it != selectedItems.end(); ++it)
    {
        QString fullName = (*it)->data(ROLE_PATH).toString();
        bool isDir = (*it)->data(ROLE_ISDIR).toBool();
        if (isDir)
        {
            getFileList(fullName.toStdString());
            this->setCursor(Qt::ArrowCursor);
            return;
        }

        if ((*it)->column() == 0)
        {
            filenames.push_back(filesTab_->item((*it)->row(), NAME)
                                    ->data(ROLE_PATH)
                                    .toString()
                                    .toStdString());
        }
    }

    if (!filenames.empty())
    {
        if (FileReader::getInstance()->openFile(filenames))
        {
            FileReader::getInstance()->resetZoom();
            this->setVisible(false);
        }
    }
    else
    {
        QString title("No file selected");
        QString message("There is no file selected.");
        QMessageBox::warning(this, title, message, QMessageBox::Ok);
        this->setVisible(false);
    }

    this->setCursor(Qt::ArrowCursor);
}

/*
 * \brief Close the dialog box without any changes
 */
void XRootFileDialog::cancel()
{
    this->setVisible(false);
}

/*
 * \brief Go to the previous location
 */
void XRootFileDialog::goBack()
{
    this->setCursor(Qt::WaitCursor);
    std::string previousPath;
    if (selectedPath_ == getRootPath())
    {
        previousPath = getRootPath();
    }
    else
    {
        previousPath = QFileInfo(tr(selectedPath_.c_str())).path().toStdString();
    }

    getFileList(previousPath);
    this->setCursor(Qt::ArrowCursor);
}

/*
 * \brief Listener of the simple click mouse event
 * \param row: ID of the row where the click has been performed
 * \param column: ID of the column where the click has been performed
 */
void XRootFileDialog::cellClicked(int row, int /*column*/)
{
    //            std::cerr << "Row selected: " << row << std::endl;
    filesTab_->selectRow(row);
}

/*
 * \brief Unselect all the elements of the table
 */
void XRootFileDialog::clearSelection()
{
    QList<QTableWidgetItem *> selectedItems = filesTab_->selectedItems();
    for (QList<QTableWidgetItem *>::iterator it = selectedItems.begin();
         it != selectedItems.end(); ++it)
    {
        (*it)->setSelected(false);
    }
}

std::string XRootFileDialog::getRootPath() const
{
    return "/" + url_->GetPath();
}

/*
 * \brief Listener of the double click mouse event
 * \param row: ID of the row where the click has been performed
 * \param column: ID of the column where the click has been performed
 */
void XRootFileDialog::cellDoubleClicked(int /*row*/, int /*column*/)
{
    open();
}

/*
 * \brief Create the content of the dialog window
 */
void XRootFileDialog::init()
{
    QVBoxLayout *layout = new QVBoxLayout();

    QGroupBox *buttonsGroup = new QGroupBox();
    buttonsGroup->setObjectName(tr("location"));

    navLayout_ = new FlowLayout(2);
    goBackButton_ = new QPushButton("<=");
    goHomeButton_ = new NavButton(this, Config::getInstance()->getXRootPath(),
                                  getRootPath());
    navLayout_->addWidget(goBackButton_);
    navLayout_->addWidget(new QLabel("   "));
    navLayout_->addWidget(goHomeButton_);
    buttonsGroup->setLayout(navLayout_);
    connect(goBackButton_, SIGNAL(clicked()), this, SLOT(goBack()));

    location_ = new QLineEdit(tr("Location : "));
    // connect(location_, SIGNAL(focusInEvent()), this, SLOT(clearSelection()));

    filesTab_ = new QTableWidget();
    filesTab_->setColumnCount(3);
    QStringList tableHeaders;
    tableHeaders << "Name"
                 << "Size"
                 << "Modified";
    filesTab_->setHorizontalHeaderLabels(tableHeaders);
    filesTab_->setEditTriggers(QAbstractItemView::NoEditTriggers);
    filesTab_->verticalHeader()->hide();
    filesTab_->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    filesTab_->horizontalHeader()->setSectionResizeMode(
        1, QHeaderView::ResizeToContents);
    filesTab_->horizontalHeader()->setSectionResizeMode(
        2, QHeaderView::ResizeToContents);
    filesTab_->setSortingEnabled(true);

    connect(filesTab_, SIGNAL(cellDoubleClicked(int, int)), this,
            SLOT(cellDoubleClicked(int, int)));
    connect(filesTab_, SIGNAL(cellClicked(int, int)), this,
            SLOT(cellClicked(int, int)));

    QDialogButtonBox *buttonBox = new QDialogButtonBox(
        QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(open()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(cancel()));

    layout->addWidget(buttonsGroup);
    layout->addWidget(location_);
    layout->addWidget(filesTab_);
    layout->addWidget(buttonBox);

    setLayout(layout);
}

/*
 * \brief Fill the vector of files with the all the files at the path given
 * \param path: Localization where to look for files
 */
void XRootFileDialog::getFileList(const std::string &path)
{
    filesTab_->setSortingEnabled(false);
    selectedPath_ = path;

    const std::string rootPath = getRootPath();
    std::list<std::string> navPath;
    // Full the vector with all the consecutive path
    if (path != rootPath)
    {
        std::string previousPath = path;
        while ((previousPath != rootPath) && (previousPath.size() > 1))
        {
            navPath.push_front(previousPath);
            previousPath = QFileInfo(previousPath.c_str()).path().toStdString();
        }
    }

    // Create the buttons
    for (NavButton *nav : navBar_)
    {
        navLayout_->removeWidget(nav);
        // this may be called from NavButton itself, so delay delete
        nav->deleteLater();
    }
    navBar_.clear();

    for (const std::string &path : navPath)
    {
        QFileInfo infoFile(path.c_str());
        NavButton *button = new NavButton(
            this, infoFile.fileName().toStdString(), path);
        navLayout_->addWidget(button);
        navBar_.push_back(button);
    }

    location_->setText(path.c_str());
    filesTab_->setRowCount(0);

    try
    {
        browser_.chdir(path);
        QIcon icon_folder(":/icons/oxygen/48x48/mimetypes/inode-directory.png");
        QIcon icon_file(":/icons/oxygen/48x48/mimetypes/x-office-document.png");

        files_ = browser_.files();
        int i = 0;
        for (const XRootBrowser::SharedFileInfo &info : files_)
        {
            QString strMtime = XRootFileDialogItem::toStrTime(info->mod);

            if (info->isDir())
            {
                // If it is a directory
                QTableWidgetItem *name = new QTableWidgetItem(
                    info->name.c_str());
                name->setData(ROLE_PATH, info->fullpath().c_str());
                name->setData(ROLE_ISDIR, info->isDir());
                name->setIcon(icon_folder);

                // i = filesTab_->rowCount();
                filesTab_->setRowCount(i + 1);
                filesTab_->setItem(i, NAME, name);
                filesTab_->setItem(i, SIZE,
                                   new XRootFileDialogItem(QString(""), 0));
                filesTab_->setItem(
                    i, MODIF,
                    new XRootFileDialogItem(strMtime,
                                            QVariant::fromValue(info->mod)));
                ++i;
            }
            else
            {
                // If it is a file
                QFileInfo infoFile(info->name.c_str());
                if (infoFile.completeSuffix() == "raw.finished" ||
                    infoFile.completeSuffix() == "idx.finished")
                {
                    QTableWidgetItem *name = new QTableWidgetItem(
                        info->name.c_str());
                    name->setData(ROLE_PATH, info->url().c_str());
                    name->setData(ROLE_ISDIR, info->isDir());

                    filesTab_->setRowCount(i + 1);
                    name->setIcon(icon_file);
                    filesTab_->setItem(i, NAME, name);
                    filesTab_->setItem(
                        i, SIZE,
                        new XRootFileDialogItem(
                            XRootFileDialogItem::toStrSize(info->size),
                            QVariant::fromValue(info->size)));
                    filesTab_->setItem(
                        i, MODIF,
                        new XRootFileDialogItem(
                            strMtime, QVariant::fromValue(info->mod)));
                    ++i;
                }
            }
        }
    }
    catch (XRootBrowser::Exception &ex)
    {
        std::cerr << "Unable to read XRoot directory: " << ex.what()
                  << std::endl;
        selectedPath_ = getRootPath();
    }

    filesTab_->setSortingEnabled(true);
    filesTab_->resizeRowsToContents();
}

} // namespace eventDisplay
} // namespace ntof
