/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-04-20T16:11:40+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef XROOTFILEDIALOG_H
#define XROOTFILEDIALOG_H

#include <string>
#include <vector>

#include <QObject>

#include "VirtualCounter.h"
#include "XRootBrowser.hpp"

class QTableWidget;
class QLineEdit;
class QDialogButtonBox;
class QPushButton;
class QGroupBox;
class FlowLayout;

namespace XrdCl {
class FileSystem;
class URL;
} // namespace XrdCl

namespace ntof {
namespace eventDisplay {

class MainWindow;
class NavButton;

/* ENUM */
enum
{
    NAME,
    SIZE,
    MODIF
};

/* ENUM */
enum
{
    ROLE_PATH = 33,
    ROLE_ISDIR
};

const int32_t COLUMN_NUMBER = 3;

class XRootFileDialog : public VirtualCounter
{
    Q_OBJECT
public:
    /*
     * \brief Constructor of the class
     */
    XRootFileDialog();

    /*
     * \brief Destructor of the class
     */
    ~XRootFileDialog();

    /*
     * \brief Fill the vector of files with the all the files at the path given
     * \param path: Localization where to look for files
     */
    void getFileList(const std::string &path);

    /*
     * \brief Display the list of files
     */
    void show();

private slots:
    /*
     * \brief Action of opening a file
     */
    void open();

    /*
     * \brief Close the dialog box without any changes
     */
    void cancel();

    /*
     * \brief Go to the previous location
     */
    void goBack();

    /*
     * \brief Listener of the double click mouse event
     * \param row: ID of the row where the click has been performed
     * \param column: ID of the column where the click has been performed
     */
    void cellDoubleClicked(int row, int column);

    /*
     * \brief Listener of the simple click mouse event
     * \param row: ID of the row where the click has been performed
     * \param column: ID of the column where the click has been performed
     */
    void cellClicked(int row, int column);

    /*
     * \brief Unselect all the elements of the table
     */
    void clearSelection();

    /**
     * @brief return root path
     */
    std::string getRootPath() const;

private:
    /*
     * \brief Create the content of the dialog window
     */
    void init();

    /*
     * \brief Sort the content of the table
     * \param column: Id of the column to sort
     * \param order: Allow to set the order ascendant or descendant
     */
    void sortItems(int32_t column, Qt::SortOrder order = Qt::AscendingOrder);

    XRootBrowser browser_; //!< Miscellaneous functions to dialog with CASTOR
    XRootBrowser::FileInfoList files_; //!< List of all the files found at a
                                       //!< defined path
    XrdCl::URL *url_;
    QPushButton *goBackButton_; //!< Button to go to the parent folder
    NavButton *goHomeButton_;   //!< Button to go back to the root folder
    QLineEdit *location_;       //!< Actual path in clear
    QTableWidget *filesTab_;    //!< Table used to display the list of files
                                //!< available
    FlowLayout *navLayout_;     //!< Layout of the navigation buttons
    std::string selectedPath_;  //!< Actual path
    std::vector<NavButton *> navBar_; //!< List of all the buttons in the
                                      //!< navigation bar
};
} // namespace eventDisplay
} // namespace ntof

#endif // XROOTFILEDIALOG_H
