
#include <iostream>

#include <QApplication>
#include <QLocale>

#include <NtofLibException.h>
#include <sys/time.h>

#include "Config.h"
#include "FileReader.h"
#include "MainWindow.h"
#include "MenuBar.h"
#include "local-config.h"

int main(int argc, char *argv[])
{
    try
    {
        // Initialize the icons used by the display
        Q_INIT_RESOURCE(icons);
        QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedStates));

        // Set the version of the file display
        ntof::eventDisplay::MenuBar::versionFileDisplay_ = APP_VERSION;
        ntof::eventDisplay::MenuBar::versionQCustomPlot_ = "1.3.1";
        ntof::eventDisplay::MenuBar::versionQt_ = QT_VERSION;
        ntof::eventDisplay::MenuBar::versionBoost_ = BOOST_VERSION;
        ntof::eventDisplay::MenuBar::versionCMake_ = CMAKE_VERSION;

        // Create the window
        QApplication app(argc, argv);
        QCoreApplication::setApplicationName("FileDisplay");
        QCoreApplication::setApplicationVersion(APP_VERSION);

        QCommandLineParser parser;
        parser.setApplicationDescription(
            "FileDisplay - Visualize nToF acquisition raw files");
        parser.addHelpOption();
        parser.addVersionOption();

        // An option with a value
        QCommandLineOption configArgument(QStringList() << "c"
                                                        << "config",
                                          "configuration file's path.",
                                          "config");
        parser.addOption(configArgument);
        parser.addPositionalArgument("file", "nToF Raw file to open.");

        parser.process(app);

        QString configPath = parser.value(configArgument);
        if (!configPath.isEmpty())
        {
            ntof::eventDisplay::Config::configFile_ = configPath.toStdString();
        }

        ntof::eventDisplay::MainWindow::getInstance()->create();
        ntof::eventDisplay::MainWindow::getInstance()->updateElement();
        ntof::eventDisplay::MainWindow::getInstance()->show();

        const QStringList posArgs = parser.positionalArguments();
        if (posArgs.size() == 1)
        {
            ntof::eventDisplay::FileReader::getInstance()->openFile(
                posArgs.at(0).toStdString());
        }

        ntof::eventDisplay::MainWindow::getInstance()
            ->getCustomPlot()
            ->openglRenderingDetection();

        return app.exec();
    }
    catch (std::bad_alloc &ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    catch (ntof::lib::NtofLibException &ex)
    {
        std::cerr << "NtofLibException: " << ex.getMessage() << '\n';
    }
    catch (...)
    {
        std::cerr << "The system has been close by an uncaught exception."
                  << std::endl;
    }

    return 0;
}
