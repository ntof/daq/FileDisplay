/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-23T14:41:35+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "FileSelector.h"

#include <QDir>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QPushButton>

FileSelector::FileSelector(const QString &title,
                           const QString &filter,
                           QWidget *parent) :
    QWidget(parent), m_title(title), m_filter(filter)
{
    setLayout(new QHBoxLayout);
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);

    m_lineEdit = new QLineEdit();
    m_lineEdit->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    layout()->addWidget(m_lineEdit);

    QPushButton *button = new QPushButton(tr("Browse"));

    // Connection by lambda expression to invoke the onBrowseClicked()
    // virtual function through 'this', avoiding the cppcheck
    // virtualCallInConstructor warning
    connect(button, &QAbstractButton::clicked,
            [this]() { this->onBrowseClicked(); });
    layout()->addWidget(button);
}

void FileSelector::setPath(const QString &dir)
{
    m_lineEdit->setText(dir);
}

QString FileSelector::path() const
{
    return m_lineEdit->text();
}

void FileSelector::onBrowseClicked()
{
    QDir dir(QFileInfo(m_lineEdit->text()).absolutePath());
    if (!dir.exists())
        dir = QDir::homePath();

    QString file = QFileDialog::getOpenFileName(this, m_title,
                                                dir.absolutePath(), m_filter);
    if (!file.isEmpty())
    {
        m_lineEdit->setText(file);
    }
}

ExistingDirSelector::ExistingDirSelector(const QString &title, QWidget *parent) :
    FileSelector(title, QString(), parent)
{}

void ExistingDirSelector::onBrowseClicked()
{
    QDir dir(m_lineEdit->text());
    if (!dir.exists())
        dir = QDir::homePath();

    QString newDir = QFileDialog::getExistingDirectory(this, m_title,
                                                       dir.absolutePath());
    if (!newDir.isEmpty())
        m_lineEdit->setText(newDir);
}