/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-23T14:37:42+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef FILESELECTOR_H_
#define FILESELECTOR_H_

#include <QLineEdit>
#include <QObject>
#include <QWidget>

/**
 * @brief file selection widget, with a "browse button"
 */
class FileSelector : public QWidget
{
    Q_OBJECT
public:
    FileSelector(const QString &title, const QString &filter, QWidget *parent);

    void setPath(const QString &dir);
    QString path() const;

protected slots:
    virtual void onBrowseClicked();

protected:
    QString m_title;
    QString m_filter;
    QLineEdit *m_lineEdit;
};

class ExistingDirSelector : public FileSelector
{
    Q_OBJECT
public:
    ExistingDirSelector(const QString &title, QWidget *parent);

protected slots:
    virtual void onBrowseClicked() override;
};

#endif
